<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_JobMixFormula extends CI_Model {

  protected $userDetail 	             = 'em_user_details';
  protected $useradmin                 = 'em_useradmins';
  protected $useradminDetail           = 'em_useradmin_details';
  protected $JobMixFormula             = 'em_job_mix_formula';
  protected $DailyCompressive          = 'em_daily_compressive';
  protected $product                   = 'em_product';
  protected $project                   = 'em_project';
  protected $mutu                      = 'em_mutu';
  protected $sumberMaterial            = 'em_sumber_material';
  protected $SlumpFlowTest             = 'em_slump_flow_data';
  protected $sumberSemen               = 'em_sumber_semen';
  protected $sumberHalus               = 'em_sumber_halus';
  protected $sumberKasar               = 'em_sumber_kasar';
  protected $sumberAdditive            = 'em_sumber_additive';
  protected $toBeUsed                  = 'em_to_be_used';
  protected $status                    = 'em_status';



  public function logged_id(){ 
    return $this->session->userdata('id'); 
  }
  //fungsi check login 
  public function check_login($data_user){ 
    $this->db->where("(nis='".$data_user['email']."' OR email='".$data_user['email']."' )"); 
    $query = $this->db->get($this->user); 
    if ($query->num_rows() == 1) { 
      $hash = $query->row('password'); 
      if (password_verify($data_user['password'],$hash)){ 
        return $query->result(); 
      } else { 
        $this->session->set_flashdata('fail_msg_password', 'Password Salah');
      } 
    } else { 
      $this->session->set_flashdata('fail_msg_account', 'Account Tidak tersedia');
    } 
  } 
 
//   Input Data Job Mix Formula
  public function inputJobMixFormula($data_jmf){
    return $this->db->insert($this->JobMixFormula, $data_jmf); 
  } 
// End  Input Data Job Mix Formula

//   Input Data Slump FLow Test
public function inputSlumpFlowTest($data_sft){
  return $this->db->insert($this->SlumpFlowTest, $data_sft); 
} 
// End  Input Data Slump FLow Test

//   Input Data Daily Compressive
public function inputDailyCompressive($data_compressive){
  return $this->db->insert($this->DailyCompressive, $data_compressive); 
} 
// End  Input Data Daily Compressive


    //Ambil Data Job Mix Formula
    public function getJobMixFormula(){
        $this->db->from($this->JobMixFormula);
        $this->db->join($this->product, $this->product.'.id_product = '.$this->JobMixFormula.'.id_product');  
        $this->db->join($this->mutu, $this->mutu.'.id_mutu = '.$this->JobMixFormula.'.id_mutu');  
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Job Mix Formula

      //Ambil Data JobMixFormula BY ID
    public function getJobMixFormulaById($id){
        $this->db->from($this->JobMixFormula);
        $this->db->join($this->product, $this->product.'.id_product = '.$this->JobMixFormula.'.id_product');  
        $this->db->join($this->mutu, $this->mutu.'.id_mutu = '.$this->JobMixFormula.'.id_mutu');        
        $this->db->where($this->JobMixFormula.'.id_job_mix_formula', $id);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Job Mix Formula By ID

  

       //Ambil Data Product
    public function getProduct(){
        $this->db->from($this->product);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Product
      
       //Ambil Data Project
    public function getProject(){
        $this->db->from($this->project);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Project


      //Ambil Data Mutu
    public function getMutu(){
        $this->db->from($this->mutu);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Mutu

      //Ambil Data Sumber Material
    public function getSumberMaterial(){
        $this->db->from($this->sumberMaterial);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Sumber Material

      //Ambil Data Material
    public function getMaterial(){
        $this->db->from($this->material);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data PelMaterialaksana

      //Ambil Data Status
    public function getStatus(){
        $this->db->from($this->status);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Status

       //Ambil Data Semen
    public function getSumberSemen(){
        $this->db->from($this->sumberSemen);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Semen

      //Ambil Data Halus
    public function getSumberHalus(){
        $this->db->from($this->sumberHalus);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Halus

       //Ambil Data Kasar
    public function getSumberKasar(){
        $this->db->from($this->sumberKasar);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Kasar

         //Ambil Data Additive
    public function getSumberAdditive(){
        $this->db->from($this->sumberAdditive);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Additive

        //Delete Data Job Mix Formula

    public function delJobMixFormula($id){
      $this->db->from($this->JobMixFormula);    
      $this ->db-> where('id_job_mix_formula', $id);
      $this ->db-> delete($this->JobMixFormula);
    }
    //Delete Data JobMixFormula
  
  public  function checkEmail($email){
    $this->db->where('email',$email);
    return  $this->db->get($this->user);
  }
  public  function checkEmailAdmin($email){
    $this->db->where('email',$email);
    return  $this->db->get($this->useradmin);
  }


  // admin
  public function checkLoginAdmin($data_user){ 
    $this->db->select($this->useradmin.'.*,'.$this->useradmin.'.id as idu ,'.$this->useradminDetail.'.*');
    $this->db->from($this->useradmin);    
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin ='.$this->useradmin.'.id');
    $this->db->where($this->useradmin.'.username', $data_user['email']);
    $this->db->or_where($this->useradmin.'.email', $data_user['email']);
    $query = $this->db->get();
    if ($query->num_rows() == 1) { 
      $hash = $query->row('password'); 
      if (password_verify($data_user['password'],$hash)){ 
        return $query->result(); 
      } else { 
        $this->session->set_flashdata('fail_msg_password', 'Password Salah');
      } 
    } else { 
      $this->session->set_flashdata('fail_msg_account', 'Account Tidak tersedia');
    } 
  }

  public function addUser($data)
  {
    $this->db->insert($this->useradmin, $data);
    return $this->db->insert_id();
  }

  public function getListUser(){
    $this->db->from($this->useradminDetail);
    $this->db->join($this->useradmin, $this->useradmin.'.id = '.$this->useradminDetail.'.id_useradmin');  
    $this->db->join($this->userAdminRole,$this->useradmin.'.id_role = '.$this->userAdminRole.'.id');
    $this->db->select('*');
    return $this->db->get();
  }

  public function getUser($id=0){
    $this->db->select($this->user.'.*,'.$this->userDetail.'.*');
    $this->db->from($this->user);
    $this->db->join($this->userDetail, $this->userDetail.'.id_user = '.$this->user.'.id');
    $this->db->where($this->user.'.id',$id);
    return $this->db->get();
  }
 

  public function getRole(){
    
    $this->db->from($this->useradmin);
    $this->db->join($this->userAdminRole,$this->useradmin.'.id_role = '.$this->userAdminRole.'.id');
    $this->db->select($this->useradmin.'.id');
    $this->db->select($this->useradmin.'.fullname');
    $this->db->select($this->userAdminRole.'.name');
    return $this->db->get();
  }

  public function getAllRole(){
    $this->db->select('*');
    $this->db->from($this->userAdminRole);
    return $this->db->get();
  }
  public function getImage($id){
    $this->db->from($this->useradminDetail);
    $this->db->join($this->useradmin, $this->useradmin.'.id = '.$this->useradminDetail.'.id_useradmin');
    $this->db->where($this->useradmin.'.id', $id);
    $this->db->select('img_path');
    return $this->db->get();
  }
  public function getImg($id){
    $this->db->join($this->useradmin, $this->useradmin.'.id = '.$this->useradminDetail.'.id_useradmin');
    $this->db->where($this->useradminDetail.'.id', $id);
    return $this->db->get($this->useradminDetail);
  }

  public function getUserEmail($id){
    $this->db->select($this->user.'email');
    $this->db->from($this->user);
    $this->db->where($this->user.'.nis', $id);
    return $this->db->get($this->user);

        
  }

  
  
  public function updateImg($img){
    $this->db->set($img);
    $this->db->where('id_user', $_SESSION['id']);
    $this->db->update($this->userDetail);    
  }
  public function updateData($data_user){
    $this->db->set($data_user);
    $this->db->where('id_user', $_SESSION['id']);
    $this->db->update($this->userDetail); 
  }
  public function updateImgAdmin($img){
    $this->db->set($img);
    $this->db->where('id_useradmin', $_SESSION['id']);
    $this->db->update($this->useradminDetail);    
  }

  public function updateDataAdmin($id=0 , $data=0){
    $this->db->set($data);
    $this->db->where('id', $id);
    $this->db->update($this->useradmin); 
  }

  public function updateDataAdminDetail($id=0 , $data=0){
    $this->db->set($data);
    $this->db->where('id_useradmin', $id);
    $this->db->update($this->useradminDetail); 
  }

  public function updateEmail($email){
    $this->db->set($email);
    $this->db->where('id', $_SESSION['id']);
    $this->db->update($this->user); 
  }
  public function getUserInfoByEmail($email){
    $this->db->select($this->user.'.*,'.$this->userDetail.'.*');
    $this->db->from($this->userDetail);
    $this->db->join($this->user, $this->user.'.id = '.$this->userDetail.'.id_user');
    $this->db->where($this->user.'.email', $email);
    $q = $this->db->get();  
      if($this->db->affected_rows() > 0){  
          $row = $q->row();  
          return $row;  
      }  
  }
  public function getUserByEmail($email){
    $this->db->select($this->user.'.*, '.$this->userDetail.'.*,'.$this->userRole.'.*');
    $this->db->from($this->user);
    $this->db->join($this->userDetail, $this->userDetail.'.id_user'.'='.$this->user.'.id', 'left');
    $this->db->join($this->userRole, $this->userRole.'.id'.'='.$this->user.'.id_role', 'left');   
    $this->db->where($this->user.'.email', $email); 
    return $this->db->get();
}
public function getUserAdminByEmail($email){
  $this->db->select($this->useradmin.'.*, '.$this->useradminDetail.'.*,'.$this->userAdminRole.'.*');
  $this->db->from($this->useradmin);
  $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin'.'='.$this->useradmin.'.id', 'left');
  $this->db->join($this->userAdminRole, $this->userAdminRole.'.id'.'='.$this->useradmin.'.id_role', 'left');   
  $this->db->where($this->useradmin.'.email', $email); 
  return $this->db->get();
}
  public function insertToken($email)  
  {    
    $token = substr(sha1(rand()), 0, 30);
    $user = $this->getUserByEmail($email)->row_array();     
    $string = array( 
        'id_user'    =>$user['id_user'], 
        'token_reset'=> $token,  
        'email'=>$email,
        'status'=>0,
        'created_at' =>date('Y-m-d H:m:s')
      );  
    $query = $this->db->insert_string($this->userReset,$string);  
    $this->db->query($query);  
    return $token . $email;  
  }
  public function isTokenValid($token)  
  {  
    $tkn = substr($token,0,30);  
    $email = substr($token,30);   
    // $this->db->where('token_reset', $tkn);
    // $this->db->where('email', $email);
    // $q = $this->db->get($this->userReset, 1);
    
    $q = $this->db->get_where($this->userReset, array(  
      $this->userReset.'.token_reset' => $tkn,   
      $this->userReset.'.email' => $email, 
      'status'=> 0), 1);               
      
    if($this->db->affected_rows() > 0){  
      $row = $q->row();         
        
      $created = $row->created_at;  
      $createdTS = strtotime($created);  
      $expired = date('Y-m-d H:i:s',strtotime('+4 hour',strtotime($created)));   
      $exp = strtotime($expired);  
        
      if($createdTS > $exp){  
        return false;  
      }  
      $user_info = $this->getUserInfoByEmail($row->email);  
      return $user_info;  
        
    }else{  
      return false;  
    }  
  }
  public function updatePassword($cleanPost)  
  {    
    $this->db->where('id', $cleanPost['id_user']);  
    $this->db->update($this->user, array('password' => $cleanPost['password']));     
    return true;
  }
  public function updateToken($cleanPost){ 
    $this->db->where('email', $cleanPost['email']);  
    $this->db->update($this->userReset, array('status' => 1));
    return true;  
  }
  public function countMember(){
    // $this->db->where('deleted_at !=', 000);
    $this->db->from($this->user);
    return $this->db->count_all_results();
  }
  public function getUserAdmin(){
    $this->db->select($this->useradmin.'.username,'.$this->useradmin.'.id,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    return $this->db->get();
  }
  public function banadmin($id){
    $this->db->set('is_ban',0);
    $this->db->where('id', $id);
    $this->db->update($this->useradmin); 
    redirect(base_url('admin/user/listUser'));
  }
  public function aktifadmin($id){
    $this->db->set('is_ban',1);
    $this->db->where('id', $id);
    $this->db->update($this->useradmin); 
    redirect(base_url('admin/user/listUser'));
  }
  public function getUserAdminByUsername($username){
    $this->db->select($this->useradmin.'.username,'.$this->useradmin.'.id,'.$this->useradmin.'.is_ban,'.$this->useradmin.'.email,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    $this->db->where($this->useradmin.'.username', $username);
    return $this->db->get();
  }
  public function getUserAdminById($id){
    $this->db->select($this->useradmin.'.id,'.$this->useradmin.'.username,'.$this->useradmin.'.is_ban,'.$this->useradmin.'.email,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role,'.$this->santri.'.*,');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    $this->db->join($this->santri, $this->santri.'.nis = '.$this->useradmin.'.id_useradmin');
    $this->db->where($this->useradmin.'.id', $id);
    return $this->db->get();
  }
  public function getId($username){
    $this->db->where('username',$username);
    $this->db->select('id');
    $this->db->from($this->useradmin);
    return $this->db->get($this->$useradmin);
  }

  public function deluserAdmin($id){
    $this ->db-> where('id', $id);
    $this ->db-> delete($this->useradmin);
}
public function deluserAdminDetail($id_useradmin){
  $this ->db-> where('id_useradmin', $id_useradmin);
  $this ->db-> delete($this->useradminDetail);
}
public function getUserInfo($id)  
{  
  $q = $this->db->get_where($this->useradmin, array('id' => $id), 1);   
  if($this->db->affected_rows() > 0){  
    $row = $q->row();  
    return $row;  
  }else{  
    error_log('No user found getUserInfo('.$id.')');  
    return false;  
  }  
}  
public function getUserInfoByMailUser($email){  
  $q = $this->db->get_where($this->useradmin, array('email' => $email), 1);   
  if($this->db->affected_rows() > 0){  
    $row = $q->row();  
    return $row;  
  }  
}  

public function insertTokenUser($id)  
{    
  $token = substr(sha1(rand()), 0, 30);   
  $date = date('Y-m-d');  
    
  $string = array(  
      'token'=> $token,  
      'id'=>$id,  
      'created_at'=>$date  
    );  
  $query = $this->db->insert_string($this->useradminToken,$string);  
  $this->db->query($query);  
  return $token . $id;  
    
}  

public function isTokenValidUser($token)  
{  
  $tkn = substr($token,0,30);  
  $uid = substr($token,30);     
    
  $q = $this->db->get_where($this->useradminToken, array(  
    'em_useradmin_token.token' => $tkn,   
    'em_useradmin_token.id' => $uid), 1);               
        
  if($this->db->affected_rows() > 0){  
    $row = $q->row();         
      
    $created = $row->created_at;  
    $createdTS = strtotime($created);  
    $today = date('Y-m-d');   
    $todayTS = strtotime($today);  
      
    if($createdTS != $todayTS){  
      return false;  
    }  
      
    $user_info = $this->getUserInfo($row->id);  
    return $user_info;  
      
  }else{  
    return false;  
  }  
    
}   

public function updatePasswordUser($post)  
{    
  $this->db->where('id', $post['id']);  
  $this->db->update('em_useradmins', array('password' => $post['password']));      
  return true;  
}   


 
}

/* End of file Mod_user.php */
