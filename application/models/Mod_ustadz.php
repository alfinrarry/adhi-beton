<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_ustadz extends CI_Model {

  protected $ustadz 		 = 'ustadz';   
  protected $useradmin       = 'em_useradmins';
  protected $useradminDetail = 'em_useradmin_details';

  public function logged_id(){ 
    return $this->session->userdata('id'); 
  }
  //fungsi check login 
  public function check_login($data_user){ 
    $this->db->where("(username='".$data_user['email']."' OR email='".$data_user['email']."' )"); 
    $query = $this->db->get($this->user); 
    if ($query->num_rows() == 1) { 
      $hash = $query->row('password'); 
      if (password_verify($data_user['password'],$hash)){ 
        return $query->result(); 
      } else { 
        $this->session->set_flashdata('fail_msg_password', 'Password Salah');
      } 
    } else { 
      $this->session->set_flashdata('fail_msg_account', 'Account Tidak tersedia');
    } 
  } 
 
  public function create_ustadz($data_ustadz){
    return $this->db->insert($this->ustadz, $data_ustadz); 
  } 
  
  public  function checkEmail($email){
    $this->db->where('email',$email);
    return  $this->db->get($this->user);
  }
  public  function checkEmailAdmin($email){
    $this->db->where('email',$email);
    return  $this->db->get($this->useradmin);
  }

  //Ambil Data Kota
  public function getListUstadz(){
    $this->db->from($this->ustadz);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Kota

   //Ambil Data Institusi
   public function getListInstitusi(){
    $this->db->from($this->institusi);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Institusi

  //Ambil Data Provinsi
  public function getListProvinsi(){
    $this->db->from($this->provinsi);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Provinsi

  //Ambil Data Nama Komplek
  public function getListNamaKomplek(){
    $this->db->from($this->komplek);
    $this->db->select('nama_komplek');
    return $this->db->get();
  }
  //End Ambil Data Nama Komplek


  //Ambil Data Komplek
  public function getListKomplek(){
    $this->db->from($this->komplek);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Komplek

  //Ambil Data Kelas
  public function getListKelas(){
    $this->db->from($this->kelas);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Kelas

   //Ambil Data Status Santri
   public function getListStatus(){
    $this->db->from($this->status_santri);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Status Santri

  //Ambil Data Lengkap Santri
  public function getListSantri(){
    $this->db->from($this->santri);
    $this->db->join($this->komplek, $this->komplek.'.id_komplek = '.$this->santri.'.id_komplek');  
    $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->santri.'.id_kelas');  
    $this->db->select($this->santri.'.*,'.$this->santri.'.id_komplek ,'.$this->komplek.'.nama_komplek,'.$this->kelas.'.kelas');
    return $this->db->get();
  }
  //End Ambil Data Lengkap Santri

  //Ambil Data Ustadz By ID

  public function getUstadzById($id){
    $this->db->where('id_ustadz',$id);
    $this->db->select('*');
    $this->db->from($this->ustadz);
    return $this->db->get();
  
  }

  //End Ambil Data Ustadz By ID
  
  //Update Foto Santri
  public function updateImgSantri($img){
    $this->db->set($img);
    $this->db->where('nis', $_SESSION['id']);
    $this->db->update($this->santri);    
  }
  //End Update Foto Santri
 

  //Ambil Data Foto
  public function getFoto($id){
    $this->db->from($this->santri);
    $this->db->where($this->santri.'.nis', $id);
    $this->db->select('foto');
    return $this->db->get();
  }
  //End Data Foto

  //Delete Data Santri

  public function delUstadz($id){
    $this ->db-> where('id_ustadz', $id);
    $this ->db-> delete($this->ustadz);
}

//End Delete Data Santri

//Update Data Santri

public function updateDataUstadz($id=0 , $data=0){
  $this->db->set($data);
  $this->db->where('id_ustadz', $id);
  $this->db->update($this->ustadz); 
}

//End Update Data Santri




  // admin
  public function checkLoginAdmin($data_user){ 
    $this->db->select($this->useradmin.'.*,'.$this->useradmin.'.id as idu ,'.$this->useradminDetail.'.*');
    $this->db->from($this->useradmin);    
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin ='.$this->useradmin.'.id');
    $this->db->where($this->useradmin.'.username', $data_user['email']);
    $this->db->or_where($this->useradmin.'.email', $data_user['email']);
    $query = $this->db->get();
    if ($query->num_rows() == 1) { 
      $hash = $query->row('password'); 
      if (password_verify($data_user['password'],$hash)){ 
        return $query->result(); 
      } else { 
        $this->session->set_flashdata('fail_msg_password', 'Password Salah');
      } 
    } else { 
      $this->session->set_flashdata('fail_msg_account', 'Account Tidak tersedia');
    } 
  }


 
 
  public function getUserAdmin(){
    $this->db->select($this->useradmin.'.username,'.$this->useradmin.'.id,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    return $this->db->get();
  }
 
 
  public function getUserAdminById($id){
    $this->db->select($this->useradmin.'.id,'.$this->useradmin.'.username,'.$this->useradmin.'.is_ban,'.$this->useradmin.'.email,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role,'.$this->santri.'.*,');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    $this->db->join($this->santri, $this->santri.'.nis = '.$this->useradmin.'.id_useradmin');
    $this->db->where($this->useradmin.'.id', $id);
    return $this->db->get();
  }
  public function getId($username){
    $this->db->where('username',$username);
    $this->db->select('id');
    $this->db->from($this->useradmin);
    return $this->db->get($this->$useradmin);
  }


 
}

/* End of file Mod_user.php */
