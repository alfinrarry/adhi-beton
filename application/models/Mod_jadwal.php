<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_jadwal extends CI_Model {

  protected $pelajaran 		 = 'pelajaran';
  protected $jadwal 		 = 'jadwal';
  protected $kelas 		     = 'kelas';
  protected $hari 		     = 'hari';
  protected $ustadz 		 = 'ustadz';
  protected $useradmin       = 'em_useradmins';
  protected $useradminDetail = 'em_useradmin_details';

  public function logged_id(){ 
    return $this->session->userdata('id'); 
  }
  //fungsi check login 
  public function check_login($data_user){ 
    $this->db->where("(username='".$data_user['email']."' OR email='".$data_user['email']."' )"); 
    $query = $this->db->get($this->user); 
    if ($query->num_rows() == 1) { 
      $hash = $query->row('password'); 
      if (password_verify($data_user['password'],$hash)){ 
        return $query->result(); 
      } else { 
        $this->session->set_flashdata('fail_msg_password', 'Password Salah');
      } 
    } else { 
      $this->session->set_flashdata('fail_msg_account', 'Account Tidak tersedia');
    } 
  } 
  
 //Create Data Jadwal
  public function create_jadwal($data_jadwal){
    return $this->db->insert($this->jadwal, $data_jadwal); 
  } 
 //End Create Data Jadwal

 function pencarian_jadwal($kelas){
  $this->db->select($this->jadwal.'.*,'.$this->pelajaran.'.nama_pelajaran,'.$this->hari.'.nama_hari,'.$this->kelas.'.kelas,'.$this->ustadz.'.nama_ustadz');
  $this->db->join($this->hari, $this->hari.'.id_hari = '.$this->jadwal.'.id_hari');  
  $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->jadwal.'.id_kelas');  
  $this->db->join($this->ustadz, $this->ustadz.'.id_ustadz = '.$this->jadwal.'.id_ustadz');
  $this->db->join($this->pelajaran, $this->pelajaran.'.id_pelajaran = '.$this->jadwal.'.id_pelajaran');    
  $this->db->from($this->jadwal);
  $this->db->where($this->jadwal.'.id_kelas', $kelas);
  return $this->db->get();
  
  // $this->db->where("id_kelas",$kelas);
  // return $this->db->get("t_santri");
  } 

//Update Data Jadwal
 public function updateDataJadwal($id=0 , $data=0){
    $this->db->set($data);
    $this->db->where('id_jadwal', $id);
    $this->db->update($this->jadwal); 
  }
  //End Update Data Jadwal

  //Delete Jadwal
  public function delJadwal($id){
    $this ->db-> where('id_jadwal', $id);
    $this ->db-> delete($this->jadwal);
}
    //End Delete Jadwal


  //Ambil Data Lengkap Jadwal
  public function getListJadwal(){
    $this->db->from($this->jadwal);
    $this->db->join($this->hari, $this->hari.'.id_hari = '.$this->jadwal.'.id_hari');  
    $this->db->join($this->pelajaran, $this->pelajaran.'.id_pelajaran = '.$this->jadwal.'.id_pelajaran'); 
    $this->db->join($this->ustadz, $this->ustadz.'.id_ustadz = '.$this->jadwal.'.id_ustadz');   
    $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->jadwal.'.id_kelas');  
    $this->db->select($this->jadwal.'.*,'.$this->jadwal.'.id_hari ,'.$this->hari.'.nama_hari,'.$this->kelas.'.kelas,'.$this->ustadz.'.nama_ustadz,'.$this->pelajaran.'.nama_pelajaran');
    return $this->db->get();
  }
  //End Ambil Data Lengkap Jadwal

//Get Jadwal By Id
  public function getJadwalById($id){
    $this->db->select($this->jadwal.'.*,'.$this->jadwal.'.id_hari ,'.$this->hari.'.nama_hari,'.$this->kelas.'.kelas,'.$this->ustadz.'.nama_ustadz,'.$this->pelajaran.'.nama_pelajaran');
    $this->db->join($this->hari, $this->hari.'.id_hari = '.$this->jadwal.'.id_hari');  
    $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->jadwal.'.id_kelas');  
    $this->db->join($this->ustadz, $this->ustadz.'.id_ustadz = '.$this->jadwal.'.id_ustadz');  
    $this->db->join($this->pelajaran, $this->pelajaran.'.id_pelajaran = '.$this->jadwal.'.id_pelajaran');  
    $this->db->from($this->jadwal);
    $this->db->where($this->jadwal.'.id_jadwal', $id);
    return $this->db->get();
  }
  //End Get Jadwal By Id

  function cari_jadwal($kelas){
    $this->db->from($this->jadwal);
    $this->db->select($this->jadwal.'.*,'.$this->kelas.'.kelas,'.$this->pelajaran.'.*,'.$this->ustadz.'.*,'.$this->hari.'.*');
    $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->jadwal.'.id_kelas');  
    $this->db->join($this->pelajaran, $this->pelajaran.'.id_pelajaran = '.$this->jadwal.'.id_pelajaran');  
    $this->db->join($this->ustadz, $this->ustadz.'.id_ustadz = '.$this->jadwal.'.id_ustadz');  
    $this->db->join($this->hari, $this->hari.'.id_hari = '.$this->jadwal.'.id_hari');  
    $this->db->where($this->jadwal.'.id_kelas', $kelas);
    return $this->db->get();
  }


 


  //Ambil Data Pelajaran
  public function getListPelajaran(){
    $this->db->from($this->pelajaran);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Pelajaran

   //Ambil Data Pelajaran By ID
   public function getPelajaranById($id){
    $this->db->where('id_pelajaran',$id);
    $this->db->select('*');
    $this->db->from($this->pelajaran);
    return $this->db->get();
  }
  //End Ambil Data Pelajaran By ID

 
  //Ambil Data Nama Komplek
  public function getListNamaKomplek(){
    $this->db->from($this->komplek);
    $this->db->select('nama_komplek');
    return $this->db->get();
  }
  //End Ambil Data Nama Komplek


  //Ambil Data Komplek
  public function getListKomplek(){
    $this->db->from($this->komplek);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Komplek

  //Ambil Data Kelas
  public function getListKelas(){
    $this->db->from($this->kelas);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Kelas

   //Ambil Data Status Santri
   public function getListStatus(){
    $this->db->from($this->status_santri);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Status Santri

  //Ambil Data Lengkap Santri
  public function getListSantri(){
    $this->db->from($this->santri);
    $this->db->join($this->komplek, $this->komplek.'.id_komplek = '.$this->santri.'.id_komplek');  
    $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->santri.'.id_kelas');  
    $this->db->select($this->santri.'.*,'.$this->santri.'.id_komplek ,'.$this->komplek.'.nama_komplek,'.$this->kelas.'.kelas');
    return $this->db->get();
  }
  //End Ambil Data Lengkap Santri

  //Ambil Data Santri By NIS

  public function getSantriByNis($id){
    $this->db->select($this->santri.'.*,'.$this->santri.'.id_komplek ,'.$this->komplek.'.nama_komplek,'.$this->kelas.'.kelas,'.$this->institusi.'.nama_institusi,'.$this->status_santri.'.status_santri');
    $this->db->join($this->komplek, $this->komplek.'.id_komplek = '.$this->santri.'.id_komplek');  
    $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->santri.'.id_kelas');  
    $this->db->join($this->status_santri, $this->status_santri.'.id_status = '.$this->santri.'.id_status');  
    $this->db->join($this->institusi, $this->institusi.'.id_institusi = '.$this->santri.'.profesi');  
    $this->db->from($this->santri);
    $this->db->where($this->santri.'.nis', $id);
    return $this->db->get();
  }

  //End Ambil Data Santri By NIS
  
  //Update Foto Santri
  public function updateImgSantri($img){
    $this->db->set($img);
    $this->db->where('nis', $_SESSION['id']);
    $this->db->update($this->santri);    
  }
  //End Update Foto Santri
 

  //Ambil Data Foto
  public function getFoto($id){
    $this->db->from($this->santri);
    $this->db->where($this->santri.'.nis', $id);
    $this->db->select('foto');
    return $this->db->get();
  }
  //End Data Foto

  //Delete Data Santri

  public function delPelajaran($id){
    $this ->db-> where('id_pelajaran', $id);
    $this ->db-> delete($this->pelajaran);
}

//End Delete Data Santri

//Update Data Pelajaran

public function updateDataPelajaran($id=0 , $data=0){
  $this->db->set($data);
  $this->db->where('id_pelajaran', $id);
  $this->db->update($this->pelajaran); 
}

//End Update Data Pelajaran




  // admin
  public function checkLoginAdmin($data_user){ 
    $this->db->select($this->useradmin.'.*,'.$this->useradmin.'.id as idu ,'.$this->useradminDetail.'.*');
    $this->db->from($this->useradmin);    
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin ='.$this->useradmin.'.id');
    $this->db->where($this->useradmin.'.username', $data_user['email']);
    $this->db->or_where($this->useradmin.'.email', $data_user['email']);
    $query = $this->db->get();
    if ($query->num_rows() == 1) { 
      $hash = $query->row('password'); 
      if (password_verify($data_user['password'],$hash)){ 
        return $query->result(); 
      } else { 
        $this->session->set_flashdata('fail_msg_password', 'Password Salah');
      } 
    } else { 
      $this->session->set_flashdata('fail_msg_account', 'Account Tidak tersedia');
    } 
  }

  public function addUser($data)
  {
    $this->db->insert($this->useradmin, $data);
    return $this->db->insert_id();
  }

  public function getListUser(){
    $this->db->from($this->useradminDetail);
    $this->db->join($this->useradmin, $this->useradmin.'.id = '.$this->useradminDetail.'.id_useradmin');  
    $this->db->join($this->userAdminRole,$this->useradmin.'.id_role = '.$this->userAdminRole.'.id');
    $this->db->select('*');
    return $this->db->get();
  }

  public function getUser($id=0){
    $this->db->select($this->user.'.*,'.$this->userDetail.'.*');
    $this->db->from($this->user);
    $this->db->join($this->userDetail, $this->userDetail.'.id_user = '.$this->user.'.id');
    $this->db->where($this->user.'.id',$id);
    return $this->db->get();
  }
 

  public function getRole(){
    
    $this->db->from($this->useradmin);
    $this->db->join($this->userAdminRole,$this->useradmin.'.id_role = '.$this->userAdminRole.'.id');
    $this->db->select($this->useradmin.'.id');
    $this->db->select($this->useradmin.'.fullname');
    $this->db->select($this->userAdminRole.'.name');
    return $this->db->get();
  }

  public function getAllRole(){
    $this->db->select('*');
    $this->db->from($this->userAdminRole);
    return $this->db->get();
  }
  public function getImage($id){
    $this->db->from($this->useradminDetail);
    $this->db->join($this->useradmin, $this->useradmin.'.id = '.$this->useradminDetail.'.id_useradmin');
    $this->db->where($this->useradmin.'.id', $id);
    $this->db->select('img_path');
    return $this->db->get();
  }
  public function getImg($id){
    $this->db->join($this->useradmin, $this->useradmin.'.id = '.$this->useradminDetail.'.id_useradmin');
    $this->db->where($this->useradminDetail.'.id', $id);
    return $this->db->get($this->useradminDetail);
  }

  
  
  public function updateImg($img){
    $this->db->set($img);
    $this->db->where('id_user', $_SESSION['id']);
    $this->db->update($this->userDetail);    
  }
  public function updateData($data_user){
    $this->db->set($data_user);
    $this->db->where('id_user', $_SESSION['id']);
    $this->db->update($this->userDetail); 
  }
  public function updateImgAdmin($img){
    $this->db->set($img);
    $this->db->where('id_useradmin', $_SESSION['id']);
    $this->db->update($this->useradminDetail);    
  }

  public function updateDataAdmin($id=0 , $data=0){
    $this->db->set($data);
    $this->db->where('id', $id);
    $this->db->update($this->useradmin); 
  }

  public function updateDataAdminDetail($id=0 , $data=0){
    $this->db->set($data);
    $this->db->where('id_useradmin', $id);
    $this->db->update($this->useradminDetail); 
  }

  public function updateEmail($email){
    $this->db->set($email);
    $this->db->where('id', $_SESSION['id']);
    $this->db->update($this->user); 
  }
  public function getUserInfoByEmail($email){
    $this->db->select($this->user.'.*,'.$this->userDetail.'.*');
    $this->db->from($this->userDetail);
    $this->db->join($this->user, $this->user.'.id = '.$this->userDetail.'.id_user');
    $this->db->where($this->user.'.email', $email);
    $q = $this->db->get();  
      if($this->db->affected_rows() > 0){  
          $row = $q->row();  
          return $row;  
      }  
  }
  public function getUserByEmail($email){
    $this->db->select($this->user.'.*, '.$this->userDetail.'.*,'.$this->userRole.'.*');
    $this->db->from($this->user);
    $this->db->join($this->userDetail, $this->userDetail.'.id_user'.'='.$this->user.'.id', 'left');
    $this->db->join($this->userRole, $this->userRole.'.id'.'='.$this->user.'.id_role', 'left');   
    $this->db->where($this->user.'.email', $email); 
    return $this->db->get();
}
public function getUserAdminByEmail($email){
  $this->db->select($this->useradmin.'.*, '.$this->useradminDetail.'.*,'.$this->userAdminRole.'.*');
  $this->db->from($this->useradmin);
  $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin'.'='.$this->useradmin.'.id', 'left');
  $this->db->join($this->userAdminRole, $this->userAdminRole.'.id'.'='.$this->useradmin.'.id_role', 'left');   
  $this->db->where($this->useradmin.'.email', $email); 
  return $this->db->get();
}
  public function insertToken($email)  
  {    
    $token = substr(sha1(rand()), 0, 30);
    $user = $this->getUserByEmail($email)->row_array();     
    $string = array( 
        'id_user'    =>$user['id_user'], 
        'token_reset'=> $token,  
        'email'=>$email,
        'status'=>0,
        'created_at' =>date('Y-m-d H:m:s')
      );  
    $query = $this->db->insert_string($this->userReset,$string);  
    $this->db->query($query);  
    return $token . $email;  
  }
  public function isTokenValid($token)  
  {  
    $tkn = substr($token,0,30);  
    $email = substr($token,30);   
    // $this->db->where('token_reset', $tkn);
    // $this->db->where('email', $email);
    // $q = $this->db->get($this->userReset, 1);
    
    $q = $this->db->get_where($this->userReset, array(  
      $this->userReset.'.token_reset' => $tkn,   
      $this->userReset.'.email' => $email, 
      'status'=> 0), 1);               
      
    if($this->db->affected_rows() > 0){  
      $row = $q->row();         
        
      $created = $row->created_at;  
      $createdTS = strtotime($created);  
      $expired = date('Y-m-d H:i:s',strtotime('+4 hour',strtotime($created)));   
      $exp = strtotime($expired);  
        
      if($createdTS > $exp){  
        return false;  
      }  
      $user_info = $this->getUserInfoByEmail($row->email);  
      return $user_info;  
        
    }else{  
      return false;  
    }  
  }
  public function updatePassword($cleanPost)  
  {    
    $this->db->where('id', $cleanPost['id_user']);  
    $this->db->update($this->user, array('password' => $cleanPost['password']));     
    return true;
  }
  public function updateToken($cleanPost){ 
    $this->db->where('email', $cleanPost['email']);  
    $this->db->update($this->userReset, array('status' => 1));
    return true;  
  }
  public function countMember(){
    // $this->db->where('deleted_at !=', 000);
    $this->db->from($this->user);
    return $this->db->count_all_results();
  }
  public function getUserAdmin(){
    $this->db->select($this->useradmin.'.username,'.$this->useradmin.'.id,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    return $this->db->get();
  }
  public function banadmin($id){
    $this->db->set('is_ban',0);
    $this->db->where('id', $id);
    $this->db->update($this->useradmin); 
    redirect(base_url('admin/user/listUser'));
  }
  public function aktifadmin($id){
    $this->db->set('is_ban',1);
    $this->db->where('id', $id);
    $this->db->update($this->useradmin); 
    redirect(base_url('admin/user/listUser'));
  }
  public function getUserAdminByUsername($username){
    $this->db->select($this->useradmin.'.username,'.$this->useradmin.'.id,'.$this->useradmin.'.is_ban,'.$this->useradmin.'.email,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    $this->db->where($this->useradmin.'.username', $username);
    return $this->db->get();
  }
  public function getUserAdminById($id){
    $this->db->select($this->useradmin.'.id,'.$this->useradmin.'.username,'.$this->useradmin.'.is_ban,'.$this->useradmin.'.email,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role,'.$this->santri.'.*,');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    $this->db->join($this->santri, $this->santri.'.nis = '.$this->useradmin.'.id_useradmin');
    $this->db->where($this->useradmin.'.id', $id);
    return $this->db->get();
  }
  public function getId($username){
    $this->db->where('username',$username);
    $this->db->select('id');
    $this->db->from($this->useradmin);
    return $this->db->get($this->$useradmin);
  }

  public function deluserAdmin($id){
    $this ->db-> where('id', $id);
    $this ->db-> delete($this->useradmin);
}
public function deluserAdminDetail($id_useradmin){
  $this ->db-> where('id_useradmin', $id_useradmin);
  $this ->db-> delete($this->useradminDetail);
}
public function getUserInfo($id)  
{  
  $q = $this->db->get_where($this->useradmin, array('id' => $id), 1);   
  if($this->db->affected_rows() > 0){  
    $row = $q->row();  
    return $row;  
  }else{  
    error_log('No user found getUserInfo('.$id.')');  
    return false;  
  }  
}  
public function getUserInfoByMailUser($email){  
  $q = $this->db->get_where($this->useradmin, array('email' => $email), 1);   
  if($this->db->affected_rows() > 0){  
    $row = $q->row();  
    return $row;  
  }  
}  

public function insertTokenUser($id)  
{    
  $token = substr(sha1(rand()), 0, 30);   
  $date = date('Y-m-d');  
    
  $string = array(  
      'token'=> $token,  
      'id'=>$id,  
      'created_at'=>$date  
    );  
  $query = $this->db->insert_string($this->useradminToken,$string);  
  $this->db->query($query);  
  return $token . $id;  
    
}  

public function isTokenValidUser($token)  
{  
  $tkn = substr($token,0,30);  
  $uid = substr($token,30);     
    
  $q = $this->db->get_where($this->useradminToken, array(  
    'em_useradmin_token.token' => $tkn,   
    'em_useradmin_token.id' => $uid), 1);               
        
  if($this->db->affected_rows() > 0){  
    $row = $q->row();         
      
    $created = $row->created_at;  
    $createdTS = strtotime($created);  
    $today = date('Y-m-d');   
    $todayTS = strtotime($today);  
      
    if($createdTS != $todayTS){  
      return false;  
    }  
      
    $user_info = $this->getUserInfo($row->id);  
    return $user_info;  
      
  }else{  
    return false;  
  }  
    
}   

public function updatePasswordUser($post)  
{    
  $this->db->where('id', $post['id']);  
  $this->db->update('em_useradmins', array('password' => $post['password']));      
  return true;  
}   


 
}

/* End of file Mod_user.php */
