<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_absensi extends CI_Model {

  protected $santri 		   = 't_santri';
  protected $komplek 		   = 'komplek';
  protected $institusi 		 = 'institusi';
  protected $kelurahan 		 = 'kelurahan';
  protected $kecamatan 		 = 'kecamatan';
  protected $kota 		     = 'kota';
  protected $provinsi 		 = 'provinsi';
  protected $kelas 		     = 'kelas';
  protected $absensi       = 'absensi';
  protected $bulan         = 'bulan';
  protected $tahun_ajaran  = 'tahun_ajaran';
  protected $semester      = 'semester';
  protected $rekap_absensi = 'rekap_absensi';




  public function addRekapAbsensiSantri($data){
    return $this->db->insert($this->rekap_absensi, $data);
  } 

  public function getListAbsen(){
    $this->db->from($this->absensi);
    $this->db->select('*');
    return $this->db->get();
  }

  public function updateAbsensiSantri($nis=0 , $data=0){
    $this->db->set($data);
    $this->db->where('nis', $nis);
    $this->db->update($this->absensi); 
  }

  function pencarian_absen($kelas){
    $this->db->from($this->santri);
    $this->db->select($this->santri.'.*,'.$this->kelas.'.kelas,'.$this->absensi.'.*');
    $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->santri.'.id_kelas');  
    $this->db->join($this->absensi, $this->absensi.'.nis = '.$this->santri.'.nis');  
    $this->db->where($this->santri.'.id_kelas', $kelas);
    return $this->db->get();
  }

  public function getKelasById($id){
    $this->db->from($this->kelas);
    $this->db->select('kelas');
    $this->db->where('id_kelas',$id);
    return $this->db->get();

}

public function getSemester(){
  $this->db->from($this->semester);
  $this->db->select('*');
  return $this->db->get();
}

public function getTahunAjaran(){
  $this->db->from($this->tahun_ajaran);
  $this->db->select('*');
  return $this->db->get();
}

public function getBulan(){
  $this->db->from($this->bulan);
  $this->db->select('*');
  return $this->db->get();
}

public function getAbsensiByNis($nis){
  $this->db->from($this->santri);
  $this->db->select($this->santri.'.*,'.$this->kelas.'.kelas,'.$this->absensi.'.*');
  $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->santri.'.id_kelas');  
  $this->db->join($this->absensi, $this->absensi.'.nis = '.$this->santri.'.nis');  
  $this->db->where($this->santri.'.nis', $nis);
    return $this->db->get();
  }


 
}

/* End of file Mod_user.php */
