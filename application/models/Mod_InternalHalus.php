<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_InternalHalus extends CI_Model {

  protected $userDetail 	           = 'em_user_details';
  protected $useradmin                 = 'em_useradmins';
  protected $useradminDetail           = 'em_useradmin_details';
  protected $internalTestHalus         = 'em_internal_tes_halus';
  protected $sampleSource              = 'em_sample_source';
  protected $sampleDescription         = 'em_sample_description';
  protected $supplier                  = 'em_supplier';
  protected $pelaksana                 = 'em_pelaksana';
  protected $toBeUsed                  = 'em_to_be_used';
  protected $status                    = 'em_status';



  public function logged_id(){ 
    return $this->session->userdata('id'); 
  }
  //fungsi check login 
  public function check_login($data_user){ 
    $this->db->where("(nis='".$data_user['email']."' OR email='".$data_user['email']."' )"); 
    $query = $this->db->get($this->user); 
    if ($query->num_rows() == 1) { 
      $hash = $query->row('password'); 
      if (password_verify($data_user['password'],$hash)){ 
        return $query->result(); 
      } else { 
        $this->session->set_flashdata('fail_msg_password', 'Password Salah');
      } 
    } else { 
      $this->session->set_flashdata('fail_msg_account', 'Account Tidak tersedia');
    } 
  } 
 
//   Input Data Agregat Halus
  public function inputAgregatHalus($data_agregat_halus){
    return $this->db->insert($this->internalTestHalus, $data_agregat_halus); 
  } 
// End  Input Data Agregat Halus


    //Ambil Data Internal Halus
    public function getInternalHalus(){
        $this->db->from($this->internalTestHalus);
        $this->db->join($this->sampleSource, $this->sampleSource.'.id_sample_source = '.$this->internalTestHalus.'.id_sample_source');  
        $this->db->join($this->pelaksana, $this->pelaksana.'.id_pelaksana = '.$this->internalTestHalus.'.id_pelaksana');
        $this->db->join($this->status, $this->status.'.id_status = '.$this->internalTestHalus.'.id_status');  
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Internal Halus

      //Ambil Data Internal Halus BY ID
    public function getInternalHalusById($id){
        $this->db->from($this->internalTestHalus);
        $this->db->join($this->sampleSource, $this->sampleSource.'.id_sample_source = '.$this->internalTestHalus.'.id_sample_source');  
        $this->db->join($this->sampleDescription, $this->sampleDescription.'.id_sample_description = '.$this->internalTestHalus.'.id_sample_description');  
        $this->db->join($this->pelaksana, $this->pelaksana.'.id_pelaksana = '.$this->internalTestHalus.'.id_pelaksana');
        $this->db->join($this->toBeUsed, $this->toBeUsed.'.id_to_be_used = '.$this->internalTestHalus.'.id_to_be_used');
        $this->db->join($this->status, $this->status.'.id_status = '.$this->internalTestHalus.'.id_status');  
        $this->db->where($this->internalTestHalus.'.id_internal_tes_halus', $id);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Internal Halus By ID

  

       //Ambil Data Sample Source
    public function getSampleSource(){
        $this->db->from($this->sampleSource);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Sample Source
      
       //Ambil Data Sample Description
    public function getSampleDescription(){
        $this->db->from($this->sampleDescription);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Sample Description


      //Ambil Data Pelaksana
    public function getPelaksana(){
        $this->db->from($this->pelaksana);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Pelaksana

      //Ambil Data To Be Used
    public function getToBeUsed(){
        $this->db->from($this->toBeUsed);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Pelaksana

      //Ambil Data Status
    public function getStatus(){
        $this->db->from($this->status);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Status

      //Ambil Data Visual Bersih By Id
      public function getBersihById($id=0){
        $this->db->from($this->dailyIncomingAdditive);
        $this->db->where($this->dailyIncomingAdditive.'.id_daily_incoming_additive', $id);
        $this->db->select($this->dailyIncomingAdditive.'.kriteria_visual_bersih');
        return $this->db->get();
      }
      //End Ambil Data Visual Additive By Id


      //Ambil Data supplier By Id
      public function getsupplierById($id=0){
        $this->db->from($this->dailyIncomingAdditive);
        $this->db->join($this->supplier, $this->supplier.'.id_supplier = '.$this->dailyIncomingAdditive.'.id_supplier');  
        $this->db->where($this->dailyIncomingAdditive.'.id_supplier', $id);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data supplier By Id

      //Ambil Data Pelaksana By Id
      public function getPelaksanaById($id=0){
        $this->db->from($this->dailyIncomingAdditive);
        $this->db->join($this->pelaksana, $this->pelaksana.'.id_pelaksana = '.$this->dailyIncomingAdditive.'.id_pelaksana');
        $this->db->where($this->dailyIncomingAdditive.'.id_pelaksana', $id);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Pelaksana By Id

       //Ambil Data Status By Id
       public function getStatusById($id=0){
        $this->db->from($this->dailyIncomingAdditive);
        $this->db->join($this->status, $this->status.'.id_status = '.$this->dailyIncomingAdditive.'.id_status'); 
        $this->db->where($this->dailyIncomingAdditive.'.id_status', $id);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Status By Id



        //Delete Data Daily Incoming Halus

    public function delInternalHalus($id){
      $this->db->from($this->internalTestHalus);    
      $this ->db-> where('id_internal_tes_halus', $id);
      $this ->db-> delete($this->internalTestHalus);
    }
    //Delete Data Daily Incoming Halus

// Update Image Incoming Additive
    public function updateImgIncomingHalus($img){
      $this->db->set($img);
      $this->db->where('id_daily_incoming_additive', $_SESSION['id']);
      $this->db->update($this->dailyIncomingAdditive);    
    }
// End Update Image Incoming Additive


    // Update Data Incoming Additive
    public function updateIncomingAdditive($id=0 , $data=0){
      $this->db->set($data);
      $this->db->where('id_daily_incoming_additive', $id);
      $this->db->update($this->dailyIncomingAdditive); 
    }
     // End Update Data Incoming Additive

       // Tolak Data Internal Halus
    public function tolakInternalHalus($id=0 , $data=0){
        $this->db->set($data);
        $this->db->where('id_internal_tes_halus', $id);
        $this->db->update($this->internalTestHalus); 
      }
       // End Tolak Data Internal Halus

          // Setuju Data Internal Halus
    public function setujuInternalHalus($id=0 , $data=0){
        $this->db->set($data);
        $this->db->where('id_internal_tes_halus', $id);
        $this->db->update($this->internalTestHalus); 
      }
       // End Setuju Data Internal Halus

  
  public  function checkEmail($email){
    $this->db->where('email',$email);
    return  $this->db->get($this->user);
  }
  public  function checkEmailAdmin($email){
    $this->db->where('email',$email);
    return  $this->db->get($this->useradmin);
  }

  

 
  
  //Update Foto Santri
  public function updateImgSantri($img){
    $this->db->set($img);
    $this->db->where('nis', $_SESSION['id']);
    $this->db->update($this->santri);    
  }
  //End Update Foto Santri
 

  //Ambil Data Foto
  public function getFoto($id){
    $this->db->from($this->santri);
    $this->db->where($this->santri.'.nis', $id);
    $this->db->select('foto');
    return $this->db->get();
  }
  //End Data Foto




public function delUserSantri($id){
  $this->db->from($this->user);    
  $this ->db-> where('nis', $id);
  $this ->db-> delete($this->user);
}

public function delUserDetailSantri($id){
  $this->db->from($this->userDetail);    
  $this ->db-> where('nis', $id);
  $this ->db-> delete($this->userDetail);
}

//End Delete Data Santri

//Update Data Santri

public function updateDataSantri($nis=0 , $data=0){
  $this->db->set($data);
  $this->db->where('nis', $nis);
  $this->db->update($this->santri); 
}

public function updateDataNilai($nis=0 , $data=0){
  $this->db->set($data);
  $this->db->where('nis', $nis);
  $this->db->update($this->nilai); 
}

public function updateDataAbsensi($nis=0 , $data=0){
  $this->db->set($data);
  $this->db->where('nis', $nis);
  $this->db->update($this->absensi); 
}

public function updateDataUserSantri($nis=0 , $data=0){
  $this->db->set($data);
  $this->db->where('nis', $nis);
  $this->db->update($this->user); 
}

//End Update Data Santri




  // admin
  public function checkLoginAdmin($data_user){ 
    $this->db->select($this->useradmin.'.*,'.$this->useradmin.'.id as idu ,'.$this->useradminDetail.'.*');
    $this->db->from($this->useradmin);    
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin ='.$this->useradmin.'.id');
    $this->db->where($this->useradmin.'.username', $data_user['email']);
    $this->db->or_where($this->useradmin.'.email', $data_user['email']);
    $query = $this->db->get();
    if ($query->num_rows() == 1) { 
      $hash = $query->row('password'); 
      if (password_verify($data_user['password'],$hash)){ 
        return $query->result(); 
      } else { 
        $this->session->set_flashdata('fail_msg_password', 'Password Salah');
      } 
    } else { 
      $this->session->set_flashdata('fail_msg_account', 'Account Tidak tersedia');
    } 
  }

  public function addUser($data)
  {
    $this->db->insert($this->useradmin, $data);
    return $this->db->insert_id();
  }

  public function getListUser(){
    $this->db->from($this->useradminDetail);
    $this->db->join($this->useradmin, $this->useradmin.'.id = '.$this->useradminDetail.'.id_useradmin');  
    $this->db->join($this->userAdminRole,$this->useradmin.'.id_role = '.$this->userAdminRole.'.id');
    $this->db->select('*');
    return $this->db->get();
  }

  public function getUser($id=0){
    $this->db->select($this->user.'.*,'.$this->userDetail.'.*');
    $this->db->from($this->user);
    $this->db->join($this->userDetail, $this->userDetail.'.id_user = '.$this->user.'.id');
    $this->db->where($this->user.'.id',$id);
    return $this->db->get();
  }
 

  public function getRole(){
    
    $this->db->from($this->useradmin);
    $this->db->join($this->userAdminRole,$this->useradmin.'.id_role = '.$this->userAdminRole.'.id');
    $this->db->select($this->useradmin.'.id');
    $this->db->select($this->useradmin.'.fullname');
    $this->db->select($this->userAdminRole.'.name');
    return $this->db->get();
  }

  public function getAllRole(){
    $this->db->select('*');
    $this->db->from($this->userAdminRole);
    return $this->db->get();
  }
  public function getImage($id){
    $this->db->from($this->useradminDetail);
    $this->db->join($this->useradmin, $this->useradmin.'.id = '.$this->useradminDetail.'.id_useradmin');
    $this->db->where($this->useradmin.'.id', $id);
    $this->db->select('img_path');
    return $this->db->get();
  }
  public function getImg($id){
    $this->db->join($this->useradmin, $this->useradmin.'.id = '.$this->useradminDetail.'.id_useradmin');
    $this->db->where($this->useradminDetail.'.id', $id);
    return $this->db->get($this->useradminDetail);
  }

  public function getUserEmail($id){
    $this->db->select($this->user.'email');
    $this->db->from($this->user);
    $this->db->where($this->user.'.nis', $id);
    return $this->db->get($this->user);

        
  }

  
  
  public function updateImg($img){
    $this->db->set($img);
    $this->db->where('id_user', $_SESSION['id']);
    $this->db->update($this->userDetail);    
  }
  public function updateData($data_user){
    $this->db->set($data_user);
    $this->db->where('id_user', $_SESSION['id']);
    $this->db->update($this->userDetail); 
  }
  public function updateImgAdmin($img){
    $this->db->set($img);
    $this->db->where('id_useradmin', $_SESSION['id']);
    $this->db->update($this->useradminDetail);    
  }

  public function updateDataAdmin($id=0 , $data=0){
    $this->db->set($data);
    $this->db->where('id', $id);
    $this->db->update($this->useradmin); 
  }

  public function updateDataAdminDetail($id=0 , $data=0){
    $this->db->set($data);
    $this->db->where('id_useradmin', $id);
    $this->db->update($this->useradminDetail); 
  }

  public function updateEmail($email){
    $this->db->set($email);
    $this->db->where('id', $_SESSION['id']);
    $this->db->update($this->user); 
  }
  public function getUserInfoByEmail($email){
    $this->db->select($this->user.'.*,'.$this->userDetail.'.*');
    $this->db->from($this->userDetail);
    $this->db->join($this->user, $this->user.'.id = '.$this->userDetail.'.id_user');
    $this->db->where($this->user.'.email', $email);
    $q = $this->db->get();  
      if($this->db->affected_rows() > 0){  
          $row = $q->row();  
          return $row;  
      }  
  }
  public function getUserByEmail($email){
    $this->db->select($this->user.'.*, '.$this->userDetail.'.*,'.$this->userRole.'.*');
    $this->db->from($this->user);
    $this->db->join($this->userDetail, $this->userDetail.'.id_user'.'='.$this->user.'.id', 'left');
    $this->db->join($this->userRole, $this->userRole.'.id'.'='.$this->user.'.id_role', 'left');   
    $this->db->where($this->user.'.email', $email); 
    return $this->db->get();
}
public function getUserAdminByEmail($email){
  $this->db->select($this->useradmin.'.*, '.$this->useradminDetail.'.*,'.$this->userAdminRole.'.*');
  $this->db->from($this->useradmin);
  $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin'.'='.$this->useradmin.'.id', 'left');
  $this->db->join($this->userAdminRole, $this->userAdminRole.'.id'.'='.$this->useradmin.'.id_role', 'left');   
  $this->db->where($this->useradmin.'.email', $email); 
  return $this->db->get();
}
  public function insertToken($email)  
  {    
    $token = substr(sha1(rand()), 0, 30);
    $user = $this->getUserByEmail($email)->row_array();     
    $string = array( 
        'id_user'    =>$user['id_user'], 
        'token_reset'=> $token,  
        'email'=>$email,
        'status'=>0,
        'created_at' =>date('Y-m-d H:m:s')
      );  
    $query = $this->db->insert_string($this->userReset,$string);  
    $this->db->query($query);  
    return $token . $email;  
  }
  public function isTokenValid($token)  
  {  
    $tkn = substr($token,0,30);  
    $email = substr($token,30);   
    // $this->db->where('token_reset', $tkn);
    // $this->db->where('email', $email);
    // $q = $this->db->get($this->userReset, 1);
    
    $q = $this->db->get_where($this->userReset, array(  
      $this->userReset.'.token_reset' => $tkn,   
      $this->userReset.'.email' => $email, 
      'status'=> 0), 1);               
      
    if($this->db->affected_rows() > 0){  
      $row = $q->row();         
        
      $created = $row->created_at;  
      $createdTS = strtotime($created);  
      $expired = date('Y-m-d H:i:s',strtotime('+4 hour',strtotime($created)));   
      $exp = strtotime($expired);  
        
      if($createdTS > $exp){  
        return false;  
      }  
      $user_info = $this->getUserInfoByEmail($row->email);  
      return $user_info;  
        
    }else{  
      return false;  
    }  
  }
  public function updatePassword($cleanPost)  
  {    
    $this->db->where('id', $cleanPost['id_user']);  
    $this->db->update($this->user, array('password' => $cleanPost['password']));     
    return true;
  }
  public function updateToken($cleanPost){ 
    $this->db->where('email', $cleanPost['email']);  
    $this->db->update($this->userReset, array('status' => 1));
    return true;  
  }
  public function countMember(){
    // $this->db->where('deleted_at !=', 000);
    $this->db->from($this->user);
    return $this->db->count_all_results();
  }
  public function getUserAdmin(){
    $this->db->select($this->useradmin.'.username,'.$this->useradmin.'.id,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    return $this->db->get();
  }
  public function banadmin($id){
    $this->db->set('is_ban',0);
    $this->db->where('id', $id);
    $this->db->update($this->useradmin); 
    redirect(base_url('admin/user/listUser'));
  }
  public function aktifadmin($id){
    $this->db->set('is_ban',1);
    $this->db->where('id', $id);
    $this->db->update($this->useradmin); 
    redirect(base_url('admin/user/listUser'));
  }
  public function getUserAdminByUsername($username){
    $this->db->select($this->useradmin.'.username,'.$this->useradmin.'.id,'.$this->useradmin.'.is_ban,'.$this->useradmin.'.email,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    $this->db->where($this->useradmin.'.username', $username);
    return $this->db->get();
  }
  public function getUserAdminById($id){
    $this->db->select($this->useradmin.'.id,'.$this->useradmin.'.username,'.$this->useradmin.'.is_ban,'.$this->useradmin.'.email,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role,'.$this->santri.'.*,');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    $this->db->join($this->santri, $this->santri.'.nis = '.$this->useradmin.'.id_useradmin');
    $this->db->where($this->useradmin.'.id', $id);
    return $this->db->get();
  }
  public function getId($username){
    $this->db->where('username',$username);
    $this->db->select('id');
    $this->db->from($this->useradmin);
    return $this->db->get($this->$useradmin);
  }

  public function deluserAdmin($id){
    $this ->db-> where('id', $id);
    $this ->db-> delete($this->useradmin);
}
public function deluserAdminDetail($id_useradmin){
  $this ->db-> where('id_useradmin', $id_useradmin);
  $this ->db-> delete($this->useradminDetail);
}
public function getUserInfo($id)  
{  
  $q = $this->db->get_where($this->useradmin, array('id' => $id), 1);   
  if($this->db->affected_rows() > 0){  
    $row = $q->row();  
    return $row;  
  }else{  
    error_log('No user found getUserInfo('.$id.')');  
    return false;  
  }  
}  
public function getUserInfoByMailUser($email){  
  $q = $this->db->get_where($this->useradmin, array('email' => $email), 1);   
  if($this->db->affected_rows() > 0){  
    $row = $q->row();  
    return $row;  
  }  
}  

public function insertTokenUser($id)  
{    
  $token = substr(sha1(rand()), 0, 30);   
  $date = date('Y-m-d');  
    
  $string = array(  
      'token'=> $token,  
      'id'=>$id,  
      'created_at'=>$date  
    );  
  $query = $this->db->insert_string($this->useradminToken,$string);  
  $this->db->query($query);  
  return $token . $id;  
    
}  

public function isTokenValidUser($token)  
{  
  $tkn = substr($token,0,30);  
  $uid = substr($token,30);     
    
  $q = $this->db->get_where($this->useradminToken, array(  
    'em_useradmin_token.token' => $tkn,   
    'em_useradmin_token.id' => $uid), 1);               
        
  if($this->db->affected_rows() > 0){  
    $row = $q->row();         
      
    $created = $row->created_at;  
    $createdTS = strtotime($created);  
    $today = date('Y-m-d');   
    $todayTS = strtotime($today);  
      
    if($createdTS != $todayTS){  
      return false;  
    }  
      
    $user_info = $this->getUserInfo($row->id);  
    return $user_info;  
      
  }else{  
    return false;  
  }  
    
}   

public function updatePasswordUser($post)  
{    
  $this->db->where('id', $post['id']);  
  $this->db->update('em_useradmins', array('password' => $post['password']));      
  return true;  
}   


 
}

/* End of file Mod_user.php */
