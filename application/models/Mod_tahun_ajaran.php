<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_tahun_ajaran extends CI_Model {

  protected $tahun_ajaran 	 = 'tahun_ajaran';
  protected $useradmin       = 'em_useradmins';
  protected $useradminDetail = 'em_useradmin_details';

  public function logged_id(){ 
    return $this->session->userdata('id'); 
  }
  //fungsi check login 
  public function check_login($data_user){ 
    $this->db->where("(username='".$data_user['email']."' OR email='".$data_user['email']."' )"); 
    $query = $this->db->get($this->user); 
    if ($query->num_rows() == 1) { 
      $hash = $query->row('password'); 
      if (password_verify($data_user['password'],$hash)){ 
        return $query->result(); 
      } else { 
        $this->session->set_flashdata('fail_msg_password', 'Password Salah');
      } 
    } else { 
      $this->session->set_flashdata('fail_msg_account', 'Account Tidak tersedia');
    } 
  } 
 
  public function create_santri($data_santri){
    return $this->db->insert($this->santri, $data_santri); 
  } 

  public function create_nilai_santri($data_nilai){
    return $this->db->insert($this->nilai, $data_nilai); 
  } 

  public function updateNilaiSantri($nis=0 , $data=0){
    $this->db->set($data);
    $this->db->where('nis', $nis);
    $this->db->update($this->nilai); 
  }

  function pencarian_santri($kelas){
    $this->db->from($this->santri);
    $this->db->select($this->santri.'.*,'.$this->kelas.'.kelas,'.$this->nilai.'.*');
    $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->santri.'.id_kelas');  
    $this->db->join($this->nilai, $this->nilai.'.nis = '.$this->santri.'.nis');  
    $this->db->where($this->santri.'.id_kelas', $kelas);
    return $this->db->get();
    
    // $this->db->where("id_kelas",$kelas);
    // return $this->db->get("t_santri");
    } 

    function pencarian_nilai($kelas){
      $this->db->from($this->santri);
      $this->db->select($this->santri.'.*,'.$this->kelas.'.kelas,'.$this->nilai.'.*');
      $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->santri.'.id_kelas');  
      $this->db->join($this->nilai, $this->nilai.'.nis = '.$this->santri.'.nis');  
      $this->db->where($this->santri.'.id_kelas', $kelas);
      return $this->db->get();
      
      // $this->db->where("id_kelas",$kelas);
      // return $this->db->get("t_santri");
      } 

      public function getSemester(){
        $this->db->from($this->semester);
        $this->db->select('*');
        return $this->db->get();
      }

      public function create_tahun($data_ajaran){
      return $this->db->insert($this->tahun_ajaran, $data_ajaran); 
      } 

      public function getTahunAjaran(){
        $this->db->from($this->tahun_ajaran);
        $this->db->select('*');
        return $this->db->get();
      }

      public function getTahunAjaranById($th){
        $this->db->select('*');
        $this->db->from($this->tahun_ajaran);
        $this->db->where('id_tahun_ajaran', $th);
        return $this->db->get();
  
    }

    public function updateTahunAjaran($th=0 , $data_ajaran){
        $this->db->set($data_ajaran);
        $this->db->where('id_tahun_ajaran', $th);
        $this->db->update($this->tahun_ajaran); 
      }

      public function delTahun($th){
        $this ->db-> where('id_tahun_ajaran', $th);
        $this ->db-> delete($this->tahun_ajaran);
    }


    public function getListNilai(){
      $this->db->from($this->nilai);
      $this->db->select('*');
      return $this->db->get();
    }

    public function getKelasById($id){
      $this->db->from($this->kelas);
      $this->db->select('kelas');
      $this->db->where('id_kelas',$id);
      return $this->db->get();

  }



    
//Ambil Data Nilai By NIS
    public function getNilaiByNis($id){
      $this->db->from($this->santri);
      $this->db->select($this->santri.'.*,'.$this->kelas.'.kelas,'.$this->nilai.'.*');
      $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->santri.'.id_kelas');  
      $this->db->join($this->nilai, $this->nilai.'.nis = '.$this->santri.'.nis');  
      $this->db->where($this->santri.'.nis', $id);
        return $this->db->get();
      }
//End Ambil Data Nilai By NIS

  
  public  function checkEmail($email){
    $this->db->where('email',$email);
    return  $this->db->get($this->user);
  }
  public  function checkEmailAdmin($email){
    $this->db->where('email',$email);
    return  $this->db->get($this->useradmin);
  }



  //Ambil Data Kota
  public function getListKota(){
    $this->db->from($this->kota);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Kota

   //Ambil Data Institusi
   public function getListInstitusi(){
    $this->db->from($this->institusi);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Institusi

  //Ambil Data Provinsi
  public function getListProvinsi(){
    $this->db->from($this->provinsi);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Provinsi

  //Ambil Data Nama Komplek
  public function getListNamaKomplek(){
    $this->db->from($this->komplek);
    $this->db->select('nama_komplek');
    return $this->db->get();
  }
  //End Ambil Data Nama Komplek


  //Ambil Data Komplek
  public function getListKomplek(){
    $this->db->from($this->komplek);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Komplek

  //Ambil Data Kelas
  public function getListKelas(){
    $this->db->from($this->kelas);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Kelas

   //Ambil Data Status Santri
   public function getListStatus(){
    $this->db->from($this->status_santri);
    $this->db->select('*');
    return $this->db->get();
  }
  //End Ambil Data Status Santri

  //Ambil Data Lengkap Santri
  public function getListSantri(){
    $this->db->from($this->santri);
    $this->db->join($this->komplek, $this->komplek.'.id_komplek = '.$this->santri.'.id_komplek');  
    $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->santri.'.id_kelas');  
    $this->db->select($this->santri.'.*,'.$this->santri.'.id_komplek ,'.$this->komplek.'.nama_komplek,'.$this->kelas.'.kelas');
    return $this->db->get();
  }
  //End Ambil Data Lengkap Santri

  //Ambil Data Santri By NIS

  public function getSantriByNis($id){
    $this->db->select($this->santri.'.*,'.$this->santri.'.id_komplek ,'.$this->komplek.'.nama_komplek,'.$this->kelas.'.kelas,'.$this->institusi.'.nama_institusi,'.$this->status_santri.'.status_santri');
    $this->db->join($this->komplek, $this->komplek.'.id_komplek = '.$this->santri.'.id_komplek');  
    $this->db->join($this->kelas, $this->kelas.'.id_kelas = '.$this->santri.'.id_kelas');  
    $this->db->join($this->status_santri, $this->status_santri.'.id_status = '.$this->santri.'.id_status');  
    $this->db->join($this->institusi, $this->institusi.'.id_institusi = '.$this->santri.'.profesi');  
    $this->db->from($this->santri);
    $this->db->where($this->santri.'.nis', $id);
    return $this->db->get();
  }

  //End Ambil Data Santri By NIS
  
  //Update Foto Santri
  public function updateImgSantri($img){
    $this->db->set($img);
    $this->db->where('nis', $_SESSION['id']);
    $this->db->update($this->santri);    
  }
  //End Update Foto Santri
 

  //Ambil Data Foto
  public function getFoto($id){
    $this->db->from($this->santri);
    $this->db->where($this->santri.'.nis', $id);
    $this->db->select('foto');
    return $this->db->get();
  }
  //End Data Foto

  //Delete Data Santri

  public function delSantri($id){
    $this ->db-> where('nis', $id);
    $this ->db-> delete($this->santri);
}

//End Delete Data Santri

//Update Data Santri

public function updateDataSantri($id=0 , $data=0){
  $this->db->set($data);
  $this->db->where('nis', $id);
  $this->db->update($this->santri); 
}

//End Update Data Santri







 
}

/* End of file Mod_user.php */
