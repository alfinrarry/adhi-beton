<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_DailyIncomingKasar extends CI_Model {

  protected $userDetail 	           = 'em_user_details';
  protected $useradmin                 = 'em_useradmins';
  protected $useradminDetail           = 'em_useradmin_details';
  protected $dailyIncomingKasar        = 'em_daily_incoming_kasar';
  protected $material                  = 'em_material';
  protected $vendor                    = 'em_vendor';
  protected $quarry                    = 'em_quarry';
  protected $pelaksana                 = 'em_pelaksana';
  protected $status                    = 'em_status';



  public function logged_id(){ 
    return $this->session->userdata('id'); 
  }
  //fungsi check login 
  public function check_login($data_user){ 
    $this->db->where("(nis='".$data_user['email']."' OR email='".$data_user['email']."' )"); 
    $query = $this->db->get($this->user); 
    if ($query->num_rows() == 1) { 
      $hash = $query->row('password'); 
      if (password_verify($data_user['password'],$hash)){ 
        return $query->result(); 
      } else { 
        $this->session->set_flashdata('fail_msg_password', 'Password Salah');
      } 
    } else { 
      $this->session->set_flashdata('fail_msg_account', 'Account Tidak tersedia');
    } 
  } 
 
  public function inputDailyIncomingKasar($data_daily_incoming_kasar){
    return $this->db->insert($this->dailyIncomingKasar, $data_daily_incoming_kasar); 
  } 


    //Ambil Data Daily Incoming Kasar
    public function getDailyIncomingKasar(){
        $this->db->from($this->dailyIncomingKasar);
        $this->db->join($this->material, $this->material.'.id_material = '.$this->dailyIncomingKasar.'.id_material');
        $this->db->join($this->vendor, $this->vendor.'.id_vendor = '.$this->dailyIncomingKasar.'.id_vendor');  
        $this->db->join($this->quarry, $this->quarry.'.id_quarry = '.$this->dailyIncomingKasar.'.id_quarry');  
        $this->db->join($this->pelaksana, $this->pelaksana.'.id_pelaksana = '.$this->dailyIncomingKasar.'.id_pelaksana');
        $this->db->join($this->status, $this->status.'.id_status = '.$this->dailyIncomingKasar.'.id_status');  
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Daily Incoming Kasar

       //Ambil Data Material
    public function getMaterial(){
        $this->db->from($this->material);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Material

       //Ambil Data Vendor
    public function getVendor(){
        $this->db->from($this->vendor);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Vendor

       //Ambil Data Quarry
    public function getQuarry(){
        $this->db->from($this->quarry);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Quarry

      //Ambil Data Pelaksana
    public function getPelaksana(){
        $this->db->from($this->pelaksana);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Pelaksana

      //Ambil Data Status
    public function getStatus(){
        $this->db->from($this->status);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Status

          //Ambil Data Incoming Kasar By Id

          public function getDailyIncomingKasarById($id=0){
            $this->db->from($this->dailyIncomingKasar);
            $this->db->join($this->material, $this->material.'.id_material = '.$this->dailyIncomingKasar.'.id_material');
            $this->db->join($this->vendor, $this->vendor.'.id_vendor = '.$this->dailyIncomingKasar.'.id_vendor');  
            $this->db->join($this->quarry, $this->quarry.'.id_quarry = '.$this->dailyIncomingKasar.'.id_quarry');  
            $this->db->join($this->pelaksana, $this->pelaksana.'.id_pelaksana = '.$this->dailyIncomingKasar.'.id_pelaksana');
            $this->db->join($this->status, $this->status.'.id_status = '.$this->dailyIncomingKasar.'.id_status'); 
            $this->db->where($this->dailyIncomingKasar.'.id_daily_incoming_kasar', $id);
            $this->db->select('*');
            return $this->db->get();
          }
      //End Ambil Data Incoming Kasar By Id

      //Ambil Data Visual Bersih By Id
      public function getBersihById($id=0){
        $this->db->from($this->dailyIncomingKasar);
        $this->db->where($this->dailyIncomingKasar.'.id_daily_incoming_kasar', $id);
        $this->db->select($this->dailyIncomingKasar.'.kriteria_visual_bersih');
        return $this->db->get();
      }
      //End Ambil Data Visual Bersih By Id


      //Ambil Data Material By Id
      public function getMaterialById($id=0){
        $this->db->select($this->dailyIncomingKasar.'.id_material, '.$this->material.'.name_material');
        $this->db->from($this->dailyIncomingKasar);
        $this->db->join($this->material, $this->material.'.id_material = '.$this->dailyIncomingKasar.'.id_material');
        $this->db->where($this->dailyIncomingKasar.'.id_daily_incoming_kasar', $id);
        return $this->db->get();
      }
      //End Ambil Data Material By Id

      //Ambil Data Vendor By Id
      public function getVendorById($id=0){
        $this->db->from($this->dailyIncomingKasar);
        $this->db->join($this->vendor, $this->vendor.'.id_vendor = '.$this->dailyIncomingKasar.'.id_vendor');  
        $this->db->where($this->dailyIncomingKasar.'.id_vendor', $id);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Vendor By Id

       //Ambil Data Quarry By Id
       public function getQuarryById($id=0){
        $this->db->from($this->dailyIncomingKasar);
        $this->db->join($this->quarry, $this->quarry.'.id_quarry = '.$this->dailyIncomingKasar.'.id_quarry');  
        $this->db->where($this->dailyIncomingKasar.'.id_quarry', $id);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Quarry By Id

      //Ambil Data Pelaksana By Id
      public function getPelaksanaById($id=0){
        $this->db->from($this->dailyIncomingKasar);
        $this->db->join($this->pelaksana, $this->pelaksana.'.id_pelaksana = '.$this->dailyIncomingKasar.'.id_pelaksana');
        $this->db->where($this->dailyIncomingKasar.'.id_pelaksana', $id);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Pelaksana By Id

       //Ambil Data Status By Id
       public function getStatusById($id=0){
        $this->db->from($this->dailyIncomingKasar);
        $this->db->join($this->status, $this->status.'.id_status = '.$this->dailyIncomingKasar.'.id_status'); 
        $this->db->where($this->dailyIncomingKasar.'.id_status', $id);
        $this->db->select('*');
        return $this->db->get();
      }
      //End Ambil Data Status By Id



        //Delete Data Daily Incoming Kasar

    public function delIncomingKasar($id){
      $this->db->from($this->dailyIncomingKasar);    
      $this ->db-> where('id_daily_incoming_kasar', $id);
      $this ->db-> delete($this->dailyIncomingKasar);
    }
    //Delete Data Daily Incoming Kasar
// Update Image Incoming Kasar
    public function updateImgIncomingKasar($img){
      $this->db->set($img);
      $this->db->where('id_daily_incoming_kasar', $_SESSION['id']);
      $this->db->update($this->dailyIncomingKasar);    
    }
// End Update Image Incoming Kasar


    // Update Data Incoming Kasar
    public function updateIncomingKasar($id=0 , $data=0){
      $this->db->set($data);
      $this->db->where('id_daily_incoming_kasar', $id);
      $this->db->update($this->dailyIncomingKasar); 
    }
     // End Update Data Incoming Kasar

       // Tolak Data Daily Incoming Kasar
    public function tolakIncomingKasar($id=0 , $data=0){
        $this->db->set($data);
        $this->db->where('id_daily_incoming_kasar', $id);
        $this->db->update($this->dailyIncomingKasar); 
      }
       // End Tolak Data Daily Incoming Kasar

          // Setuju Data Daily Incoming Kasar
    public function setujuIncomingKasar($id=0 , $data=0){
        $this->db->set($data);
        $this->db->where('id_daily_incoming_kasar', $id);
        $this->db->update($this->dailyIncomingKasar); 
      }
       // End Setuju Data Daily Incoming Kasar

  
  public  function checkEmail($email){
    $this->db->where('email',$email);
    return  $this->db->get($this->user);
  }
  public  function checkEmailAdmin($email){
    $this->db->where('email',$email);
    return  $this->db->get($this->useradmin);
  }

  

 
  
  //Update Foto Santri
  public function updateImgSantri($img){
    $this->db->set($img);
    $this->db->where('nis', $_SESSION['id']);
    $this->db->update($this->santri);    
  }
  //End Update Foto Santri
 

  //Ambil Data Foto
  public function getFoto($id){
    $this->db->from($this->santri);
    $this->db->where($this->santri.'.nis', $id);
    $this->db->select('foto');
    return $this->db->get();
  }
  //End Data Foto




public function delUserSantri($id){
  $this->db->from($this->user);    
  $this ->db-> where('nis', $id);
  $this ->db-> delete($this->user);
}

public function delUserDetailSantri($id){
  $this->db->from($this->userDetail);    
  $this ->db-> where('nis', $id);
  $this ->db-> delete($this->userDetail);
}

//End Delete Data Santri

//Update Data Santri

public function updateDataSantri($nis=0 , $data=0){
  $this->db->set($data);
  $this->db->where('nis', $nis);
  $this->db->update($this->santri); 
}

public function updateDataNilai($nis=0 , $data=0){
  $this->db->set($data);
  $this->db->where('nis', $nis);
  $this->db->update($this->nilai); 
}

public function updateDataAbsensi($nis=0 , $data=0){
  $this->db->set($data);
  $this->db->where('nis', $nis);
  $this->db->update($this->absensi); 
}

public function updateDataUserSantri($nis=0 , $data=0){
  $this->db->set($data);
  $this->db->where('nis', $nis);
  $this->db->update($this->user); 
}

//End Update Data Santri




  // admin
  public function checkLoginAdmin($data_user){ 
    $this->db->select($this->useradmin.'.*,'.$this->useradmin.'.id as idu ,'.$this->useradminDetail.'.*');
    $this->db->from($this->useradmin);    
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin ='.$this->useradmin.'.id');
    $this->db->where($this->useradmin.'.username', $data_user['email']);
    $this->db->or_where($this->useradmin.'.email', $data_user['email']);
    $query = $this->db->get();
    if ($query->num_rows() == 1) { 
      $hash = $query->row('password'); 
      if (password_verify($data_user['password'],$hash)){ 
        return $query->result(); 
      } else { 
        $this->session->set_flashdata('fail_msg_password', 'Password Salah');
      } 
    } else { 
      $this->session->set_flashdata('fail_msg_account', 'Account Tidak tersedia');
    } 
  }

  public function addUser($data)
  {
    $this->db->insert($this->useradmin, $data);
    return $this->db->insert_id();
  }

  public function getListUser(){
    $this->db->from($this->useradminDetail);
    $this->db->join($this->useradmin, $this->useradmin.'.id = '.$this->useradminDetail.'.id_useradmin');  
    $this->db->join($this->userAdminRole,$this->useradmin.'.id_role = '.$this->userAdminRole.'.id');
    $this->db->select('*');
    return $this->db->get();
  }

  public function getUser($id=0){
    $this->db->select($this->user.'.*,'.$this->userDetail.'.*');
    $this->db->from($this->user);
    $this->db->join($this->userDetail, $this->userDetail.'.id_user = '.$this->user.'.id');
    $this->db->where($this->user.'.id',$id);
    return $this->db->get();
  }
 

  public function getRole(){
    
    $this->db->from($this->useradmin);
    $this->db->join($this->userAdminRole,$this->useradmin.'.id_role = '.$this->userAdminRole.'.id');
    $this->db->select($this->useradmin.'.id');
    $this->db->select($this->useradmin.'.fullname');
    $this->db->select($this->userAdminRole.'.name');
    return $this->db->get();
  }

  public function getAllRole(){
    $this->db->select('*');
    $this->db->from($this->userAdminRole);
    return $this->db->get();
  }
  public function getImage($id){
    $this->db->from($this->useradminDetail);
    $this->db->join($this->useradmin, $this->useradmin.'.id = '.$this->useradminDetail.'.id_useradmin');
    $this->db->where($this->useradmin.'.id', $id);
    $this->db->select('img_path');
    return $this->db->get();
  }
  public function getImg($id){
    $this->db->join($this->useradmin, $this->useradmin.'.id = '.$this->useradminDetail.'.id_useradmin');
    $this->db->where($this->useradminDetail.'.id', $id);
    return $this->db->get($this->useradminDetail);
  }

  public function getUserEmail($id){
    $this->db->select($this->user.'email');
    $this->db->from($this->user);
    $this->db->where($this->user.'.nis', $id);
    return $this->db->get($this->user);

        
  }

  
  
  public function updateImg($img){
    $this->db->set($img);
    $this->db->where('id_user', $_SESSION['id']);
    $this->db->update($this->userDetail);    
  }
  public function updateData($data_user){
    $this->db->set($data_user);
    $this->db->where('id_user', $_SESSION['id']);
    $this->db->update($this->userDetail); 
  }
  public function updateImgAdmin($img){
    $this->db->set($img);
    $this->db->where('id_useradmin', $_SESSION['id']);
    $this->db->update($this->useradminDetail);    
  }

  public function updateDataAdmin($id=0 , $data=0){
    $this->db->set($data);
    $this->db->where('id', $id);
    $this->db->update($this->useradmin); 
  }

  public function updateDataAdminDetail($id=0 , $data=0){
    $this->db->set($data);
    $this->db->where('id_useradmin', $id);
    $this->db->update($this->useradminDetail); 
  }

  public function updateEmail($email){
    $this->db->set($email);
    $this->db->where('id', $_SESSION['id']);
    $this->db->update($this->user); 
  }
  public function getUserInfoByEmail($email){
    $this->db->select($this->user.'.*,'.$this->userDetail.'.*');
    $this->db->from($this->userDetail);
    $this->db->join($this->user, $this->user.'.id = '.$this->userDetail.'.id_user');
    $this->db->where($this->user.'.email', $email);
    $q = $this->db->get();  
      if($this->db->affected_rows() > 0){  
          $row = $q->row();  
          return $row;  
      }  
  }
  public function getUserByEmail($email){
    $this->db->select($this->user.'.*, '.$this->userDetail.'.*,'.$this->userRole.'.*');
    $this->db->from($this->user);
    $this->db->join($this->userDetail, $this->userDetail.'.id_user'.'='.$this->user.'.id', 'left');
    $this->db->join($this->userRole, $this->userRole.'.id'.'='.$this->user.'.id_role', 'left');   
    $this->db->where($this->user.'.email', $email); 
    return $this->db->get();
}
public function getUserAdminByEmail($email){
  $this->db->select($this->useradmin.'.*, '.$this->useradminDetail.'.*,'.$this->userAdminRole.'.*');
  $this->db->from($this->useradmin);
  $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin'.'='.$this->useradmin.'.id', 'left');
  $this->db->join($this->userAdminRole, $this->userAdminRole.'.id'.'='.$this->useradmin.'.id_role', 'left');   
  $this->db->where($this->useradmin.'.email', $email); 
  return $this->db->get();
}
  public function insertToken($email)  
  {    
    $token = substr(sha1(rand()), 0, 30);
    $user = $this->getUserByEmail($email)->row_array();     
    $string = array( 
        'id_user'    =>$user['id_user'], 
        'token_reset'=> $token,  
        'email'=>$email,
        'status'=>0,
        'created_at' =>date('Y-m-d H:m:s')
      );  
    $query = $this->db->insert_string($this->userReset,$string);  
    $this->db->query($query);  
    return $token . $email;  
  }
  public function isTokenValid($token)  
  {  
    $tkn = substr($token,0,30);  
    $email = substr($token,30);   
    // $this->db->where('token_reset', $tkn);
    // $this->db->where('email', $email);
    // $q = $this->db->get($this->userReset, 1);
    
    $q = $this->db->get_where($this->userReset, array(  
      $this->userReset.'.token_reset' => $tkn,   
      $this->userReset.'.email' => $email, 
      'status'=> 0), 1);               
      
    if($this->db->affected_rows() > 0){  
      $row = $q->row();         
        
      $created = $row->created_at;  
      $createdTS = strtotime($created);  
      $expired = date('Y-m-d H:i:s',strtotime('+4 hour',strtotime($created)));   
      $exp = strtotime($expired);  
        
      if($createdTS > $exp){  
        return false;  
      }  
      $user_info = $this->getUserInfoByEmail($row->email);  
      return $user_info;  
        
    }else{  
      return false;  
    }  
  }
  public function updatePassword($cleanPost)  
  {    
    $this->db->where('id', $cleanPost['id_user']);  
    $this->db->update($this->user, array('password' => $cleanPost['password']));     
    return true;
  }
  public function updateToken($cleanPost){ 
    $this->db->where('email', $cleanPost['email']);  
    $this->db->update($this->userReset, array('status' => 1));
    return true;  
  }
  public function countMember(){
    // $this->db->where('deleted_at !=', 000);
    $this->db->from($this->user);
    return $this->db->count_all_results();
  }
  public function getUserAdmin(){
    $this->db->select($this->useradmin.'.username,'.$this->useradmin.'.id,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    return $this->db->get();
  }
  public function banadmin($id){
    $this->db->set('is_ban',0);
    $this->db->where('id', $id);
    $this->db->update($this->useradmin); 
    redirect(base_url('admin/user/listUser'));
  }
  public function aktifadmin($id){
    $this->db->set('is_ban',1);
    $this->db->where('id', $id);
    $this->db->update($this->useradmin); 
    redirect(base_url('admin/user/listUser'));
  }
  public function getUserAdminByUsername($username){
    $this->db->select($this->useradmin.'.username,'.$this->useradmin.'.id,'.$this->useradmin.'.is_ban,'.$this->useradmin.'.email,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    $this->db->where($this->useradmin.'.username', $username);
    return $this->db->get();
  }
  public function getUserAdminById($id){
    $this->db->select($this->useradmin.'.id,'.$this->useradmin.'.username,'.$this->useradmin.'.is_ban,'.$this->useradmin.'.email,'.$this->useradminDetail.'.*,'.$this->userAdminRole.'.id,'.$this->userAdminRole.'.name as role,'.$this->santri.'.*,');
    $this->db->from($this->useradmin);
    $this->db->join($this->useradminDetail, $this->useradminDetail.'.id_useradmin = '.$this->useradmin.'.id');    
    $this->db->join($this->userAdminRole, $this->userAdminRole.'.id = '.$this->useradmin.'.id_role');
    $this->db->join($this->santri, $this->santri.'.nis = '.$this->useradmin.'.id_useradmin');
    $this->db->where($this->useradmin.'.id', $id);
    return $this->db->get();
  }
  public function getId($username){
    $this->db->where('username',$username);
    $this->db->select('id');
    $this->db->from($this->useradmin);
    return $this->db->get($this->$useradmin);
  }

  public function deluserAdmin($id){
    $this ->db-> where('id', $id);
    $this ->db-> delete($this->useradmin);
}
public function deluserAdminDetail($id_useradmin){
  $this ->db-> where('id_useradmin', $id_useradmin);
  $this ->db-> delete($this->useradminDetail);
}
public function getUserInfo($id)  
{  
  $q = $this->db->get_where($this->useradmin, array('id' => $id), 1);   
  if($this->db->affected_rows() > 0){  
    $row = $q->row();  
    return $row;  
  }else{  
    error_log('No user found getUserInfo('.$id.')');  
    return false;  
  }  
}  
public function getUserInfoByMailUser($email){  
  $q = $this->db->get_where($this->useradmin, array('email' => $email), 1);   
  if($this->db->affected_rows() > 0){  
    $row = $q->row();  
    return $row;  
  }  
}  

public function insertTokenUser($id)  
{    
  $token = substr(sha1(rand()), 0, 30);   
  $date = date('Y-m-d');  
    
  $string = array(  
      'token'=> $token,  
      'id'=>$id,  
      'created_at'=>$date  
    );  
  $query = $this->db->insert_string($this->useradminToken,$string);  
  $this->db->query($query);  
  return $token . $id;  
    
}  

public function isTokenValidUser($token)  
{  
  $tkn = substr($token,0,30);  
  $uid = substr($token,30);     
    
  $q = $this->db->get_where($this->useradminToken, array(  
    'em_useradmin_token.token' => $tkn,   
    'em_useradmin_token.id' => $uid), 1);               
        
  if($this->db->affected_rows() > 0){  
    $row = $q->row();         
      
    $created = $row->created_at;  
    $createdTS = strtotime($created);  
    $today = date('Y-m-d');   
    $todayTS = strtotime($today);  
      
    if($createdTS != $todayTS){  
      return false;  
    }  
      
    $user_info = $this->getUserInfo($row->id);  
    return $user_info;  
      
  }else{  
    return false;  
  }  
    
}   

public function updatePasswordUser($post)  
{    
  $this->db->where('id', $post['id']);  
  $this->db->update('em_useradmins', array('password' => $post['password']));      
  return true;  
}   


 
}

/* End of file Mod_user.php */
