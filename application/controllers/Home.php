<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends PIS_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_santri','santri');
    $this->load->model('Mod_system','system');
    $this->load->library('email');
    $this->load->library('session');
  }
  public function index($id=0)
  {
    $data['codepage']       = "back_index";
    $data['page_title'] 	  = 'Biodata Santri';
    $id                     = $_SESSION['id'];
    $data['user']           = $this->user->getUser($id)->row_array();
    $data['image']          = $this->user->getImageUser($id)->result_array();
    $userEmail              = $this->user->getUser($id)->row_array();
    $email                  = $userEmail['email'];
    

    
    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('Home/index');

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
      redirect(base_url('auth'));
    }

    if(isset($_POST['submit'])){

      
      $config['upload_path']    ='./assets/img/content/santri/';
      $config['allowed_types']  ='jpg|png|ico';
      $config['encrypt_name']   = TRUE;

    $this->load->library('upload',$config);

    if($this->upload->do_upload('img_path'))
    {
      $img ='img/content/santri/';
      $img.=  $this->upload->data('file_name');
      $img = array(
        'img_path'         => @$img
      );
      $this->user->updateImgUser($img);
    }
      if($email != $_POST['emailedit']){
      $data = array(
      
       
        'email'            => $_POST['emailedit'],

      );
      // print_r($data);die;

      $data = $this->user->updateData($id,$data);
    }

      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect('Home');
    
  }

    $this->template->front_views('site/front/home', $data);
  }

  public function reset_password()  
  {  
    $this->load->library('form_validation');
    $this->load->helper('form');
    $this->load->helper('security');
    $token = $this->base64url_decode($this->uri->segment(3));           
    $cleanToken = $this->security->xss_clean($token);
    $user_info = $this->user->isTokenValid($cleanToken);
  //either false or array();          
    if(!$user_info){  
      $this->session->set_flashdata('fail_msg_register', 'Token Reset Password tidak valid atau kedaluarsa');  
      redirect(base_url('auth'));   
    } 
    $this->form_validation->set_message('min_length', 'Password Kurang panjang');
    $this->form_validation->set_message('matches', 'Password tidak cocok');          
    $this->form_validation->set_rules('password', 'Pasword Minimal 6 Karakter', 'required|min_length[6]');  
    $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');         
      
    if ($this->form_validation->run() == TRUE) {
      $post = $this->input->post(NULL, TRUE);     
      $cleanPost = $this->security->xss_clean($post);      
      $hashed = hash_password($cleanPost['password']);         
      $cleanPost['password'] = $hashed;  
      $cleanPost['id_user'] = $user_info->id_user;
      $cleanPost['email']   = $user_info->email; 
      unset($cleanPost['passconf']);   
      if(!$this->user->updatePassword($cleanPost)){
        $this->session->set_flashdata('fail_msg_register', 'Update password gagal.');  
      }else{  
        $this->user->updateToken($cleanPost);
        $this->session->set_flashdata('success_msg_register', 'Password anda sudah  
          diperbaharui. Silakan login.');  
      }  
      redirect(base_url('auth'));  
    }else{
      // $data 	= array ();
    $data = array(
      'codepage' => "back_login",
      'token'=>$this->base64url_encode($token)  
    );   
    $this->template->front_views('site/front/v_reset_password', $data); 
    }  
  }
  
 
  
}

/* End of file Home.php */