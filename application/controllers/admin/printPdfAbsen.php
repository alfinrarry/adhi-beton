<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class printPdfAbsen extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_santri','santri');
    $this->load->model('Mod_kelas','kelas');
    $this->load->model('Mod_nilai','nilai');
    $this->load->model('Mod_printPdf','print');
    $this->load->library('email');
    $this->load->library('session');
    $this->load->library('pdf');


    
  }



public function printCariKelasAbsen(){
  $data['codepage']     = "back_useradmin";
  $data['page_title'] 	= 'Print Out Rekap Absensi';
  $data['santri']       = $this->santri->getListSantri()->result_array();
  $data['kelas']        = $this->santri->getListKelas()->result_array();
  $data['th']            = $this->nilai->getTahunAjaran()->result_array();
  $data['bln']           = $this->print->getBulan()->result_array();

  $id = $_SESSION['id'];
  $data['image']        = $this->user->getImage($id)->result_array();
  
  
  if ($_SESSION['id'] == true) { 
    $this->session->set_userdata($_SESSION); 
    base_url('admin/printPdf/printCariKelas/');

  } else { 
    $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
    redirect(base_url('login_admin'));
  }

  $this->template->back_views('site/back/printCariAbsen',$data);
  
}

public function pencarianAbsen()
{
  $data['codepage']      = "back_useradmin";
  $id                    = $_SESSION['id'];
  $data['image']         = $this->user->getImage($id)->result_array();
  $data['useradmin']     = $this->user->getUserAdminById($id)->row_array();

  if ($_SESSION['id'] == true) { 
    $this->session->set_userdata($_SESSION); 
    base_url('admin/printPdf/printCariKelas/');

  } else { 
    $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
    redirect(base_url('login_admin'));
  }


  $kelas=18;
  $kelas                 = $this->input->get('kelas');
  $semester              = $this->input->get('semester');
  $bulan                 = $this->input->get('bulan');
  $tahun_ajaran          = $this->input->get('tahun_ajaran');
  $data['hasil']         = $this->print->printPdfAbsen($kelas,$semester,$bulan,$tahun_ajaran)->result_array();    

  if ($kelas <= 14)
  {
    $hasil    = $this->print->printPdfAbsen($kelas,$semester,$bulan,$tahun_ajaran)->result_array();    
// print_r($hasil);die;
    $pdf = new FPDF('l','mm','A4');
    // membuat halaman baru
    $pdf->AddPage();
    // setting jenis font yang akan digunakan
    $pdf->SetFont('Arial','B',16);
    // mencetak string 
    $pdf->Cell(280,7,"MADRASAH MATHOLI'UL HUDA",0,1,'C');
    $pdf->Cell(280,7,'PONDOK PESANTREN MIFTAHUL HUDA',0,1,'C');
    $pdf->SetFont('Arial','B',14);
    foreach (array_slice($hasil,0,1) as $row){
    $pdf->Cell(280,7,'REKAP ABSENSI KELAS '.$row['kelas'],0,1,'C');
    $pdf->Cell(280,7,'Periode '.$row['bulan'].' '.$row['tahun_ajaran'],0,1,'C');
    }
    // Memberikan space kebawah agar tidak terlalu rapat
    $pdf->Cell(0,7,'',0,1);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(60,6,'NIS',1,0);
    $pdf->Cell(80,6,'NAMA SANTRI',1,0);
    $pdf->Cell(27,6,'KELAS',1,0);
    $pdf->Cell(25,6,'SEMESTER',1,0);
    $pdf->Cell(25,6,'SAKIT',1,0);
    $pdf->Cell(25,6,'IZIN',1,0);
    $pdf->Cell(29,6,'ALPHA',1,0);

    $pdf->SetFont('Arial','',10);
    foreach ($hasil as $row){
        $pdf->Cell(0,7,'',0,1);
        $pdf->Cell(60,6,$row['nis'],1,0);
        $pdf->Cell(80,6,$row['nama'],1,0);
        $pdf->Cell(27,6,$row['kelas'],1,0);
        $pdf->Cell(25,6,$row['semester'],1,0);
        $pdf->Cell(25,6,$row['sakit'],1,0);
        $pdf->Cell(25,6,$row['izin'],1,0);
        $pdf->Cell(29,6,$row['alpha'],1,0);


    }
    $pdf->Output();  
   }
  }





 


}

/* End of file User.php */
