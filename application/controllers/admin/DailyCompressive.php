<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class DailyCompressive extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_DailyCompressive','DailyCompressive');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

  // List  DailyCompressive
  public function listDailyCompressive(){
    $data['codepage']         = "back_addProduct";
    $data['page_title']   	  = ' List Daily Compressive';
    $id                       = $_SESSION['id'];
    $data['image']            = $this->user->getImage($id)->result_array();
    $data['compressive']      = $this->DailyCompressive->getDailyCompressive()->result_array();
    $data['project']          = $this->DailyCompressive->getProject()->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/DailyCompressive/listDailyCompressive');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/dailyCompressiveList',$data);
  }
    // List DailyCompressive

    // Form DailyCompressive
    public function formDailyCompressive($id){
      $data['codepage']            = "back_addProduct";
      $data['page_title']   	   = 'Form Add Daily Compressive';
      $data['userAdminRole']       = $this->user->getAllRole()->result_array();
      $data['compressive']         = $this->DailyCompressive->getDailyCompressiveById($id)->row_array();
      $data['project']             = $this->DailyCompressive->getProject()->result_array();
      $data['type']                = $this->DailyCompressive->getType()->result_array();
      $id                          = $_SESSION['id'];
      $data['image']               = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/DailyCompressive/formDailyCompressive');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
      $this->template->back_views('site/back/dailyCompressiveForm',$data);
    }
    // Form DailyCompressive

        // Edit dan View  DailyCompressive
    public function detailDailyCompressive($id=0){
      $data['codepage']              = "back_useradmin";
      $data['page_title'] 	         = "Detail Daily Compressive";
      $data['compressive']           = $this->DailyCompressive->getDailyCompressiveById($id)->row_array();
      $id                            = $_SESSION['id'];
      $data['image']                 = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/DailyCompressive/detailDailyCompressive/');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
        redirect(base_url('login_admin'));
      }
  
      $this->template->back_views('site/back/dailyCompressiveDetail',$data);

    }

    // End Edit dan View DailyCompressive

    public function delDailyCompressive($id){
      $data= $this->DailyCompressive->delDailyCompressive($id);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
      redirect(base_url("admin/DailyCompressive/listDailyCompressive"));
}

// Udpate DailyCompressive
    public function updateDailyCompressive($id=0){
    $data['codepage']               = "back_addProduct";
    $data['page_title'] 	        = 'Update Slump Flow Test';
    $data['compressive']            = $this->DailyCompressive->getDailyCompressiveById($id)->result_array();
    
   {
    $config['upload_path']    ='./assets/img/content/DailyCompressive/';
    $config['allowed_types']  ='jpg|png|ico';
    $config['encrypt_name']   = TRUE;
  
    $this->load->library('upload',$config);
    if($this->upload->do_upload('doc_img'))
    {
        $img ='img/content/DailyCompressive/';
        $img.=  $this->upload->data('file_name');
    }
    $status = 3;
    $data_compressive = array(
        'id_project'            => $_POST['id_project'] ,
        'id_type'               => $_POST['id_type'] ,
        'customer_name'         => $_POST['customer_name'] ,
        'no_produksi'           => $_POST['no_produksi'] ,
        'age'                   => $_POST['age'] ,
        'weight'                => $_POST['weight'] ,
        'density'               => $_POST['density'] ,
        'load'                  => $_POST['load'] ,
        'strength_mpa'          => $_POST['strength_mpa'] ,
        'correction_to_cube'    => $_POST['correction_to_cube'] ,
        'date_of_pouring'       => $_POST['date_of_pouring'] ,
        'date_test'             => $_POST['date_test'] ,
        'doc_img'               => @$img,
        'id_status'             => @$status 
    );
      $data = $this->DailyCompressive->updateDailyCompressive($id,$data_compressive);
   }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url("admin/DailyCompressive/listDailyCompressive"));  
  
  }
  // End Udpate Daily Compressive

  // Udpate DailyCompressive
  public function updateDailyCompressive2($id=0){
    $data['codepage']               = "back_addProduct";
    $data['page_title'] 	        = 'Update Slump Flow Test';
    $data['compressive']            = $this->DailyCompressive->getDailyCompressiveById($id)->result_array();
    
   {
    $config['upload_path']    ='./assets/img/content/DailyCompressive/';
    $config['allowed_types']  ='jpg|png|ico';
    $config['encrypt_name']   = TRUE;
  
    $this->load->library('upload',$config);
    if($this->upload->do_upload('doc_img'))
    {
        $img ='img/content/DailyCompressive/';
        $img.=  $this->upload->data('file_name');
    }
    $status = 3;
    $data_compressive = array(
        
        'date_of_pouring'       => $_POST['date_of_pouring'] ,
        'date_test'             => $_POST['date_test'] 
        
    );
      $data = $this->DailyCompressive->updateDailyCompressive2($id,$data_compressive);
   }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url("admin/DailyCompressive/listDailyCompressive"));  
  
  }
  // End Udpate Daily Compressive
    
    public function downloadArchive(){
      if (isset($_GET['filename'])) {
        $filename    = $_GET['filename'];
        $back_dir    ="assets/";
        $file = $back_dir.$_GET['filename'];
     
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: private');
            header('Pragma: private');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            
            exit;
        } 
        else {
            $_SESSION['pesan'] = "Oops! File - $filename - not found ...";
            header("location:archiveList.php");
        }
    }
  }

}

/* End of file User.php */
