<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class importNilai extends CI_Controller {
    public function __construct()
    {
      parent::__construct();
      $this->load->model('Mod_santri','santri');
      $this->load->model('Mod_user','user');
      $this->load->library('email');
      $this->load->library('session');
      
    }

    public function index(){
    $data['codepage']      = "back_user";
    $data['page_title'] 	 = 'Import Nilai Santri';
    $data['user']          = $this->user->getListUser()->result_array();
    $id                    = $_SESSION['id'];
    $data['image']         = $this->user->getImage($id)->result_array();
    
    
    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/importNilai/importNilai');

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
      redirect(base_url('login_admin'));
    }
    
    
    
    $this->template->back_views('site/back/importNilai',$data);
    
  }

    public function upload()
    {
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';

        $config['upload_path'] = realpath('excel');
        $config['allowed_types'] = 'xlsx|xls|csv';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {

            //upload gagal
            $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>PROSES IMPORT GAGAL!</b> '.$this->upload->display_errors().'</div>');
            //redirect halaman
            redirect('admin/importNilai/');

        } else {

            $data_upload = $this->upload->data();

            $excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel         = $excelreader->load('excel/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true,true,true);

            $data = array();

            $numrow = 1;
            foreach($sheet as $row){
                            if($numrow > 1){
                                array_push($data, array(
                                    'nis'       => $row['A'],
                                    'nama'      => $row['B'],
                                    'kelas'     => $row['C'],
                                    'semester'  => $row['D'],
                                    'nahwu'     => $row['E']

                                ));
                    }
                $numrow++;
            }
            $this->db->insert_batch('import_nilai', $data);
            //delete file from server
            unlink(realpath('excel/'.$data_upload['file_name']));

            //upload success
            $this->session->set_flashdata('notif', '<div class="alert alert-success"><b>PROSES IMPORT BERHASIL!</b> Data berhasil diimport!</div>');
            //redirect halaman
            redirect('admin/importNilai/');

        }
    }

}