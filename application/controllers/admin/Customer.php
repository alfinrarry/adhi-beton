<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_customer','customer');
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_status','status');
    $this->load->library('email');
    $this->load->library('session');
    
  }

  public function create_data_santri(){
    $data['codepage']     = "back_addProduct";
    $data['page_title'] 	= 'Add Santri';

    if(isset($_POST['submit'])){

    $config['upload_path']    ='./assets/img/content/santri/';
    $config['allowed_types']  ='jpg|png|ico';
    $config['encrypt_name']   = TRUE;

    $this->load->library('upload',$config);
    if($this->upload->do_upload('foto'))
      {
          $img ='img/content/santri/';
          $img.=  $this->upload->data('file_name');
      }
      
      $data_santri = array(
      
        'nis'                => $_POST['nis'] ,
        'nama'               => $_POST['nama'],
        'nama_panggilan'     => $_POST['nama_panggilan'] ,
        'tempat_lahir'       => $_POST['tempat_lahir'] ,
        'tgl_lahir'          => date('Y-m-j'),
        'no_hp'              => $_POST['no_hp'] ,
        'profesi'            => $_POST['profesi'] ,
        'jurusan'            => $_POST['jurusan'] ,
        'ayah'               => $_POST['ayah'] ,
        'ibu'                => $_POST['ibu'] ,
        'wali'               => $_POST['wali'] ,
        'tlp_rmh'            => $_POST['tlp_rmh'] ,
        'alamat'             => $_POST['alamat'] ,
        'kecamatan'          => $_POST['kecamatan'] ,
        'kota'               => $_POST['kota'] ,
        'provinsi'           => $_POST['provinsi'] ,
        'kode_pos'           => $_POST['kode_pos'] ,
        'tgl_masuk'          => date('Y-m-j'),
        'id_komplek'         => $_POST['id_komplek'] ,
        'id_kelas'           => $_POST['id_kelas'] ,
        'id_status'          => $_POST['id_status'] ,
      );
      $data = $this->santri->create_santri($data_santri);

      $config['upload_path']    ='./assets/img/content/santri/';
      $config['allowed_types']  ='jpg|png|ico';
      $config['encrypt_name']   = TRUE;

    $this->load->library('upload',$config);
    if($this->upload->do_upload('img_path'))
      {
          $img ='img/content/santri/';
          $img.=  $this->upload->data('file_name');
      }

      $username    = $_POST ['nis'];
      $data_santri = array(
      
        'nis'                => $_POST['nis'] ,
        'nama'               => $_POST['nama'],
        'email'              => $_POST['email'],
        'id_komplek'         => $_POST['id_komplek'] ,
        'id_kelas'           => $_POST['id_kelas'] ,
        'id_institusi'       => $_POST['profesi'] ,
        'tgl_masuk'          => date('Y-m-j'),
        'img_path'           => @$img  
      );
      $data = $this->santri->addUserSantri($data_santri);

      $result = $this->user->getUserId($username)->row(); 
      $getUserId  = $result->id;
      $data_santri = array(
      
        'fullname'           => $_POST['nama'],
        'nis'                => $_POST['nis'] ,
        'id_user'            => @$getUserId,
        'created_at'         => date('Y-m-j'),
 
      );
      $data = $this->santri->addUserDetailSantri($data_santri);

      $data_santri = array(
      
        'nis'                => $_POST['nis'] ,
        'id_kelas'           => $_POST['id_kelas'] ,
      );
      $data = $this->santri->create_absen($data_santri);

      $data_santri = array(
      
        'nis'                => $_POST['nis'] ,
        'id_kelas'           => $_POST['id_kelas'] ,
      );
      $data = $this->santri->create_nilai($data_santri);

      $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
      redirect(base_url('admin/santri/listSantri'));
    }
    
  }

  // List Customer
  public function listCustomer(){
    $data['codepage']       = "back_addProduct";
    $data['page_title']   	= 'Customer';
    $data['userAdminRole']  = $this->user->getAllRole()->result_array();
    $id                     = $_SESSION['id'];
    $data['image']          = $this->user->getImage($id)->result_array();
    $data['customer']       = $this->customer->getCustomer()->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/customer/listCustomer');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }
    $this->template->back_views('site/back/customerList',$data);
  }
    // End List Customer

    // Form Add Customer
    public function formCustomer(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']   	= 'Form Add Customer';
      $data['status']         = $this->status->getListStatus()->result_array();
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/Customer/formCustomer');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/customerForm',$data);
    }
    // End Form Add Customer

       // Form Edit Customer
       public function formEditCustomer($id=0){
        $data['codepage']       = "back_addProduct";
        $data['page_title']   	= 'Form Edit Customer';
        $data['userAdminRole']  = $this->user->getAllRole()->result_array();
        $data['customer']       = $this->customer->getCustomerById($id)->row_array();
        $id                     = $_SESSION['id'];
        $data['image']          = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/Customer/formEditCustomer');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/customerEditForm',$data);
      }
      // End Form Edit Customer

        // Update Customer
    public function updateCustomer($id=0){
      $data['codepage']     = "back_addProduct";
      $data['page_title'] 	= 'Update Customer';
      $data['customer']     = $this->customer->getCustomerById($id)->row_array();

     {
        $data_customer = array(
        
          'customer_name'      => $_POST['name'] 
        );
        $data = $this->customer->updateCustomer($id,$data_customer);
      }
      redirect(base_url("admin/Customer/listCustomer"));

    }

        // End Update Customer

    // Add Customer
    public function inputCustomer(){
      $data['codepage']     = "back_addProduct";
      $data['page_title'] 	= 'Add Agregat Halus';
      $data['status']       = $this->status->getListStatus()->result_array();
      $data['customer']     = $this->customer->getCustomer()->result_array();
     {
        $data_customer = array(
        
          'customer_name'      => $_POST['name'] 
        );
        $data = $this->customer->inputCustomer($data_customer);
      }
      redirect(base_url("admin/Customer/listCustomer"));

    }

        // End Add Customer
  
  public function delCustomer($id){
    $data= $this->customer->delCustomer($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/Customer/listCustomer"));
}
}

/* End of file User.php */
