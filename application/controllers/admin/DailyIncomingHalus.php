<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class DailyIncomingHalus extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_DailyIncomingHalus','DailyIncomingHalus');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List  Daily Incoming Halus
   public function listDailyIncomingHalus(){
    $data['codepage']            = "back_addProduct";
    $data['page_title']   	     = 'List Daily Incoming Kasar';
    $data['userAdminRole']       = $this->user->getAllRole()->result_array();
    $data['dailyIncomingHalus']  = $this->DailyIncomingHalus->getDailyIncomingHalus()->result_array();
    $id                          = $_SESSION['id'];
    $data['image']               = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/DailyIncomingHalus/listDailyIncomingHalus');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/dailyIncomingHalusList',$data);
  }
    // End List  Daily Incoming Halus

 // Add Daily Incoming Halus
 public function inputDailyIncomingHalus(){
  $data['codepage']               = "back_addProduct";
  $data['page_title'] 	          = 'Add Daily Incoming Halus';
  $data['dailyIncomingHalus']     = $this->DailyIncomingHalus->getDailyIncomingHalus()->result_array();

    $config['upload_path']    ='./assets/img/content/DailyIncomingHalus/';
    $config['allowed_types']  ='jpg|png|ico';
    $config['encrypt_name']   = TRUE;

    $this->load->library('upload',$config);
    if($this->upload->do_upload('doc_img'))
    {
        $img ='img/content/DailyIncomingHalus/';
        $img.=  $this->upload->data('file_name');
    }
    $status='3';
 {
    $data_daily_incoming_halus = array(
    
      'id_material'                => $_POST['id_material'],
      'surat_jalan'                => $_POST['surat_jalan'],
      'id_vendor'                  => $_POST['id_vendor'],
      'id_quarry'                  => $_POST['id_quarry'],
      'kriteria_visual_butiran'    => $_POST['kriteria_visual_butiran'],
      'kriteria_visual_kekerasan'  => $_POST['kriteria_visual_kekerasan'],
      'kriteria_visual_warna'      => $_POST['kriteria_visual_warna'],
      'slit_content'               => $_POST['slit_content'],
      'id_pelaksana'               => $_POST['id_pelaksana'],
      'keterangan'                 => $_POST['keterangan'],
      'id_status'                  => @$status,
      'doc_img'                    => @$img 

    );
    $data = $this->DailyIncomingHalus->inputDailyIncomingHalus($data_daily_incoming_halus);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
  redirect(base_url("admin/DailyIncomingHalus/listDailyIncomingHalus"));

}
// End Add Daily Incoming Halus

    // Form Daily Incoming Halus
    public function formDailyIncomingHalus(){
      $data['codepage']                 = "back_addProduct";
      $data['page_title']               = 'Form Add Daily Incoming Halus';
      $data['userAdminRole']            = $this->user->getAllRole()->result_array();
      $data['dailyIncomingHalus']       = $this->DailyIncomingHalus->getDailyIncomingHalus()->result_array();
      $data['material']                 = $this->DailyIncomingHalus->getMaterial()->result_array();
      $data['vendor']                   = $this->DailyIncomingHalus->getVendor()->result_array();
      $data['quarry']                   = $this->DailyIncomingHalus->getQuarry()->result_array();
      $data['pelaksana']                = $this->DailyIncomingHalus->getPelaksana()->result_array();
      $data['status']                   = $this->DailyIncomingHalus->getStatus()->result_array();
      $id                               = $_SESSION['id'];
      $data['image']                    = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/DailyIncomingHalus/formDailyIncomingHalus');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/dailyIncomingHalusForm',$data);
    }
    // End Form Daily Incoming Halus

  
//   Delete Data Daily Incoming Halus
  public function delIncomingHalus($id){
    $data= $this->DailyIncomingHalus->delIncomingHalus($id);
           $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/DailyIncomingHalus/listDailyIncomingHalus"));
  }
//   Delete Data Daily Incoming Halus

// Tolak Incoming Halus
public function tolakIncomingHalus($id=0){
    $data['codepage']              = "back_addProduct";
    $data['page_title'] 	       = 'Tolak Incoming Kasar';
    $data['dailyIncomingHalus']    = $this->DailyIncomingHalus->getDailyIncomingHalusById($id)->row_array();

    $status='2';
   {
      $data_incoming_halus = array(
      
          'id_status'                => @$status
      );

      $data = $this->DailyIncomingHalus->tolakIncomingHalus($id,$data_incoming_halus);
    }
    $this->session->set_flashdata('success_msg_register', 'Data berhasil ditolak !');  
    redirect(base_url("admin/DailyIncomingHalus/listDailyIncomingHalus"));

  }

// End Tolak Incoming Halus

// Setuju Incoming Halus
public function setujuIncomingHalus($id=0){
    $data['codepage']              = "back_addProduct";
    $data['page_title'] 	       = 'Setuju Incoming Kasar';
    $data['dailyIncomingHalus']    = $this->DailyIncomingHalus->getDailyIncomingHalusById($id)->row_array();

    $status='1';
   {
      $data_incoming_halus = array(
      
          'id_status'                => @$status
      );

      $data = $this->DailyIncomingHalus->setujuIncomingHalus($id,$data_incoming_halus);
    }
    $this->session->set_flashdata('success_msg_register', 'Data berhasil disetujui !');  
    redirect(base_url("admin/DailyIncomingHalus/listDailyIncomingHalus"));

  }

// End Setuju Incoming Halus

// Edit dan View Daily Incoming Halus
public function detailIncomingHalus($id=0){
  $data['codepage']              = "back_useradmin";
  $data['page_title'] 	         = "Detail Daily Incoming Halus";
  $data['dailyIncomingHalus']    = $this->DailyIncomingHalus->getDailyIncomingHalusById($id)->row_array();
  // $data['material']           = $this->DailyIncomingKasar->getMaterialById($id)->row_array();
  $data['material']              = $this->DailyIncomingHalus->getMaterial()->result_array();
  $id                            = $_SESSION['id'];
  $data['image']                 = $this->user->getImage($id)->result_array();

  if ($_SESSION['id'] == true) { 
    $this->session->set_userdata($_SESSION); 
    base_url('admin/DailyIncomingHalus/detailIncomingHalus/');

  } else { 
    $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
    redirect(base_url('login_admin'));
  }
 
  $this->template->back_views('site/back/dailyIncomingHalusDetail',$data);
  
}

// End Edit dan View Daily Incoming Halus

// Udpate Daily Incoming Halus
public function updateIncomingHalus($id=0){
  $data['codepage']            = "back_addProduct";
  $data['page_title'] 	       = 'Update Incoming Halus';
  $data['dailyIncomingHalus']  = $this->DailyIncomingHalus->getDailyIncomingHalusById($id)->row_array();
  $data['material']            = $this->DailyIncomingHalus->getMaterialById($id)->row_array();
  


 {
  $config['upload_path']    ='./assets/img/content/DailyIncomingHalus/';
  $config['allowed_types']  ='jpg|png|ico';
  $config['encrypt_name']   = TRUE;

  $this->load->library('upload',$config);
  if($this->upload->do_upload('doc_img'))
  {
      $img ='img/content/DailyIncomingHalus/';
      $img.=  $this->upload->data('file_name');
  }

  $data_daily_incoming_halus = array(

    
    'surat_jalan'                => $_POST['surat_jalan'],
    'kriteria_visual_butiran'    => $_POST['kriteria_visual_butiran'],
    'kriteria_visual_kekerasan'  => $_POST['kriteria_visual_kekerasan'],
    'kriteria_visual_warna'      => $_POST['kriteria_visual_warna'],
    'slit_content'               => $_POST['slit_content'],
    'keterangan'                 => $_POST['keterangan'],
    'doc_img'                    => @$img
  );
    $data = $this->DailyIncomingHalus->updateIncomingHalus($id,$data_daily_incoming_halus);
 }
    $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
    redirect(base_url("admin/DailyIncomingHalus/listDailyIncomingHalus"));  

}
// End Udpate Daily Incoming Halus

}

/* End of file User.php */
