<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class TahunAjaran extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_tahun_ajaran','tahun_ajaran');
    $this->load->library('email');
    $this->load->library('session');
    
  }

  public function addTahunAjaran(){
    $data['codepage'] = "back_addProduct";
    $data['page_title'] 	= 'Tambah Ustadz';
    if(isset($_POST['submit'])){
    
      $data_ajaran = array(
      
        'tahun_ajaran'         => $_POST['tahun_ajaran'] 
        
      );
      $data = $this->tahun_ajaran->create_tahun($data_ajaran);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
      redirect(base_url('admin/TahunAjaran/listTahunAjaran'));

    }
    
  }

  public function formAddTahunAjaran(){
    $data['codepage']     = "back_addProduct";
    $data['page_title'] 	= 'Tambah Tahun Ajaran';
    $data['tahun_ajaran'] = $this->tahun_ajaran->getTahunAjaran()->result_array();
    $id = $_SESSION['id'];
    $data['image']    = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/TahunAjaran/listTahunAjaran');

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    $this->template->back_views('site/back/tahunAjaranAdd',$data);

  }

 

  public function listTahunAjaran(){
    $data['codepage']     = "back_user";
    $data['page_title'] 	= 'List Data Tahun Ajaran';
    $data['tahun_ajaran'] = $this->tahun_ajaran->getTahunAjaran()->result_array();
    $id = $_SESSION['id'];
    $data['image']        = $this->user->getImage($id)->result_array();
    
    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/TahunAjaran/listTahunAjaran');

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }
  
    $this->template->back_views('site/back/tahunAjaranList',$data);
    
  }

  public function detailTahunAjaran($th=0){
    $data['codepage']     = "back_useradmin";
    $data['page_title'] 	= "Edit Tahun Ajaran";
    $data['tahun_ajaran'] = $this->tahun_ajaran->getTahunAjaranById($th)->row_array();
    $id                   = $_SESSION['id'];
    $data['useradmin']    = $this->user->getUserAdminById($id)->row_array();
    $data['image']        = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/TahunAjaran/detailTahunAjaran/'.$_SESSION['id']);

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    if(isset($_POST['submit'])){

      $data_ajaran = array(
      
        'tahun_ajaran'    => $_POST['tahun_ajaran'] 
        
      );
      $data = $this->tahun_ajaran->updateTahunAjaran($th,$data_ajaran);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url('admin/TahunAjaran/listTahunAjaran'));
    
  }
  $this->template->back_views('site/back/tahunAjaranEdit',$data);
  }
  
  public function del_tahun($th){
    $this->tahun_ajaran->delTahun($th);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/TahunAjaran/listTahunAjaran"));
}
}

/* End of file User.php */
