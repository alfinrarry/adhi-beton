<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_santri','santri');
    $this->load->model('Mod_absensi','absensi');
    $this->load->library('email');
    $this->load->library('session');
    
  }
  
  public function detailAbsensi($nis=0){
    $data['codepage']       = "back_useradmin";
    $data['page_title'] 	  = "Detail Absensi";
    $data['absensi']        = $this->absensi->getAbsensiByNis($nis)->row_array();
    $id                     = $_SESSION['id'];
    $data['useradmin']      = $this->user->getUserAdminById($id)->row_array();
    $data['image']          = $this->user->getImage($id)->result_array();
    $data['komplek']        = $this->santri->getListKomplek()->result_array();
    $data['kelas']          = $this->santri->getListKelas()->result_array();
    $data['th']             = $this->absensi->getTahunAjaran()->result_array();
    $data['smt']            = $this->absensi->getSemester()->result_array();
    $data['bln']            = $this->absensi->getBulan()->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/absensi/listAbsensi/');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    $absensi                = $this->absensi->getAbsensiByNis($nis)->row_array();
    $nis                    = $absensi['nis'];
    $kelas                  = $absensi['id_kelas'];

   
  
    if(isset($_POST['submit'])){

      $data = array(
        'semester'         => $_POST['semester'] ,
        'sakit'            => $_POST['sakit'],
        'izin'             => $_POST['izin'] ,
        'alpha'            => $_POST['alpha'] ,
        'bulan'            => $_POST['bulan'] ,
        'tahun_ajaran'     => $_POST['tahun_ajaran'] 
 
      );
      $data = $this->absensi->updateAbsensiSantri($nis,$data);
      
      $data = array(
        'nis'              => @$nis,
        'id_kelas'         => @$kelas,
        'semester'         => $_POST['semester'] ,
        'sakit'            => $_POST['sakit'],
        'izin'             => $_POST['izin'] ,
        'alpha'            => $_POST['alpha'] ,
        'bulan'            => $_POST['bulan'] ,
        'tahun_ajaran'     => $_POST['tahun_ajaran'] 
   
      );
      $data = $this->absensi->addRekapAbsensiSantri($data);
      $this->session->set_flashdata('success_msg', 'Data Berhasil Di Input, Silahkan Cek Perubahan Data di Rekap Absensi');
        redirect($_SERVER['HTTP_REFERER']);
    
  }
  $this->template->back_views('site/back/detailAbsensi',$data);
    
  }

  public function listAbsensi(){
    $data['codepage']      = "back_user";
    $data['page_title'] 	 = 'Cari Kelas';
    $data['user']          = $this->user->getListUser()->result_array();
    $data['absensi']         = $this->absensi->getListAbsen()->result_array();
    $data['santri']        = $this->santri->getListSantri()->result_array();
    $data['komplek']       = $this->santri->getListKomplek()->result_array();
    $id = $_SESSION['id'];
    $data['image']    = $this->user->getImage($id)->result_array();
    
    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/santri/formAddSantri');

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    $this->template->back_views('site/back/absensiCariKelas',$data);
    
  }

  public function pencarian()
  {
    $data['codepage']      = "back_useradmin";
    $data['page_title'] 	 = "Riwayat Terakhir Input Absensi Santri";
    $id                    = $_SESSION['id'];
    $data['image']         = $this->user->getImage($id)->result_array();
    $data['useradmin']     = $this->user->getUserAdminById($id)->row_array();
    $data['santri']        = $this->santri->getListSantri()->result_array();
    $data['kelas']         = $this->absensi->getKelasById($id)->row_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Absensi/listAbsensi/');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

  
  $kelas=$this->input->get('kelas');
  $data['hasil'] = $this->absensi->pencarian_absen($kelas)->result_array();
  $this->template->back_views('site/back/absensiList',$data);

  }  

  

}

/* End of file User.php */
