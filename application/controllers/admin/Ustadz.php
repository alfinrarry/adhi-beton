<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Ustadz extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_ustadz','ustadz');
    $this->load->library('email');
    $this->load->library('session');
    
  }
 
  public function addUstadz(){
    $data['codepage']     = "back_addProduct";
    $data['page_title'] 	= 'Tambah Ustadz';

    if(isset($_POST['submit'])){
    
      $data_ustadz = array(
      
        'nama_ustadz'      => $_POST['nama_ustadz'] ,
        'alamat'           => $_POST['alamat'],
        'no_telp'          => $_POST['no_telp'] ,
        'created_at'       => date('Y-m-j H:i:s'),
        'updated_at'       => date('Y-m-j H:i:s')
      );
      $data= $this->ustadz->create_ustadz($data_ustadz);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
      redirect(base_url('admin/ustadz/listUstadz'));

    }
    
  }

  public function formAddUstadz(){
    $data['codepage']         = "back_addProduct";
    $data['page_title'] 	    = 'Tambah Ustadz';
    $data['userAdminRole']    = $this->user->getAllRole()->result_array();
    $id                       = $_SESSION['id'];
    $data['image']            = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Ustadz/formAddUstadz');

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    $this->template->back_views('site/back/ustadzAdd',$data);
  }

  public function listUstadz(){
    $data['codepage']     = "back_user";
    $data['page_title'] 	= 'List Data Ustadz';
    $data['ustadz']       = $this->ustadz->getListUstadz()->result_array();
    $id                   = $_SESSION['id'];
    $data['image']        = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Ustadz/listUstadz');

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }
    
    $this->template->back_views('site/back/ustadzList',$data);
  }

  public function detailUstadz($id=0){
    $data['codepage']     = "back_useradmin";
    $data['page_title'] 	= "Detail Ustadz";
    $data['ustadz']       = $this->ustadz->getUstadzById($id)->row_array();
    $data['useradmin']    = $this->user->getUserAdminById($id)->row_array();
    $data['image']        = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Ustadz/detailUstadz/'.$_SESSION['id']);

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

  
    if(isset($_POST['submit'])){
  
      $data_ustadz = array(
      
        'nama_ustadz'      => $_POST['nama_ustadz'] ,
        'alamat'           => $_POST['alamat'],
        'no_telp'          => $_POST['no_telp'] ,
        'created_at'       => date('Y-m-j H:i:s'),
        'updated_at'       => date('Y-m-j H:i:s')
      );
      $data = $this->ustadz->updateDataUstadz($id,$data_ustadz);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url('admin/ustadz/listUstadz'));
    
  }
  $this->template->back_views('site/back/ustadzEdit',$data);
    
  }
  
  public function del_ustadz($id){
    $this->ustadz->delUstadz($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/ustadz/listUstadz"));
}
}

/* End of file User.php */
