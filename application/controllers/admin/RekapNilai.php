<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class RekapNilai extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_santri','santri');
    $this->load->model('Mod_kelas','kelas');
    $this->load->model('Mod_nilai','nilai');
    $this->load->model('Mod_rekap_nilai','rekap');
    $this->load->library('email');
    $this->load->library('session');
    
  }

  public function listRekapNilai(){
    $data['codepage']     = "back_useradmin";
    $data['page_title'] 	= 'Cari Rekap Nilai Santri';
    $data['santri']       = $this->santri->getListSantri()->result_array();
    $data['kelas']        = $this->santri->getListKelas()->result_array();
    $id                   = $_SESSION['id'];
    $data['image']        = $this->user->getImage($id)->result_array();
    $data['th']           = $this->rekap->getTahunAjaran()->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/RekapNilai/listRekapNilai/');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }
 
    $this->template->back_views('site/back/rekapNilaiList',$data);
    
  }

  public function pencarian()
  {

  $data['codepage']      = "back_useradmin";
  $data['page_title'] 	 = "Hasil Pencarian Rekap Nilai";
  $id                    = $_SESSION['id'];
  $data['image']         = $this->user->getImage($id)->result_array();
  $data['useradmin']     = $this->user->getUserAdminById($id)->row_array();
  $data['santri']        = $this->santri->getListSantri()->result_array();
  $data['kelas']         = $this->nilai->getKelasById($id)->row_array();
  $data['th']            = $this->nilai->getTahunAjaran()->result_array();
  
  if ($_SESSION['id'] == true) { 
    $this->session->set_userdata($_SESSION); 
    base_url('admin/RekapNilai/listRekapNilai/');

  } else { 
    $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
    redirect(base_url('login_admin'));
  }

  $kelas                 = 18;
  $kelas                 = $this->input->get('kelas');
  $semester              = $this->input->get('semester');
  $tahun_ajaran          = $this->input->get('tahun_ajaran');
  $data['hasil']         = $this->rekap->pencarian_santri($kelas,$semester,$tahun_ajaran)->result_array();       
          
  if ($kelas <= 1 )
  {        
  $this->template->back_views('site/back/rekapNilaiCariKelas',$data);
  }
  elseif ($kelas <=8)
  {
    $this->template->back_views('site/back/rekapNilaiCariKelasUla',$data);
  }
  elseif ($kelas <=13)
  {
    $this->template->back_views('site/back/rekapNilaiCariKelasWustho',$data);
  }
  elseif ($kelas >=14)
  {
    $this->template->back_views('site/back/rekapNilaiCariKelasUlya',$data);
  }

  }


  public function deleteRekap($id=0){
    $this->rekap->delRekap($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/RekapNilai/listRekapNilai"));
}

  


}

/* End of file User.php */
