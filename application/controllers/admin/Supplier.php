<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_Supplier','supplier');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List  Supplier
   public function listSupplier(){
    $data['codepage']            = "back_addProduct";
    $data['page_title']   	     = 'List Supplier';
    $data['userAdminRole']       = $this->user->getAllRole()->result_array();
    $data['supplier']            = $this->supplier->getSupplier()->result_array();
    $id                          = $_SESSION['id'];
    $data['image']               = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/Supplier/listSupplier');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/supplierList',$data);
  }
    // End List  Supplier

 // Add Supplier
 public function inputSupplier(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Supplier';
 {
    $data_supplier = array(
    
      'supplier_name'   => $_POST['supplier_name']
    );
    $data = $this->supplier->inputSupplier($data_supplier);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
  redirect(base_url("admin/Supplier/listSupplier"));

}
// End Add Supplier

    // Form Supplier
    public function formSupplier(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']     = 'Form Add Supplier';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/Supplier/formSupplier');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/supplierForm',$data);
    }
    // End Form Supplier

      // Form Edit Supplier
      public function formEditSupplier($id=0){
        $data['codepage']            = "back_addProduct";
        $data['page_title']        	 = 'Form Edit Supplier';
        $data['userAdminRole']       = $this->user->getAllRole()->result_array();
        $data['supplier']            = $this->supplier->getSupplierById($id)->row_array();
        $id                          = $_SESSION['id'];
        $data['image']               = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/Supplier/formEditSupplier');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/supplierEditForm',$data);
      }
      // End Form Edit Supplier

       // Update Supplier
    public function updateSupplier($id=0){
      $data['codepage']              = "back_addProduct";
      $data['page_title'] 	         = 'Update Supplier';
      $data['supplier']              = $this->supplier->getSupplierById($id)->row_array();

     {
        $data_supplier = array(
        
          'supplier_name'          => $_POST['supplier_name']
        );
        $data = $this->supplier->updateSupplier($id,$data_supplier);
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url("admin/Supplier/listSupplier"));

    }

        // End Update Supplier

  
//   Delete Data Supplier
  public function delSupplier($id){
    $data= $this->supplier->delSupplier($id);
           $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/Supplier/listSupplier"));
  }
//   Delete Data Supplier
}

/* End of file User.php */
