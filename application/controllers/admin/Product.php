<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_customer','customer');
    $this->load->model('Mod_product','product');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List Project
   public function listProduct(){
    $data['codepage']       = "back_addProduct";
    $data['page_title']   	= 'List Product';
    $data['userAdminRole']  = $this->user->getAllRole()->result_array();
    $data['product']        = $this->product->getProduct()->result_array();
    $id                     = $_SESSION['id'];
    $data['image']          = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/product/listProduct');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/productList',$data);
  }
    // End List Customer

 // Add Product
 public function inputProduct(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Product';
 {
    $data_product = array(
    
      'name_product'    => $_POST['name_product']
    );
    $data = $this->product->inputProduct($data_product);
  }
  redirect(base_url("admin/Product/listProduct"));

}
// End Add Product

    // Form Project
    public function formProduct(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']   	= 'Form Add Product';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/product/formProduct');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/productForm',$data);
    }
    // End Form Edit Project

      // Form Project
      public function formEditProduct($id=0){
        $data['codepage']       = "back_addProduct";
        $data['page_title']   	= 'Form Edit Product';
        $data['userAdminRole']  = $this->user->getAllRole()->result_array();
        $data['product']        = $this->product->getProductById($id)->row_array();
        $id                     = $_SESSION['id'];
        $data['image']          = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/Product/formEditProduct');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/productEditForm',$data);
      }
      // End Form Edit Customer

       // Update Product
    public function updateProduct($id=0){
      $data['codepage']     = "back_addProduct";
      $data['page_title'] 	= 'Update Product';
      $data['product']      = $this->product->getProductById($id)->row_array();

     {
        $data_product = array(
        
          'name_product'    => $_POST['name_product']
        );
        $data = $this->product->updateProduct($id,$data_product);
      }
      redirect(base_url("admin/Product/listProduct"));

    }

        // End Update Product

  
//   Delete Data Product
  public function delProduct($id){
    $data= $this->product->delProduct($id);
    $this->product->delProduct($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/Product/listProduct"));
  }
//   Delete Data Product

}

/* End of file User.php */
