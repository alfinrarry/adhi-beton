<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class SumberMaterialAdditive extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_SumberMaterial','SumberMaterial');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List  Sumber Material Additive
   public function listSumberMaterialAdditive(){
    $data['codepage']            = "back_addProduct";
    $data['page_title']   	     = 'List Sumber Material Additive';
    $data['userAdminRole']       = $this->user->getAllRole()->result_array();
    $data['sumberMaterial']      = $this->SumberMaterial->getSumberMaterialAdditive()->result_array();
    $id                          = $_SESSION['id'];
    $data['image']               = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/SumberMaterialAdditive/listSumberMaterialAdditive');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/sumberMaterialAdditiveList',$data);
  }
    // End List  Sumber Material Additive

 // Add Sumber Material Additive
 public function inputSumberMaterialAdditive(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Sumber Material Additive';
 {
    $data_sumber_material = array(
    
      'tempat_sumber'            => $_POST['tempat_sumber']
    );
    $data = $this->SumberMaterial->inputSumberMaterialAdditive($data_sumber_material);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
  redirect(base_url("admin/SumberMaterialAdditive/listSumberMaterialAdditive"));

}
// End Add Sumber Material Additive

    // Form Sumber Material Additive
    public function formSumberMaterialAdditive(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']     = 'Form Add Sumber Material Additive';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/SumberMaterialAdditive/formSumberMaterialAdditive');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/sumberMaterialAdditiveForm',$data);
    }
    // End Form Sumber Material Additive

      // Form Edit Sumber Material Additive
      public function formEditSumberMaterialAdditive($id=0){
        $data['codepage']            = "back_addProduct";
        $data['page_title']        	 = 'Form Edit Sumber Material Additive';
        $data['userAdminRole']       = $this->user->getAllRole()->result_array();
        $data['sumberMaterial']      = $this->SumberMaterial->getSumberMaterialAdditiveById($id)->row_array();
        $id                          = $_SESSION['id'];
        $data['image']               = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/SumberMaterialAdditive/formEditSumberMaterialAdditive');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/sumberMaterialAdditiveEditForm',$data);
      }
      // End Form Edit SumberMaterial Additive

       // Update Sumber Material Additive
    public function updateSumberMaterialAdditive($id=0){
      $data['codepage']              = "back_addProduct";
      $data['page_title'] 	         = 'Update Additive';
      $data['sumberMaterial']        = $this->SumberMaterial->getSumberMaterialAdditiveById($id)->row_array();

     {
        $data_sumber_material = array(
        
            'tempat_sumber'     => $_POST['tempat_sumber']
        );
        $data = $this->SumberMaterial->updateSumberMaterialAdditive($id,$data_sumber_material);
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url("admin/SumberMaterialAdditive/listSumberMaterialAdditive"));

    }

        // End Update Sumber Material Additive

  
//   Delete Data Sumber Material Additive
  public function delSumberMaterialAdditive($id){
    $data= $this->SumberMaterial->delSumberMaterialAdditive($id);
           $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/SumberMaterialAdditive/listSumberMaterialAdditive"));
  }
//   Delete Data Sumber Material Additive
}

/* End of file User.php */
