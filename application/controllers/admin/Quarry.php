<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Quarry extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_Quarry','quarry');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List  Quarry
   public function listQuarry(){
    $data['codepage']            = "back_addProduct";
    $data['page_title']   	     = 'List Quarry';
    $data['userAdminRole']       = $this->user->getAllRole()->result_array();
    $data['quarry']              = $this->quarry->getQuarry()->result_array();
    $id                          = $_SESSION['id'];
    $data['image']               = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/Quarry/listQuarry');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/quarryList',$data);
  }
    // End List  Quarry

 // Add Quarry
 public function inputQuarry(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Quarry';
 {
    $data_quarry = array(
    
      'name_quarry'     => $_POST['name_quarry']
    );
    $data = $this->quarry->inputQuarry($data_quarry);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
  redirect(base_url("admin/Quarry/listQuarry"));

}
// End Add Quarry

    // Form Quarry
    public function formQuarry(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']     = 'Form Add Quarry';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/Quarry/formQuarry');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/quarryForm',$data);
    }
    // End Form Quarry

      // Form Edit Quarry
      public function formEditQuarry($id=0){
        $data['codepage']            = "back_addProduct";
        $data['page_title']        	 = 'Form Edit Quarry';
        $data['userAdminRole']       = $this->user->getAllRole()->result_array();
        $data['quarry']              = $this->quarry->getQuarryById($id)->row_array();
        $id                          = $_SESSION['id'];
        $data['image']               = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/Quarry/formEditQuarry');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/quarryEditForm',$data);
      }
      // End Form Edit Quarry

       // Update Quarry
    public function updateQuarry($id=0){
      $data['codepage']              = "back_addProduct";
      $data['page_title'] 	         = 'Update Quarry';
      $data['quarry']                = $this->quarry->getQuarryById($id)->row_array();

     {
        $data_quarry = array(
        
          'name_quarry'              => $_POST['name_quarry']
        );
        $data = $this->quarry->updateQuarry($id,$data_quarry);
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url("admin/Quarry/listQuarry"));

    }

        // End Update Quarry

  
//   Delete Data Quarry
  public function delQuarry($id){
    $data= $this->quarry->delQuarry($id);
           $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/Quarry/listQuarry"));
  }
//   Delete Data Quarry
}

/* End of file User.php */
