<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class SumberMaterialSemen extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_SumberMaterial','SumberMaterial');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List  Sumber Material Semen
   public function listSumberMaterialSemen(){
    $data['codepage']            = "back_addProduct";
    $data['page_title']   	     = 'List Sumber Material Semen';
    $data['userAdminRole']       = $this->user->getAllRole()->result_array();
    $data['sumberMaterial']      = $this->SumberMaterial->getSumberMaterialSemen()->result_array();
    $id                          = $_SESSION['id'];
    $data['image']               = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/SumberMaterialSemen/listSumberMaterialSemen');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/sumberMaterialSemenList',$data);
  }
    // End List  Sumber Material Semen

 // Add Sumber Material Semen
 public function inputSumberMaterialSemen(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Sumber Material Semen';
 {
    $data_sumber_material = array(
    
      'tempat_sumber'            => $_POST['tempat_sumber']
    );
    $data = $this->SumberMaterial->inputSumberMaterialSemen($data_sumber_material);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
  redirect(base_url("admin/SumberMaterialSemen/listSumberMaterialSemen"));

}
// End Add Sumber Material Semen

    // Form Sumber Material Semen
    public function formSumberMaterialSemen(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']     = 'Form Add Sumber Material Semen';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/SumberMaterialSemen/formSumberMaterialSemen');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/sumberMaterialSemenForm',$data);
    }
    // End Form Sumber Material Semen

      // Form Edit Sumber Material Semen
      public function formEditSumberMaterialSemen($id=0){
        $data['codepage']            = "back_addProduct";
        $data['page_title']        	 = 'Form Edit Sumber Material Semen';
        $data['userAdminRole']       = $this->user->getAllRole()->result_array();
        $data['sumberMaterial']      = $this->SumberMaterial->getSumberMaterialSemenById($id)->row_array();
        $id                          = $_SESSION['id'];
        $data['image']               = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/SumberMaterialSemen/formEditSumberMaterialSemen');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/sumberMaterialSemenEditForm',$data);
      }
      // End Form Edit SumberMaterial Semen

       // Update Sumber Material Semen
    public function updateSumberMaterialSemen($id=0){
      $data['codepage']              = "back_addProduct";
      $data['page_title'] 	         = 'Update Semen';
      $data['sumberMaterial']        = $this->SumberMaterial->getSumberMaterialSemenById($id)->row_array();

     {
        $data_sumber_material = array(
        
            'tempat_sumber'     => $_POST['tempat_sumber']
        );
        $data = $this->SumberMaterial->updateSumberMaterialSemen($id,$data_sumber_material);
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url("admin/SumberMaterialSemen/listSumberMaterialSemen"));

    }

        // End Update Sumber Material Semen

  
//   Delete Data Sumber Material Semen
  public function delSumberMaterialSemen($id){
    $data= $this->SumberMaterial->delSumberMaterialSemen($id);
           $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/SumberMaterialSemen/listSumberMaterialSemen"));
  }
//   Delete Data Sumber Material Semen
}

/* End of file User.php */
