<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class RekapAbsensi extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_santri','santri');
    $this->load->model('Mod_kelas','kelas');
    $this->load->model('Mod_nilai','nilai');
    $this->load->model('Mod_rekap_absensi','rekap');
    $this->load->library('email');
    $this->load->library('session');
    
  }
  
  public function cariSantri(){
    $data['codepage']     = "back_useradmin";
    $data['page_title'] 	= 'Cari Santri';
    $data['santri']       = $this->santri->getListSantri()->result_array();
    $data['kelas']        = $this->santri->getListKelas()->result_array();
    $id                   = $_SESSION['id'];
    $data['image']        = $this->user->getImage($id)->result_array();
    
    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/RekapAbsensi/listRekapAbsensi');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }
    
    $this->template->back_views('site/back/nilaiCari',$data);
    
  }

 

  public function listRekapAbsensi(){
    $data['codepage']     = "back_useradmin";
    $data['page_title'] 	= 'Cari Rekap Absensi Santri';
    $data['santri']       = $this->santri->getListSantri()->result_array();
    $data['kelas']        = $this->santri->getListKelas()->result_array();
    $id                   = $_SESSION['id'];
    $data['image']        = $this->user->getImage($id)->result_array();
    $data['th']           = $this->rekap->getTahunAjaran()->result_array();
    $data['bln']          = $this->rekap->getBulan()->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/RekapAbsensi/listRekapAbsensi/');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    $this->template->back_views('site/back/rekapAbsensiList',$data);
    
  }

  

public function pencarian()
  {
    $data['codepage']      = "back_useradmin";
    $data['page_title'] 	 = "Hasil Pencarian  Santri";
    $id                    = $_SESSION['id'];
    $data['image']         = $this->user->getImage($id)->result_array();
    $data['useradmin']     = $this->user->getUserAdminById($id)->row_array();
    $data['santri']        = $this->santri->getListSantri()->result_array();
    $data['kelas']         = $this->nilai->getKelasById($id)->row_array();
    $data['th']            = $this->rekap->getTahunAjaran()->result_array();
    $data['smt']           = $this->rekap->getSemester()->result_array();
    $data['bln']           = $this->rekap->getBulan()->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/RekapAbsensi/listRekapAbsensi/');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    $kelas                 = $this->input->get('kelas');
    $semester              = $this->input->get('semester');
    $tahun_ajaran          = $this->input->get('tahun_ajaran');
    $bulan                 = $this->input->get('bulan');
    $data['hasil'] = $this->rekap->pencarian_santri($kelas,$semester,$tahun_ajaran,$bulan)->result_array();

    $this->template->back_views('site/back/rekapAbsensiCariKelas',$data);


  }  

  public function deleteRekap($id)
{
    $this->rekap->delRekap($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/RekapAbsensi/listRekapAbsensi"));
}



  


}

/* End of file User.php */
