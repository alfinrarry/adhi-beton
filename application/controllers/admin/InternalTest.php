<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class InternalTest extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_InternalHalus','InternalHalus');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

  // List Agregat Halus
  public function listAgregatHalus(){
    $data['codepage']       = "back_addProduct";
    $data['page_title']   	= 'Agregat Halus';
    $id                     = $_SESSION['id'];
    $data['image']          = $this->user->getImage($id)->result_array();
    $data['internalHalus']  = $this->InternalHalus->getInternalHalus()->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/InternalTest/listAgregatHalus');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/agregatHalusList',$data);
  }
    // List Agregat Halus

    // Form Agregat Halus
    public function formAgregatHalus(){
      $data['codepage']            = "back_addProduct";
      $data['page_title']   	     = 'Form Add Agregat Halus';
      $data['userAdminRole']       = $this->user->getAllRole()->result_array();
      $data['sampleSource']        = $this->InternalHalus->getSampleSource()->result_array();
      $data['sampleDescription']   = $this->InternalHalus->getSampleDescription()->result_array();
      $data['pelaksana']           = $this->InternalHalus->getPelaksana()->result_array();
      $data['toBeUsed']            = $this->InternalHalus->getToBeUsed()->result_array();
      $id                          = $_SESSION['id'];
      $data['image']               = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/InternalHalus/formAgregatHalus');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/agregatHalusForm',$data);
    }
    // Form Agregat Halus

    // Add Agregat Halus
    public function inputAgregatHalus(){
      $data['codepage']     = "back_addProduct";
      $data['page_title'] 	= 'Add Agregat Halus';
      $data['internalHalus']  = $this->InternalHalus->getInternalHalus()->result_array();
  
      {
        $config['upload_path']    ='./assets/img/content/InternalHalus/';
        $config['allowed_types']  ='jpg|png|ico';
        $config['encrypt_name']   = TRUE;
  
        $this->load->library('upload',$config);
        if($this->upload->do_upload('doc_input_satu'))
        {
            $img1 ='img/content/InternalHalus/';
            $img1.=  $this->upload->data('file_name');
        }
        if($this->upload->do_upload('doc_input_dua'))
        {
            $img2 ='img/content/InternalHalus/';
            $img2.=  $this->upload->data('file_name');
        }
        if($this->upload->do_upload('doc_input_tiga'))
        {
            $img3 ='img/content/InternalHalus/';
            $img3.=  $this->upload->data('file_name');
        }
        if($this->upload->do_upload('doc_input_empat'))
        {
            $img4 ='img/content/InternalHalus/';
            $img4.=  $this->upload->data('file_name');
        }
        if($this->upload->do_upload('doc_input_lima'))
        {
            $img5 ='img/content/InternalHalus/';
            $img5.=  $this->upload->data('file_name');
        }
        if($this->upload->do_upload('doc_input_enam'))
        {
            $img6 ='img/content/InternalHalus/';
            $img6.=  $this->upload->data('file_name');
        }
        $status='3';
        $data_agregat_halus = array(
        
          'id_sample_source'        => $_POST['id_sample_source'] ,
          'id_sample_description'   => $_POST['id_sample_description'],
          'id_pelaksana'            => $_POST['id_pelaksana'] ,
          'id_to_be_used'           => $_POST['id_to_be_used'] ,
          'received_date'           => date('Y-m-j'),
          'satu_input_satu'         => $_POST['satu_input_satu'] ,
          'satu_input_dua'          => $_POST['satu_input_dua'] ,
          'satu_input_tiga'         => $_POST['satu_input_tiga'] ,
          'satu_input_empat'        => $_POST['satu_input_empat'] ,
          'satu_input_lima'         => $_POST['satu_input_lima'] ,
          'satu_input_enam'         => $_POST['satu_input_enam'] ,
          'satu_input_tujuh'        => $_POST['satu_input_tujuh'] ,
          'satu_input_fm'           => $_POST['satu_input_fm'] ,
          'dua_input_satu'          => $_POST['dua_input_satu'] ,
          'dua_input_dua'           => $_POST['dua_input_dua'] ,
          'tiga_input_satu'         => $_POST['tiga_input_satu'] ,
          'empat_input_satu'        => $_POST['empat_input_satu'] ,
          'lima_input_satu'         => $_POST['lima_input_satu'],
          'enam_input_satu'         => $_POST['enam_input_satu'],
          'doc_input_satu'          => @$img1,
          'doc_input_dua'           => @$img2,
          'doc_input_tiga'          => @$img3,
          'doc_input_empat'         => @$img4,
          'doc_input_lima'          => @$img5,
          'doc_input_enam'          => @$img6,
          'id_status'               => @$status
        );
        $data = $this->InternalHalus->inputAgregatHalus($data_agregat_halus);
         
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
      redirect(base_url("admin/InternalTest/listAgregatHalus"));    }

        // End Add Agregat Halus

        // Edit dan View Internal Halus
public function detailInternalHalus($id=0){
  $data['codepage']              = "back_useradmin";
  $data['page_title'] 	         = "Detail Internal Halus";
  $data['internalHalus']         = $this->InternalHalus->getInternalHalusById($id)->row_array();
  $id                            = $_SESSION['id'];
  $data['image']                 = $this->user->getImage($id)->result_array();

  if ($_SESSION['id'] == true) { 
    $this->session->set_userdata($_SESSION); 
    base_url('admin/InternalHalus/detailInternalHalus/');

  } else { 
    $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
    redirect(base_url('login_admin'));
  }
 
  $this->template->back_views('site/back/agregatHalusDetail',$data);
  
}

// End Edit dan View Daily Incoming Additive


  // Tolak Incoming Internal Halus
public function tolakInternalHalus($id=0){
  $data['codepage']            = "back_addProduct";
  $data['page_title'] 	       = 'Tolak Incoming Halus';
  $data['internalHalus']       = $this->InternalHalus->getInternalHalusById($id)->row_array();

  $status='2';
 {
    $data_internal_halus = array(
    
        'id_status'                => @$status
    );

    $data = $this->InternalHalus->tolakInternalHalus($id,$data_internal_halus);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditolak !');  
  redirect(base_url("admin/InternalTest/listAgregatHalus"));

}
// End Tolak Internal Halus

// Setuju Internal Halus
public function setujuInternalHalus($id=0){
  $data['codepage']              = "back_addProduct";
  $data['page_title'] 	         = 'Setuju Internal Halus';
  $data['internalHalus']       = $this->InternalHalus->getInternalHalusById($id)->row_array();

  $status='1';
 {
    $data_internal_halus = array(
    
        'id_status'                => @$status
    );

    $data = $this->InternalHalus->setujuInternalHalus($id,$data_internal_halus);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil disetujui !');  
  redirect(base_url("admin/InternalTest/listAgregatHalus"));

}
// End Setuju Internal Halus

public function delInternalHalus($id){
  $data= $this->InternalHalus->delInternalHalus($id);
   $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
   redirect(base_url("admin/InternalTest/listAgregatHalus"));
}

  

 

}

/* End of file User.php */
