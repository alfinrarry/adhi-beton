<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class JobMixFormula extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_JobMixFormula','JobMixFormula');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

  // List Agregat JobMixFormula
  public function listJobMixFormula(){
    $data['codepage']         = "back_addProduct";
    $data['page_title']   	  = ' List Job Mix Formula';
    $id                       = $_SESSION['id'];
    $data['image']            = $this->user->getImage($id)->result_array();
    $data['jmf']              = $this->JobMixFormula->getJobMixFormula()->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/JobMixFormula/listJobMixFormula');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/jobMixFormulaList',$data);
  }
    // List Agregat Job Mix Formula

    // Form Agregat Job Mix Formula
    public function formJobMixFormula(){
      $data['codepage']            = "back_addProduct";
      $data['page_title']   	   = 'Form Add Job Mix Formula';
      $data['userAdminRole']       = $this->user->getAllRole()->result_array();
      $data['product']             = $this->JobMixFormula->getProduct()->result_array();
      $data['project']             = $this->JobMixFormula->getProject()->result_array();
      $data['mutu']                = $this->JobMixFormula->getMutu()->result_array();
      $data['sumberSemen']         = $this->JobMixFormula->getSumberSemen()->result_array();
      $data['sumberHalus']         = $this->JobMixFormula->getSumberHalus()->result_array();
      $data['sumberKasar']         = $this->JobMixFormula->getSumberKasar()->result_array();
      $data['sumberAdditive']      = $this->JobMixFormula->getSumberAdditive()->result_array();
      $id                          = $_SESSION['id'];
      $data['image']               = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/JobMixFormula/formJobMixFormula');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/jobMixFormulaForm',$data);
    }
    // Form Agregat Job Mix Formula

    // Add Agregat Job Mix Formula
    public function inputJobMixFormula(){
      $data['codepage']         = "back_addProduct";
      $data['page_title'] 	    = 'Add Job Mix Formula';
      $data['jmf']              = $this->JobMixFormula->getJobMixFormula()->result_array();
  
      {
        $config['upload_path']    ='./assets/img/content/JobMixFormula/';
        $config['allowed_types']  ='jpg|png|ico|pdf|docx';
  
        $this->load->library('upload',$config);
        if($this->upload->do_upload('doc_img'))
        {
            $img ='img/content/JobMixFormula/';
            $img.=  $this->upload->data('file_name');
        }
       $status = 3;
        $data_jmf = array(
          'name_jmf'              => $_POST['name_jmf'] ,
          'id_product'            => $_POST['id_product'] ,
          'id_mutu'               => $_POST['id_mutu'] ,
          'id_sumber_semen'       => $_POST['id_sumber_semen'] ,
          'jumlah_semen'          => $_POST['jumlah_semen'] ,
          'id_sumber_halus'       => $_POST['id_sumber_halus'] ,
          'jumlah_halus'          => $_POST['jumlah_halus'] ,
          'id_sumber_kasar'       => $_POST['id_sumber_kasar'] ,
          'jumlah_kasar'          => $_POST['jumlah_kasar'] ,
          'id_sumber_additive'    => $_POST['id_sumber_additive'] ,
          'jumlah_additive'       => $_POST['jumlah_additive'] ,
          'jumlah_air'            => $_POST['jumlah_air'] ,
          'doc_img'               => @$img,
          'id_status'             => @$status
        );
        $data = $this->JobMixFormula->inputJobMixFormula($data_jmf);

        $status = 3;
        $data_sft = array(
          'name_jmf'              => $_POST['name_jmf'],
          'id_product'            => $_POST['id_product'],
          'id_mutu'               => $_POST['id_mutu'],
          'id_status'             => @$status
        );
        $data = $this->JobMixFormula->inputSlumpFlowTest($data_sft);

        $status = 3;
        $kosong = 'Belum Terisi';
        $data_compressive = array(
          'name_jmf'              => $_POST['name_jmf'],
          'id_product'            => $_POST['id_product'],
          'id_project'            => @$kosong,
          'customer_name'         => @$kosong,
          'id_mutu'               => $_POST['id_mutu'],
          'id_status'             => @$status
        );
        $data = $this->JobMixFormula->inputDailyCompressive($data_compressive);
         
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
      redirect(base_url("admin/JobMixFormula/listJobMixFormula"));    }

        // End Add Job Mix Formula

        // Edit dan View  Job Mix Formula
    public function detailJobMixFormula($id=0){
      $data['codepage']              = "back_useradmin";
      $data['page_title'] 	         = "Detail Job Mix Formula";
      $data['jmf']                   = $this->JobMixFormula->getJobMixFormulaById($id)->row_array();
      $id                            = $_SESSION['id'];
      $data['image']                 = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/JobMixFormula/detailJobMixFormula/');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
        redirect(base_url('login_admin'));
      }
  
      $this->template->back_views('site/back/jobMixFormulaDetail',$data);

    }

    // End Edit dan View Job Mix Formula

    public function delJobMixFormula($id){
      $data= $this->JobMixFormula->delJobMixFormula($id);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
      redirect(base_url("admin/JobMixFormula/listJobMixFormula"));
}
    
    public function downloadArchive(){
      if (isset($_GET['filename'])) {
        $filename    = $_GET['filename'];
        $back_dir    ="assets/";
        $file = $back_dir.$_GET['filename'];
     
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: private');
            header('Pragma: private');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            
            exit;
        } 
        else {
            $_SESSION['pesan'] = "Oops! File - $filename - not found ...";
            header("location:archiveList.php");
        }
    }
  }

}

/* End of file User.php */
