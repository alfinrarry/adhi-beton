<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_customer','customer');
    $this->load->model('Mod_project','project');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List Project
   public function listProject(){
    $data['codepage']       = "back_addProduct";
    $data['page_title']   	= 'List Project';
    $data['userAdminRole']  = $this->user->getAllRole()->result_array();
    $data['project']        = $this->project->getProject()->result_array();
    $id                     = $_SESSION['id'];
    $data['image']          = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/project/listProject');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/projectList',$data);
  }
    // End List Customer

 // Add Project
 public function inputProject(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Project';
  $data['customer']     = $this->customer->getCustomer()->result_array();
 {
    $data_project = array(
    
      'id_customer'      => $_POST['id_customer'],
      'project_name'     => $_POST['project_name']
    );
    $data = $this->project->inputProject($data_project);
  }
  redirect(base_url("admin/Project/listProject"));

}
// End Add Project

    // Form Project
    public function formProject(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']   	= 'Form Add Project';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $data['customer']       = $this->customer->getCustomer()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/Project/formProject');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/projectForm',$data);
    }
    // End Form Edit Project

      // Form Project
      public function formEditProject($id=0){
        $data['codepage']       = "back_addProduct";
        $data['page_title']   	= 'Form Edit Project';
        $data['userAdminRole']  = $this->user->getAllRole()->result_array();
        $data['project']        = $this->project->getProjectById($id)->row_array();
        $id                     = $_SESSION['id'];
        $data['image']          = $this->user->getImage($id)->result_array();
        $data['customer']       = $this->customer->getCustomer()->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/Project/formEditProject');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/projectEditForm',$data);
      }
      // End Form Edit Customer

       // Update Project
    public function updateProject($id=0){
      $data['codepage']     = "back_addProduct";
      $data['page_title'] 	= 'Update Customer';
      $data['project']      = $this->project->getProjectById($id)->row_array();

     {
        $data_project = array(
        
          'project_name'      => $_POST['name'], 
          'id_customer'       => $_POST['id_customer'] 
        );
        $data = $this->project->updateProject($id,$data_project);
      }
      redirect(base_url("admin/Project/listProject"));

    }

        // End Update Project

  
  
  public function delProject($id){
   $data= $this->project->delProject($id);
    $this->project->delProject($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/Project/listProject"));
  }
}

/* End of file User.php */
