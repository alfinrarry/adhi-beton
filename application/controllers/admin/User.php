<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }
  

  public function index()
  {
    $data['codepage']     = "back_login";
    $data['page_title'] 	= 'Login';

    if($this->user->logged_id()){ 
      base_url('login_admin'); 
    } else { 
      $this->form_validation->set_rules('email', 'E-mail', 'required'); 
      $this->form_validation->set_rules('password', 'Password', 'required'); //jika session belum terdaftar 
      if ($this->form_validation->run() == false) {
        base_url('login_admin'); 
      } else { 
        $data_user    = array(
          'email'     => $_POST['email'],
          'password'  => $_POST['password']
        );

        $checking = $this->user->checkLoginAdmin($data_user);

        if ($checking == true) { 
          foreach ($checking as $apps) {
            
              $session_data = array( 
               'id'         => $apps->idu,
               'username'   => $apps->username,
               'email'      => $apps->email, 
               'fullname'   => $apps->fullname,
               'is_ban'     => $apps->is_ban
              ); 
              $this->session->set_userdata($session_data); 
              redirect(base_url('admin/dashboard'));
          } 
        } else { 
          redirect(base_url('login_admin'));
        } 
      } 
    } 
    $this->template->back_views('site/back/login',$data);
  }
  public function logout(){
    $this->session->sess_destroy();    
    redirect(base_url('login_admin'));
  }

  public function create_user(){
    $data['codepage']     = "back_addProduct";
    $data['page_title'] 	= 'Add User Admin';

    if(isset($_POST['submit'])){

      $ban        =1;
      $firstname  = $_POST ['firstname'];
      $lastname   = $_POST ['lastname'];
      $fullname   = $firstname." ".  $lastname;
      $username   = $_POST ['username'];
      $data_admin = array(
      
        'username'         => $_POST['username'] ,
        'email'            => $_POST['email'],
        'password'         => hash_password($_POST['password']) ,
        'id_role'          => $_POST['id_role'] ,
        'created_at'       => date('Y-m-j H:i:s'),
        'is_ban'           => $ban
      );

      $data   = $this->user->create_useradmin($data_admin);

      

      $config['upload_path']    ='./assets/img/content/useradmin/';
      $config['allowed_types']  ='jpg|png|ico';
      $config['encrypt_name']   = TRUE;

      $this->load->library('upload',$config);
      if($this->upload->do_upload('img_path'))
      {
          $img ='img/content/useradmin/';
          $img.=  $this->upload->data('file_name');
      }
      $result = $this->user->getId($username)->row(); 
      $getId  = $result->id;
      $data_admin_detail   = array(
        'id_useradmin'     => @$getId,
        'fullname'         => $fullname ,
        'phone'            => $_POST['phone'] ,
        'img_path'         => @$img ,
        'created_at'       => date('Y-m-j H:i:s'),
      );
      
      $data = $this->user->create_useradminDetail($data_admin_detail);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
      redirect(base_url('admin/user/listUser'));
    }
    
  }

  public function formAddUser(){
    $data['codepage']         = "back_addProduct";
    $data['page_title'] 	    = 'Add User Admin';
    $data['userAdminRole']    = $this->user->getAllRole()->result_array();
    $id                       = $_SESSION['id'];
    $data['image']            = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/User/forAddUser');

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    $this->template->back_views('site/back/addUser',$data);
  }

 

  public function listUser(){
    $data['codepage']     = "back_user";
    $data['page_title'] 	= 'List User Admin';
    $data['user']         = $this->user->getListUser()->result_array();
    $id                   = $_SESSION['id'];
    $data['image']        = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/user/listUser/');

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }
    
    $this->template->back_views('site/back/listUser',$data);
  }

 
  public function detailAdmin($id=0){
    $data['codepage']       = "back_useradmin";
    $data['useradmin']      = $this->user->getUserAdminById($id)->row_array();
    $data['userAdminRole']  = $this->user->getAllRole()->result_array();
    $data['page_title'] 	  = 'Detail user '.$data['useradmin']['fullname'];
    $idadmin                = $_SESSION['id'];
    $data['image']          = $this->user->getImage($id)->result_array();
    $data['img']            = $this->user->getImg($id)->row_array();
    $user                   = $this->user->getUserAdminById($id)->row_array();
    $email                  = $user['email'];

    
    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/user/detailAdmin/'.$_SESSION['id']);

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }
   
  
    if(isset($_POST['submit'])){
  
      if($email != $_POST['emailedit']){
      $data_useradmin = array(
      
       
        'email'            => $_POST['emailedit'],
        
        'updated_at'       => date('Y-m-j H:i:s'),
      );
      $data = $this->user->updateDataAdmin($id,$data_useradmin);
    }
      $config['upload_path']='./assets/img/content/useradmin/';
      $config['allowed_types']='jpg|png|ico';
      $config['encrypt_name'] = TRUE;
      $this->load->library('upload',$config);

      if($this->upload->do_upload('img_path'))
    {
        $img='img/content/useradmin/';
        $img.=  $this->upload->data('file_name');
        $img = array(
          'img_path'         => @$img
        );
        $this->user->updateImgAdmin($img);
        
    }
      
    
      $data_useradminDetail = array(
        
        'fullname'         => $_POST['fullnameedit'] ,
        'phone'            => $_POST['phone'] ,
        'updated_at'       => date('Y-m-j H:i:s')
      );
      $data = $this->user->updateDataAdminDetail($id,$data_useradminDetail);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url('admin/user/listUser'));
    
  }
  $this->template->back_views('site/back/userProfile',$data);
    
  }

  
  public function editPasswordAct($id=0){
    $data['codepage']         = "back_useradmin_detail";
    $data['useradmin']        = $this->user->getUserAdminById($id)->row_array();
    $data['userAdminRole']    = $this->user->getAllRole()->result_array();
    $data['page_title'] 	    = 'Detail User '.$data['useradmin']['fullname'];
    $id_image                 = $_SESSION['id'];
    $data['image']            = $this->user->getImage($id_image)->result_array();
    $idadmin                  = $_SESSION['id'];
    $user                     = $this->user->getUserAdminById($id)->row_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/user/editPasswordAct/'.$_SESSION['id']);

    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }
    
    if(isset($_POST['submit'])){

      $newPassword    = $this->input->post('newPassword');
      $tryNewPassword = $this->input->post('tryNewPassword');

      $this->form_validation->set_rules('newPassword','Password Baru','required|matches[tryNewPassword]');
      $this->form_validation->set_rules('tryNewPassword','Ulangi Password Baru','required');
      if($this->form_validation->run()!= false){
        $data = array('password' => hash_password($newPassword));
        
        $this->user->updateDataAdmin($id,$data);
      }
      else{
        $this->session->set_flashdata('fail_msg_register', 'Update password gagal, silahkan ulangi lagi.');  
        redirect(base_url('admin/user/editPasswordAct/'.$_SESSION['id']));


      }
      $this->session->set_flashdata('success_msg_register', 'Update password berhasil.');  
      redirect(base_url('admin/user/editPasswordAct/'.$_SESSION['id']));
    
  }
    $this->template->back_views('site/back/editPassword',$data);
  }
  public function lupa_password()  
  {  
    $data['codepage'] = "back_login";
    $data['title']    = 'Lupa Password';  

   

    $this->load->library('form_validation');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');   
      
      if($this->form_validation->run() == FALSE) {  
          $this->template->back_views('site/back/v_lupa_password',$data); 
      }else{  
          $email    = $this->input->post('email');   
          $clean    = $this->security->xss_clean($email);  
          $userInfo = $this->user->getUserInfoByMailUser($clean); 
          
            
          if(!$userInfo){  
            
            redirect(base_url('login_admin'), 'refresh'); 
          }    
            
          //build token   
         
                    
          $token   = $this->user->insertTokenUser($userInfo->id);              
          $qstring = $this->base64url_encode($token);           
          $url     = site_url() . 'admin/user/reset_password/' . $qstring;  
          $subject = 'Reset Password';  
          
          $user = $this->user->getUserAdminByEmail($userInfo->email)->row_array();

          $content      = array  ( 
            'button'    => 'ResetPassword',
            'username'  => $user['username'],
            'fullname'  => $user['fullname'],
            'url'       => $url, 
            'subject'   => @$template['subject'],
            'opening'   => @$template['opening'],
            'content'   => @$template['content'],
            'closing'   => @$template['closing'],
            'detail'    => "",
            'time'      => "",
            'class'     => "",
            'rekening'  => "",
            'value'     => ""
            );
          $message  = '';
          $message .= $this->load->view('email/email-reset',$content, TRUE);             
          $this->user->mg_send($email,$subject,$message);
          $this->session->set_flashdata('gagal', 'Email address salah, silakan coba lagi.');
          $this->session->set_flashdata('sukses', 'Konfirmasi Reset Password sudah dikirim ke email anda.');
          redirect(base_url('login_admin'),'refresh');
      }  
      
  }  
  public function checkEmailAdmin()
  {
    $email         = $_POST['email'];
    $lupa_password = $this->user->checkEmail($email)->num_rows();
    
		header('COntent-type: application/json');
		echo json_encode($lupa_password);
  }

  public function reset_password()  
  {  
    $token      = $this->base64url_decode($this->uri->segment(4));           
    $cleanToken = $this->security->xss_clean($token);  
      
    $user_info  = $this->user->isTokenValidUser($cleanToken); //either false or array();          
      
    if(!$user_info){  
      $this->session->set_flashdata('Sukses', 'Token tidak valid atau kadaluarsa');  
      redirect(site_url('login_admin'),'refresh');   
    }    
    
    $data    = array(  
      'title'=> 'Reset Password',  
      'email'=>$user_info->email,   
      'token'=>$this->base64url_encode($token)  
    );  
    $data['codepage'] = "back_login";
    $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');  
    $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');         
      
    if ($this->form_validation->run() == FALSE) {    
      $this->template->back_views('site/back/v_reset_password',$data); 
    }else{  
                        
      $post                  = $this->input->post(NULL, TRUE);          
      $cleanPost             = $this->security->xss_clean($post);          
      $hashed                = hash_password($cleanPost['password']);          
      $cleanPost['password'] = $hashed;  
      $cleanPost['id']       = $user_info->id; 

      unset($cleanPost['passconf']);          
      if(!$this->user->updatePasswordUser($cleanPost)){  
        $this->session->set_flashdata('gagal', 'Update password gagal.');  
      }else{  
        $this->session->set_flashdata('sukses', 'Password anda sudah  
          diperbaharui. Silakan login.');  
      }  
      redirect(base_url('login_admin'), 'refresh');
         
    }  
  }  
    
public function base64url_encode($data) {   
 return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');   
}   

public function base64url_decode($data) {   
 return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));   
}    

  public function del_useradmin($id){
    $this->user->deluserAdmin($id);
    $this->user->deluserAdminDetail($id_useradmin);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/user/listUser"));
}
}

/* End of file User.php */
