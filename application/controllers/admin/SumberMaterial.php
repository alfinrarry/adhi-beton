<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class SumberMaterial extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_SumberMaterial','SumberMaterial');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List  Sumber Material
   public function listSumberMaterial(){
    $data['codepage']            = "back_addProduct";
    $data['page_title']   	     = 'List Sumber Material Halus';
    $data['userAdminRole']       = $this->user->getAllRole()->result_array();
    $data['sumberMaterial']      = $this->SumberMaterial->getSumberMaterial()->result_array();
    $id                          = $_SESSION['id'];
    $data['image']               = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/SumberMaterial/listSumberMaterial');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/sumberMaterialList',$data);
  }
    // End List  Sumber Material

 // Add Sumber Material
 public function inputSumberMaterial(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Sumber Material';
 {
    $data_sumber_material = array(
    
      'id_sumber_halus'          => $_POST['id_sumber_halus'],
      'tempat_sumber'            => $_POST['tempat_sumber']
    );
    $data = $this->SumberMaterial->inputSumberMaterial($data_sumber_material);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
  redirect(base_url("admin/SumberMaterial/listSumberMaterial"));

}
// End Add Sumber Material

    // Form Sumber Material
    public function formSumberMaterial(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']     = 'Form Add Sumber Material Halus';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $data['material']       = $this->SumberMaterial->getMaterial()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/SumberMaterial/formSumberMaterial');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/sumberMaterialForm',$data);
    }
    // End Form Sumber Material

      // Form Edit Sumber Material
      public function formEditSumberMaterial($id=0){
        $data['codepage']            = "back_addProduct";
        $data['page_title']        	 = 'Form Edit Sumber Material';
        $data['userAdminRole']       = $this->user->getAllRole()->result_array();
        $data['sumberMaterial']      = $this->SumberMaterial->getSumberMaterialById($id)->row_array();
        $id                          = $_SESSION['id'];
        $data['image']               = $this->user->getImage($id)->result_array();
        $data['material']            = $this->SumberMaterial->getMaterial()->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/SumberMaterial/formEditSumberMaterial');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/sumberMaterialEditForm',$data);
      }
      // End Form Edit SumberMaterial

       // Update Sumber Material
    public function updateSumberMaterial($id=0){
      $data['codepage']              = "back_addProduct";
      $data['page_title'] 	         = 'Update Quarry';
      $data['sumberMaterial']        = $this->SumberMaterial->getSumberMaterialById($id)->row_array();

     {
        $data_sumber_material = array(
        
            'tempat_sumber'     => $_POST['tempat_sumber']
        );
        $data = $this->SumberMaterial->updateSumberMaterial($id,$data_sumber_material);
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url("admin/SumberMaterial/listSumberMaterial"));

    }

        // End Update Sumber Material

  
//   Delete Data Sumber Material
  public function delSumberMaterial($id){
    $data= $this->SumberMaterial->delSumberMaterial($id);
           $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/SumberMaterial/listSumberMaterial"));
  }
//   Delete Data Sumber Material
}

/* End of file User.php */
