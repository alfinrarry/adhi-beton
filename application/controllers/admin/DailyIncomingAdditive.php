<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class DailyIncomingAdditive extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_DailyIncomingAdditive','DailyIncomingAdditive');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List  Daily Incoming Additive
   public function listDailyIncomingAdditive(){
    $data['codepage']               = "back_addProduct";
    $data['page_title']   	        = 'List Daily Incoming Additive';
    $data['userAdminRole']          = $this->user->getAllRole()->result_array();
    $data['dailyIncomingAdditive']  = $this->DailyIncomingAdditive->getDailyIncomingAdditive()->result_array();
    $id                             = $_SESSION['id'];
    $data['image']                  = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/DailyIncomingAdditive/listDailyIncomingAdditive');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/dailyIncomingAdditiveList',$data);
  }
    // End List  Daily Incoming Additive

 // Add Daily Incoming Additive
 public function inputDailyIncomingAdditive(){
  $data['codepage']               = "back_addProduct";
  $data['page_title'] 	          = 'Add Daily Incoming Additive';
  $data['dailyIncomingAdditive']  = $this->DailyIncomingAdditive->getDailyIncomingAdditive()->result_array();

    $config['upload_path']    ='./assets/img/content/DailyIncomingAdditive/';
    $config['allowed_types']  ='jpg|png|ico';
    $config['encrypt_name']   = TRUE;

    $this->load->library('upload',$config);
    if($this->upload->do_upload('doc_img'))
    {
        $img ='img/content/DailyIncomingAdditive/';
        $img.=  $this->upload->data('file_name');
    }
    $status='3';
 {
    $data_daily_incoming_additive = array(
      'merek_additive'             => $_POST['merek_additive'],
      'surat_jalan'                => $_POST['surat_jalan'],
      'id_supplier'                => $_POST['id_supplier'],
      'kriteria_millsheet_sg'      => $_POST['kriteria_millsheet_sg'],
      'kriteria_millsheet_ph'      => $_POST['kriteria_millsheet_ph'],
      'kriteria_millsheet_warna'   => $_POST['kriteria_millsheet_warna'],
      'id_pelaksana'               => $_POST['id_pelaksana'],
      'keterangan'                 => $_POST['keterangan'],
      'id_status'                  => @$status,
      'doc_img'                    => @$img 

    );
    $data = $this->DailyIncomingAdditive->inputDailyIncomingAdditive($data_daily_incoming_additive);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
  redirect(base_url("admin/DailyIncomingAdditive/listDailyIncomingAdditive"));

}
// End Add Daily Incoming Additive

    // Form Daily Incoming Additive
    public function formDailyIncomingAdditive(){
      $data['codepage']                 = "back_addProduct";
      $data['page_title']               = 'Form Add Daily Incoming Additive';
      $data['userAdminRole']            = $this->user->getAllRole()->result_array();
      $data['dailyIncomingAdditive']    = $this->DailyIncomingAdditive->getDailyIncomingAdditive()->result_array();
      $data['supplier']                 = $this->DailyIncomingAdditive->getsupplier()->result_array();
      $data['pelaksana']                = $this->DailyIncomingAdditive->getPelaksana()->result_array();
      $data['status']                   = $this->DailyIncomingAdditive->getStatus()->result_array();
      $id                               = $_SESSION['id'];
      $data['image']                    = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/DailyIncomingAdditive/formDailyIncomingAdditive');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/dailyIncomingAdditiveForm',$data);
    }
    // End Form Daily Incoming Additive

  
//   Delete Data Daily Incoming Additive
  public function delIncomingAdditive($id){
    $data= $this->DailyIncomingAdditive->delIncomingAdditive($id);
           $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/DailyIncomingAdditive/listDailyIncomingAdditive"));
  }
//   Delete Data Daily Incoming Additive

// Tolak Incoming Additive
public function tolakIncomingAdditive($id=0){
    $data['codepage']              = "back_addProduct";
    $data['page_title'] 	       = 'Tolak Incoming Additive';
    $data['dailyIncomingAdditive'] = $this->DailyIncomingAdditive->getDailyIncomingAdditiveById($id)->row_array();

    $status='2';
   {
      $data_incoming_additive = array(
      
          'id_status'                => @$status
      );

      $data = $this->DailyIncomingAdditive->tolakIncomingAdditive($id,$data_incoming_additive);
    }
    $this->session->set_flashdata('success_msg_register', 'Data berhasil ditolak !');  
    redirect(base_url("admin/DailyIncomingAdditive/listDailyIncomingAdditive"));

  }

// End Tolak Incoming Additive

// Setuju Incoming Additive
public function setujuIncomingAdditive($id=0){
    $data['codepage']              = "back_addProduct";
    $data['page_title'] 	         = 'Setuju Incoming Additive';
    $data['dailyIncomingAdditive'] = $this->DailyIncomingAdditive->getDailyIncomingAdditiveById($id)->row_array();

    $status='1';
   {
      $data_incoming_additive = array(
      
          'id_status'                => @$status
      );

      $data = $this->DailyIncomingAdditive->setujuIncomingAdditive($id,$data_incoming_additive);
    }
    $this->session->set_flashdata('success_msg_register', 'Data berhasil disetujui !');  
    redirect(base_url("admin/DailyIncomingAdditive/listDailyIncomingAdditive"));

  }

// End Setuju Incoming Additive

// Edit dan View Daily Incoming Additive
public function detailIncomingAdditive($id=0){
  $data['codepage']              = "back_useradmin";
  $data['page_title'] 	         = "Detail Daily Incoming Additive";
  $data['dailyIncomingAdditive'] = $this->DailyIncomingAdditive->getDailyIncomingAdditiveById($id)->row_array();
  $id                            = $_SESSION['id'];
  $data['image']                 = $this->user->getImage($id)->result_array();

  if ($_SESSION['id'] == true) { 
    $this->session->set_userdata($_SESSION); 
    base_url('admin/DailyIncomingAdditive/detailIncomingAdditive/');

  } else { 
    $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
    redirect(base_url('login_admin'));
  }
 
  $this->template->back_views('site/back/dailyIncomingAdditiveDetail',$data);
  
}

// End Edit dan View Daily Incoming Additive

// Udpate Daily Incoming Additive
public function updateIncomingAdditive($id=0){
  $data['codepage']               = "back_addProduct";
  $data['page_title'] 	          = 'Update Incoming Additive';
  $data['dailyIncomingAdditive']  = $this->DailyIncomingAdditive->getDailyIncomingAdditiveById($id)->row_array();
  


 {
  $config['upload_path']    ='./assets/img/content/DailyIncomingAdditive/';
  $config['allowed_types']  ='jpg|png|ico';
  $config['encrypt_name']   = TRUE;

  $this->load->library('upload',$config);
  if($this->upload->do_upload('doc_img'))
  {
      $img ='img/content/DailyIncomingAdditive/';
      $img.=  $this->upload->data('file_name');
  }

  $data_daily_incoming_additive = array(

    
    'surat_jalan'                => $_POST['surat_jalan'],
    'kriteria_millsheet_sg'      => $_POST['kriteria_millsheet_sg'],
    'kriteria_millsheet_ph'      => $_POST['kriteria_millsheet_ph'],
    'kriteria_millsheet_warna'   => $_POST['kriteria_millsheet_warna'],
    'keterangan'                 => $_POST['keterangan'],
    'doc_img'                    => @$img


  );
    $data = $this->DailyIncomingAdditive->updateIncomingAdditive($id,$data_daily_incoming_additive);
 }
    $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
    redirect(base_url("admin/DailyIncomingAdditive/listDailyIncomingAdditive"));  

}
// End Udpate Daily Incoming Kasar

}

/* End of file User.php */
