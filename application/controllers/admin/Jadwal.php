<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_jadwal','jadwal');
    $this->load->model('Mod_kelas','kelas');
    $this->load->model('Mod_ustadz','ustadz');
    $this->load->model('Mod_hari','hari');
    $this->load->model('Mod_pelajaran','pelajaran');
    $this->load->library('email');
    $this->load->library('session');
    
  }

  public function create_data_jadwal(){
    $data['codepage'] = "back_addProduct";
    $data['page_title'] 	= 'Add Jadwal';
    if(isset($_POST['submit'])){
     
  
      $data_jadwal = array(
        'id_pelajaran'   => $_POST['id_pelajaran'] ,
        'id_kelas'       => $_POST['id_kelas'] ,
        'id_ustadz'      => $_POST['id_ustadz'] ,
        'id_hari'        => $_POST['id_hari'] ,
        'created_at'     => date('Y-m-j ')
      );
      
      $data = $this->jadwal->create_jadwal($data_jadwal);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
      redirect(base_url('admin/jadwal/listJadwal'));
    }
    
  }

  public function formAddJadwal(){
    $data['codepage']       = "back_addProduct";
    $data['page_title'] 	  = 'Tambah Jadwal';
    $data['userAdminRole']  = $this->user->getAllRole()->result_array();
    $data['jadwal']         = $this->jadwal->getListPelajaran()->result_array();
    $data['kelas']          = $this->kelas->getListKelas()->result_array();
    $data['ustadz']         = $this->ustadz->getListUstadz()->result_array();
    $data['hari']           = $this->hari->getListHari()->result_array();
    $id                     = $_SESSION['id'];
    $data['image']          = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Jadwal/formAddJadwal/');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }

    $this->template->back_views('site/back/jadwalAdd',$data);

  }

 

  public function listJadwal(){
    $data['codepage'] = "back_user";
    $data['page_title'] 	= 'List Data Jadwal';
    $data['user']    = $this->user->getListUser()->result_array();
    $data['jadwal']         = $this->jadwal->getListJadwal()->result_array();
    $id = $_SESSION['id'];
    $data['image']    = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Jadwal/listJadwal/');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }
    
    
  
    
    
    
    $this->template->back_views('site/back/jadwalList',$data);
    
  }

 

  public function detailJadwal($id=0){
    $data['codepage']     = "back_useradmin";
    $data['page_title'] 	= "Detail Santri";
    $data['jadwal']       = $this->jadwal->getJadwalById($id)->row_array();
    $data['kelas']        = $this->kelas->getListKelas()->result_array();
    $data['ustadz']       = $this->ustadz->getListUstadz()->result_array();
    $data['hari']         = $this->hari->getListHari()->result_array();
    $data['pelajaran']    = $this->pelajaran->getListPelajaran()->result_array();
    $data['useradmin']    = $this->user->getUserAdminById($id)->row_array();
    $data['image']        = $this->user->getImage($id)->result_array();

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/Jadwal/detailJadwal/'.$_SESSION['id']);
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }
    
  
    if(isset($_POST['submit'])){
  
     
      
    
      $data_jadwal = array(
        'id_pelajaran'   => $_POST['id_pelajaran'] ,
        'id_kelas'       => $_POST['id_kelas'] ,
        'id_ustadz'      => $_POST['id_ustadz'] ,
        'id_hari'        => $_POST['id_hari'],
        'updated_at'     => date('Y-m-j ')
      );
      $data = $this->jadwal->updateDataJadwal($id,$data_jadwal);
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url('admin/jadwal/listJadwal'));
    
  }
  $this->template->back_views('site/back/jadwalEdit',$data);
    
  }
  public function del_jadwal($id=0){
    $this->jadwal->delJadwal($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/jadwal/listJadwal"));
}

public function pencarian(){
  $data['codepage']     = "back_useradmin";
  $data['page_title'] 	= "Hasil Pencarian Jadwal";
  $id                    = $_SESSION['id'];
  $data['image']         = $this->user->getImage($id)->result_array();

  if ($_SESSION['id'] == true) { 
    $this->session->set_userdata($_SESSION); 
    base_url('admin/Jadwal/listJadwal/');

  } else { 
    $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
    redirect(base_url('login_admin'));
  }
  

  $kelas=$this->input->get('kelas');
  $data['hasil'] = $this->jadwal->pencarian_jadwal($kelas)->result_array();
  $this->template->back_views('site/back/jadwalCari',$data);
// ini view menampilkan hasil pencarian
}

  
 
}

/* End of file User.php */
