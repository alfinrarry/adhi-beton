<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class DailyIncomingKasar extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_DailyIncomingKasar','DailyIncomingKasar');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List  Daily Incoming Kasar
   public function listDailyIncomingKasar(){
    $data['codepage']            = "back_addProduct";
    $data['page_title']   	     = 'List Daily Incoming Kasar';
    $data['userAdminRole']       = $this->user->getAllRole()->result_array();
    $data['dailyIncomingKasar']  = $this->DailyIncomingKasar->getDailyIncomingKasar()->result_array();
    $id                          = $_SESSION['id'];
    $data['image']               = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/DailyIncomingKasar/listDailyIncomingKasar');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/dailyIncomingKasarList',$data);
  }
    // End List  Daily Incoming Kasar

 // Add Daily Incoming Kasar
 public function inputDailyIncomingKasar(){
  $data['codepage']               = "back_addProduct";
  $data['page_title'] 	          = 'Add Daily Incoming Kasar';
  $data['dailyIncomingKasar']     = $this->DailyIncomingKasar->getDailyIncomingKasar()->result_array();

    $config['upload_path']    ='./assets/img/content/DailyIncomingKasar/';
    $config['allowed_types']  ='jpg|png|ico';
    $config['encrypt_name']   = TRUE;

    $this->load->library('upload',$config);
    if($this->upload->do_upload('doc_img'))
    {
        $img ='img/content/DailyIncomingKasar/';
        $img.=  $this->upload->data('file_name');
    }
    $status='3';
 {
    $data_daily_incoming_kasar = array(
    
      'id_material'                => $_POST['id_material'],
      'surat_jalan'                => $_POST['surat_jalan'],
      'id_vendor'                  => $_POST['id_vendor'],
      'id_quarry'                  => $_POST['id_quarry'],
      'kriteria_visual_bersih'     => $_POST['kriteria_visual_bersih'],
      'kriteria_visual_sedang'     => $_POST['kriteria_visual_sedang'],
      'kriteria_visual_kotor'      => $_POST['kriteria_visual_kotor'],
      'ukuran'                     => $_POST['ukuran'],
      'id_pelaksana'               => $_POST['id_pelaksana'],
      'keterangan'                 => $_POST['keterangan'],
      'id_status'                  => @$status,
      'doc_img'                    => @$img 

    );
    $data = $this->DailyIncomingKasar->inputDailyIncomingKasar($data_daily_incoming_kasar);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
  redirect(base_url("admin/DailyIncomingKasar/listDailyIncomingKasar"));

}
// End Add Daily Incoming Kasar

    // Form Daily Incoming Kasar
    public function formDailyIncomingKasar(){
      $data['codepage']                 = "back_addProduct";
      $data['page_title']               = 'Form Add Daily Incoming Kasar';
      $data['userAdminRole']            = $this->user->getAllRole()->result_array();
      $data['dailyIncomingKasar']       = $this->DailyIncomingKasar->getDailyIncomingKasar()->result_array();
      $data['material']                 = $this->DailyIncomingKasar->getMaterial()->result_array();
      $data['vendor']                   = $this->DailyIncomingKasar->getVendor()->result_array();
      $data['quarry']                   = $this->DailyIncomingKasar->getQuarry()->result_array();
      $data['pelaksana']                = $this->DailyIncomingKasar->getPelaksana()->result_array();
      $data['status']                   = $this->DailyIncomingKasar->getStatus()->result_array();
      $id                               = $_SESSION['id'];
      $data['image']                    = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/DailyIncomingKasar/formDailyIncomingKasar');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/dailyIncomingKasarForm',$data);
    }
    // End Form Daily Incoming Kasar

       // Update Sumber Material
    public function updateSumberMaterial($id=0){
      $data['codepage']              = "back_addProduct";
      $data['page_title'] 	         = 'Update Quarry';
      $data['sumberMaterial']        = $this->SumberMaterial->getSumberMaterialById($id)->row_array();

     {
        $data_sumber_material = array(
        
            'id_material'                => $_POST['id_material'],
            'tempat_sumber_material'     => $_POST['tempat_sumber_material']
        );
        $data = $this->SumberMaterial->updateSumberMaterial($id,$data_sumber_material);
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url("admin/SumberMaterial/listSumberMaterial"));

    }

        // End Update Sumber Material

  
//   Delete Data Daily Incoming Kasar
  public function delIncomingKasar($id){
    $data= $this->DailyIncomingKasar->delIncomingKasar($id);
           $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/DailyIncomingKasar/listDailyIncomingKasar"));
  }
//   Delete Data Daily Incoming Kasar

// Tolak Incoming Kasar
public function tolakIncomingKasar($id=0){
    $data['codepage']              = "back_addProduct";
    $data['page_title'] 	       = 'Tolak Incoming Kasar';
    $data['dailyIncomingKasar']    = $this->DailyIncomingKasar->getDailyIncomingKasarById($id)->row_array();

    $status='2';
   {
      $data_incoming_kasar = array(
      
          'id_status'                => @$status
      );

      $data = $this->DailyIncomingKasar->tolakIncomingKasar($id,$data_incoming_kasar);
    }
    $this->session->set_flashdata('success_msg_register', 'Data berhasil ditolak !');  
    redirect(base_url("admin/DailyIncomingKasar/listDailyIncomingKasar"));

  }

// End Tolak Incoming Kasar

// Setuju Incoming Kasar
public function setujuIncomingKasar($id=0){
    $data['codepage']              = "back_addProduct";
    $data['page_title'] 	       = 'Setuju Incoming Kasar';
    $data['dailyIncomingKasar']    = $this->DailyIncomingKasar->getDailyIncomingKasarById($id)->row_array();

    $status='1';
   {
      $data_incoming_kasar = array(
      
          'id_status'                => @$status
      );

      $data = $this->DailyIncomingKasar->setujuIncomingKasar($id,$data_incoming_kasar);
    }
    $this->session->set_flashdata('success_msg_register', 'Data berhasil disetujui !');  
    redirect(base_url("admin/DailyIncomingKasar/listDailyIncomingKasar"));

  }

// End Setuju Incoming Kasar

// Edit dan View Daily Incoming Kasar
public function detailIncomingKasar($id=0){
  $data['codepage']              = "back_useradmin";
  $data['page_title'] 	         = "Detail Daily Incoming Kasar";
  $data['dailyIncomingKasar']    = $this->DailyIncomingKasar->getDailyIncomingKasarById($id)->row_array();
  $data['material']              = $this->DailyIncomingKasar->getMaterial()->result_array();
  $data['bersih']                = $this->DailyIncomingKasar->getBersihById($id)->row_array();
  $id                            = $_SESSION['id'];
  $data['image']                 = $this->user->getImage($id)->result_array();

  if ($_SESSION['id'] == true) { 
    $this->session->set_userdata($_SESSION); 
    base_url('admin/DailyIncomingKasar/detailIncomingKasar/');

  } else { 
    $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
    redirect(base_url('login_admin'));
  }
 
  $this->template->back_views('site/back/dailyIncomingKasarDetail',$data);
  
}

// End Edit dan View Daily Incoming Kasar

// Udpate Daily Incoming Kasar
public function updateIncomingKasar($id=0){
  $data['codepage']            = "back_addProduct";
  $data['page_title'] 	       = 'Update Incoming Kasar';
  $data['dailyIncomingKasar']  = $this->DailyIncomingKasar->getDailyIncomingKasarById($id)->row_array();
  $data['material']            = $this->DailyIncomingKasar->getMaterialById($id)->row_array();
  $data['bersih']              = $this->DailyIncomingKasar->getBersihById($id)->row_array();
  


 {
  $config['upload_path']    ='./assets/img/content/DailyIncomingKasar/';
  $config['allowed_types']  ='jpg|png|ico';
  $config['encrypt_name']   = TRUE;

  $this->load->library('upload',$config);
  if($this->upload->do_upload('doc_img'))
  {
      $img ='img/content/DailyIncomingKasar/';
      $img.=  $this->upload->data('file_name');
  }

  $data_daily_incoming_kasar = array(

    
    'surat_jalan'                => $_POST['surat_jalan'],
    'kriteria_visual_bersih'     => $_POST['kriteria_visual_bersih'],
    'kriteria_visual_sedang'     => $_POST['kriteria_visual_sedang'],
    'kriteria_visual_kotor'      => $_POST['kriteria_visual_kotor'],
    'ukuran'                     => $_POST['ukuran'],
    'keterangan'                 => $_POST['keterangan'],
    'doc_img'                    => @$img


  );
    $data = $this->DailyIncomingKasar->updateIncomingKasar($id,$data_daily_incoming_kasar);
 }
    $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
    redirect(base_url("admin/DailyIncomingKasar/listDailyIncomingKasar"));  

}
// End Udpate Daily Incoming Kasar

}

/* End of file User.php */
