<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mutu extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_mutu','mutu');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List Project
   public function listMutu(){
    $data['codepage']       = "back_addProduct";
    $data['page_title']   	= 'List Mutu';
    $data['userAdminRole']  = $this->user->getAllRole()->result_array();
    $data['mutu']           = $this->mutu->getMutu()->result_array();
    $id                     = $_SESSION['id'];
    $data['image']          = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/Mutu/listMutu');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/mutuList',$data);
  }
    // End List Customer

 // Add Mutu
 public function inputMutu(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add Mutu';
 {
    $data_mutu = array(
    
      'name_mutu'   => $_POST['name_mutu']
    );
    $data = $this->mutu->inputMutu($data_mutu);
  }
  redirect(base_url("admin/Mutu/listMutu"));

}
// End Add Mutu

    // Form Mutu
    public function formMutu(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']     = 'Form Add Mutu';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/Mutu/formMutu');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/mutuForm',$data);
    }
    // End Form Mutu

      // Form Edit Mutu
      public function formEditMutu($id=0){
        $data['codepage']       = "back_addProduct";
        $data['page_title']   	= 'Form Edit Mutu';
        $data['userAdminRole']  = $this->user->getAllRole()->result_array();
        $data['mutu']           = $this->mutu->getMutuById($id)->row_array();
        $id                     = $_SESSION['id'];
        $data['image']          = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/Mutu/formEditMutu');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/mutuEditForm',$data);
      }
      // End Form Edit Mutu

       // Update Mutu
    public function updateMutu($id=0){
      $data['codepage']         = "back_addProduct";
      $data['page_title'] 	    = 'Update Mutu';
      $data['mutu']             = $this->mutu->getMutuById($id)->row_array();

     {
        $data_mutu = array(
        
          'name_mutu'       => $_POST['name_mutu']
        );
        $data = $this->mutu->updateMutu($id,$data_mutu);
      }
      redirect(base_url("admin/Mutu/listMutu"));

    }

        // End Update Mutu

  
//   Delete Data Mutu
  public function delMutu($id){
    $data= $this->mutu->delMutu($id);
    $this->mutu->delMutu($id);
    $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/Mutu/listMutu"));
  }
//   Delete Data Mutu

}

/* End of file User.php */
