<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class printPdf extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_santri','santri');
    $this->load->model('Mod_kelas','kelas');
    $this->load->model('Mod_nilai','nilai');
    $this->load->model('Mod_printPdf','print');
    $this->load->library('email');
    $this->load->library('session');
    $this->load->library('pdf');


    
  }

  public function printCariKelas(){
    $data['codepage']     = "back_useradmin";
    $data['page_title'] 	= 'Print Out Rekap Nilai';
    $data['santri']       = $this->santri->getListSantri()->result_array();
    $data['kelas']        = $this->santri->getListKelas()->result_array();
    $data['th']            = $this->nilai->getTahunAjaran()->result_array();
    $id = $_SESSION['id'];
    $data['image']        = $this->user->getImage($id)->result_array();
    
    
    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('admin/printPdf/printCariKelas/');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('login_admin'));
    }
  
    $this->template->back_views('site/back/printCari',$data);
    
  }


  

public function pencarian()
{
  $data['codepage']      = "back_useradmin";
  $data['page_title'] 	 = "Riwayat Terkahir Input Nilai Santri";
  $id                    = $_SESSION['id'];
  $data['image']         = $this->user->getImage($id)->result_array();
  $data['useradmin']     = $this->user->getUserAdminById($id)->row_array();
  $data['santri']        = $this->santri->getListSantri()->result_array();
  $data['kelas']         = $this->nilai->getKelasById($id)->row_array();
  $data['nilaiAll']      = $this->nilai->getListNilai()->result_array();

  if ($_SESSION['id'] == true) { 
    $this->session->set_userdata($_SESSION); 
    base_url('admin/printPdf/printCariKelas/');

  } else { 
    $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
    redirect(base_url('login_admin'));
  }


  
  $kelas                 = 18;
  $kelas                 = $this->input->get('kelas');
  $semester              = $this->input->get('semester');
  $tahun_ajaran          = $this->input->get('tahun_ajaran');
  $data['hasil']         = $this->print->printPdf($kelas,$semester,$tahun_ajaran)->result_array();    
  
  // Start Kelas 2 Ula
  if ($kelas <= 1)
  {
    $hasil    = $this->print->printPdf($kelas,$semester,$tahun_ajaran)->result_array();    
    $pdf      = new FPDF('l','mm','A4');
    // membuat halaman baru
    $pdf->AddPage();
    // setting jenis font yang akan digunakan
    $pdf->SetFont('Arial','B',16);
    // mencetak string 
    $pdf->Cell(280,7,"MADRASAH MATHOLI'UL HUDA",0,1,'C');
    $pdf->Cell(280,7,'PONDOK PESANTREN MIFTAHUL HUDA',0,1,'C');
    $pdf->SetFont('Arial','B',14);
    foreach (array_slice($hasil,0,1) as $row){
      $pdf->Cell(280,7,'REKAP NILAI KELAS '.$row['kelas'],0,1,'C');
    }
    // Memberikan space kebawah agar tidak terlalu rapat
    $pdf->Cell(0,7,'',0,1);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(27,6,'NIS',1,0);
    $pdf->Cell(40,6,'NAMA SANTRI',1,0);
    $pdf->Cell(27,6,'KELAS',1,0);
    $pdf->Cell(10,6,'SMT',1,0);
    $pdf->Cell(15,6,'TAUHID',1,0);
    $pdf->Cell(15,6,'FIQIH',1,0);
    $pdf->Cell(28,6,'MUKHOFADOH',1,0);
    $pdf->Cell(17,6,'Q.KITAB',1,0);
    $pdf->Cell(15,6,'TAJWID',1,0);
    $pdf->Cell(21,6,'AL-QURAN',1,0);
    $pdf->Cell(16,6,'IMLA',1,0);
    $pdf->Cell(20,6,'TARIKH',1,0);
    $pdf->Cell(19,6,'TAHUN',1,0);

    $pdf->SetFont('Arial','',10);
    foreach ($hasil as $row){
        $pdf->Cell(0,7,'',0,1);
        $pdf->Cell(27,6,$row['nis'],1,0);
        $pdf->Cell(40,6,$row['nama'],1,0);
        $pdf->Cell(27,6,$row['kelas'],1,0);
        $pdf->Cell(10,6,$row['semester'],1,0);
        $pdf->Cell(15,6,$row['n_tauhid'],1,0);
        $pdf->Cell(15,6,$row['n_fiqih'],1,0);
        $pdf->Cell(28,6,$row['n_mukhofadhoh'],1,0);
        $pdf->Cell(17,6,$row['n_qiroatul_kitab'],1,0);
        $pdf->Cell(15,6,$row['n_tajwid'],1,0);
        $pdf->Cell(21,6,$row['n_alquran'],1,0);
        $pdf->Cell(16,6,$row['n_imla'],1,0);
        $pdf->Cell(20,6,$row['n_tarikh'],1,0);
        $pdf->Cell(19,6,$row['tahun_ajaran'],1,0);


    }
    $pdf->Output(); 
   }
     // End Kelas 2 Ula

     // Start Kelas 3-4 Ula
  if ($kelas <= 8)
  {
    $hasil    = $this->print->printPdf($kelas,$semester,$tahun_ajaran)->result_array();    
    $pdf = new FPDF('l','mm','A4');
    // membuat halaman baru
    $pdf->AddPage();
    // setting jenis font yang akan digunakan
    $pdf->SetFont('Arial','B',16);
    // mencetak string 
    $pdf->Cell(280,7,"MADRASAH MATHOLI'UL HUDA",0,1,'C');
    $pdf->Cell(280,7,'PONDOK PESANTREN MIFTAHUL HUDA',0,1,'C');
    $pdf->SetFont('Arial','B',14);
    foreach (array_slice($hasil,0,1) as $row){
      $pdf->Cell(280,7,'REKAP NILAI KELAS '.$row['kelas'],0,1,'C');
    }
    // Memberikan space kebawah agar tidak terlalu rapat
    $pdf->Cell(0,7,'',0,1);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(27,6,'NIS',1,0);
    $pdf->Cell(40,6,'NAMA SANTRI',1,0);
    $pdf->Cell(27,6,'KELAS',1,0);
    $pdf->Cell(10,6,'SMT',1,0);
    $pdf->Cell(17,6,'TAUHID',1,0);
    $pdf->Cell(15,6,'FIQIH',1,0);
    $pdf->Cell(29,6,'MUKHOFADOH',1,0);
    $pdf->Cell(19,6,'Q.KITAB',1,0);
    $pdf->Cell(17,6,'TAJWID',1,0);
    $pdf->Cell(21,6,'AL-QURAN',1,0);
    $pdf->Cell(18,6,'SHOROF',1,0);
    $pdf->Cell(19,6,'TAHUN',1,0);

    $pdf->SetFont('Arial','',10);
    foreach ($hasil as $row){
        $pdf->Cell(0,7,'',0,1);
        $pdf->Cell(27,6,$row['nis'],1,0);
        $pdf->Cell(40,6,$row['nama'],1,0);
        $pdf->Cell(27,6,$row['kelas'],1,0);
        $pdf->Cell(10,6,$row['semester'],1,0);
        $pdf->Cell(17,6,$row['n_tauhid'],1,0);
        $pdf->Cell(15,6,$row['n_fiqih'],1,0);
        $pdf->Cell(29,6,$row['n_mukhofadhoh'],1,0);
        $pdf->Cell(19,6,$row['n_qiroatul_kitab'],1,0);
        $pdf->Cell(17,6,$row['n_tajwid'],1,0);
        $pdf->Cell(21,6,$row['n_alquran'],1,0);
        $pdf->Cell(18,6,$row['n_shorof'],1,0);
        $pdf->Cell(19,6,$row['tahun_ajaran'],1,0);


    }
    $pdf->Output(); 
   }
     // End Kelas 3-4 Ula

   //Start Kelas  Wustho
  elseif ($kelas <= 13)
  {
    $hasil    = $this->print->printPdf($kelas,$semester,$tahun_ajaran)->result_array();    

    $pdf = new FPDF('l','mm','A4');
    // membuat halaman baru
    $pdf->AddPage();
    // setting jenis font yang akan digunakan
    $pdf->SetFont('Arial','B',16);
    // mencetak string 
    $pdf->Cell(280,7,"MADRASAH MATHOLI'UL HUDA",0,1,'C');
    $pdf->Cell(280,7,'PONDOK PESANTREN MIFTAHUL HUDA',0,1,'C');
    $pdf->SetFont('Arial','B',14);
    foreach (array_slice($hasil,0,1) as $row){

    $pdf->Cell(280,7,'REKAP NILAI KELAS '.$row['kelas'],0,1,'C');
    }
    // Memberikan space kebawah agar tidak terlalu rapat
    $pdf->Cell(0,7,'',0,1);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(27,6,'NIS',1,0);
    $pdf->Cell(40,6,'NAMA SANTRI',1,0);
    $pdf->Cell(27,6,'KELAS',1,0);
    $pdf->Cell(10,6,'SMT',1,0);
    $pdf->Cell(15,6,'TAUHID',1,0);
    $pdf->Cell(12,6,'FIQIH',1,0);
    $pdf->Cell(27,6,'MUKHOFADOH',1,0);
    $pdf->Cell(17,6,'Q.KITAB',1,0);
    $pdf->Cell(15,6,'HADITS',1,0);
    $pdf->Cell(17,6,'B.ARAB',1,0);
    $pdf->Cell(16,6,'TAFSIR',1,0);
    $pdf->Cell(20,6,'FAROIDH',1,0);
    $pdf->Cell(20,6,'NAHWU',1,0);
    $pdf->Cell(18,6,'TAHUN',1,0);

    $pdf->SetFont('Arial','',10);
    foreach ($hasil as $row){
        $pdf->Cell(0,7,'',0,1);
        $pdf->Cell(27,6,$row['nis'],1,0);
        $pdf->Cell(40,6,$row['nama'],1,0);
        $pdf->Cell(27,6,$row['kelas'],1,0);
        $pdf->Cell(10,6,$row['semester'],1,0);
        $pdf->Cell(15,6,$row['n_tauhid'],1,0);
        $pdf->Cell(12,6,$row['n_fiqih'],1,0);
        $pdf->Cell(27,6,$row['n_mukhofadhoh'],1,0);
        $pdf->Cell(17,6,$row['n_qiroatul_kitab'],1,0);
        $pdf->Cell(15,6,$row['n_hadits'],1,0);
        $pdf->Cell(17,6,$row['n_b_arab'],1,0);
        $pdf->Cell(16,6,$row['n_tafsir'],1,0);
        $pdf->Cell(20,6,$row['n_faroidh'],1,0);
        $pdf->Cell(20,6,$row['n_nahwu'],1,0);
        $pdf->Cell(18,6,$row['tahun_ajaran'],1,0);


    }
    $pdf->Output(); 
   }
     // End Kelas  WUSTHO

  //Start Kelas Ulya
  elseif ($kelas >=14)
  {
    $hasil    = $this->print->printPdf($kelas,$semester,$tahun_ajaran)->result_array();    

    $pdf = new FPDF('l','mm','A4');
    // membuat halaman baru
    $pdf->AddPage();
    // setting jenis font yang akan digunakan
    $pdf->SetFont('Arial','B',16);
    // mencetak string 
    $pdf->Cell(280,7,"MADRASAH MATHOLI'UL HUDA",0,1,'C');
    $pdf->Cell(280,7,'PONDOK PESANTREN MIFTAHUL HUDA',0,1,'C');
    $pdf->SetFont('Arial','B',14);
    foreach (array_slice($hasil,0,1) as $row){

    $pdf->Cell(280,7,'REKAP NILAI KELAS '.$row['kelas'],0,1,'C');
    }
    // Memberikan space kebawah agar tidak terlalu rapat
    $pdf->Cell(0,7,'',0,1);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(27,6,'NIS',1,0);
    $pdf->Cell(40,6,'NAMA SANTRI',1,0);
    $pdf->Cell(27,6,'KELAS',1,0);
    $pdf->Cell(22,6,'SEMESTER',1,0);
    $pdf->Cell(20,6,'TAUHID',1,0);
    $pdf->Cell(17,6,'FIQIH',1,0);
    $pdf->Cell(29,6,'MUKHOFADOH',1,0);
    $pdf->Cell(20,6,'Q.KITAB',1,0);
    $pdf->Cell(17,6,'HADITS',1,0);
    $pdf->Cell(20,6,'NAHWU',1,0);
    $pdf->Cell(19,6,'TAHUN',1,0);

    $pdf->SetFont('Arial','',10);
    foreach ($hasil as $row){
        $pdf->Cell(0,7,'',0,1);
        $pdf->Cell(27,6,$row['nis'],1,0);
        $pdf->Cell(40,6,$row['nama'],1,0);
        $pdf->Cell(27,6,$row['kelas'],1,0);
        $pdf->Cell(22,6,$row['semester'],1,0);
        $pdf->Cell(20,6,$row['n_tauhid'],1,0);
        $pdf->Cell(17,6,$row['n_fiqih'],1,0);
        $pdf->Cell(29,6,$row['n_mukhofadhoh'],1,0);
        $pdf->Cell(20,6,$row['n_qiroatul_kitab'],1,0);
        $pdf->Cell(17,6,$row['n_hadits'],1,0);
        $pdf->Cell(20,6,$row['n_nahwu'],1,0);
        $pdf->Cell(19,6,$row['tahun_ajaran'],1,0);


    }
    $pdf->Output();  
   }
     //End Kelas Ulya
}


}

/* End of file User.php */
