<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ToBeUsed extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_ToBeUsed','ToBeUsed');
    $this->load->model('Mod_user','user');
    $this->load->library('email');
    $this->load->library('session');
    
  }

   // List  To Be Used
   public function listToBeUsed(){
    $data['codepage']            = "back_addProduct";
    $data['page_title']   	     = 'List To Be Used ';
    $data['userAdminRole']       = $this->user->getAllRole()->result_array();
    $data['toBeUsed']            = $this->ToBeUsed->getToBeUsed()->result_array();
    $id                          = $_SESSION['id'];
    $data['image']               = $this->user->getImage($id)->result_array();

      if ($_SESSION['id'] == true) { 
        $this->session->set_userdata($_SESSION); 
        base_url('admin/ToBeUsed/listToBeUsed');

      } else { 
        $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
        redirect(base_url('login_admin'));
      }

    $this->template->back_views('site/back/toBeUsedList',$data);
  }
    // End List  To Be Used

 // Add To Be Used
 public function inputToBeUsed(){
  $data['codepage']     = "back_addProduct";
  $data['page_title'] 	= 'Add To Be Used';
 {
    $data_to_be_used = array(
    
      'name_to_be_used'   => $_POST['name_to_be_used']
    );
    $data = $this->ToBeUsed->inputToBeUsed($data_to_be_used);
  }
  $this->session->set_flashdata('success_msg_register', 'Data berhasil ditambahkan !');  
  redirect(base_url("admin/ToBeUsed/listToBeUsed"));

}
// End Add To Be Used

    // Form To Be Used
    public function formToBeUsed(){
      $data['codepage']       = "back_addProduct";
      $data['page_title']     = 'Form Add To Be Used';
      $data['userAdminRole']  = $this->user->getAllRole()->result_array();
      $id                     = $_SESSION['id'];
      $data['image']          = $this->user->getImage($id)->result_array();
  
        if ($_SESSION['id'] == true) { 
          $this->session->set_userdata($_SESSION); 
          base_url('admin/ToBeUsed/formToBeUsed');
  
        } else { 
          $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
          redirect(base_url('login_admin'));
        }
  
      $this->template->back_views('site/back/toBeUsedForm',$data);
    }
    // End Form To Be Used

      // Form Edit To Be Used
      public function formEditToBeUsed($id=0){
        $data['codepage']            = "back_addProduct";
        $data['page_title']        	 = 'Form Edit To Be Used';
        $data['userAdminRole']       = $this->user->getAllRole()->result_array();
        $data['toBeUsed']            = $this->ToBeUsed->getToBeUsedById($id)->row_array();
        $id                          = $_SESSION['id'];
        $data['image']               = $this->user->getImage($id)->result_array();
          if ($_SESSION['id'] == true) { 
            $this->session->set_userdata($_SESSION); 
            base_url('admin/ToBeUsed/formEditToBeUsed');
    
          } else { 
            $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !');  
            redirect(base_url('login_admin'));
          }
    
        $this->template->back_views('site/back/toBeUsedEditForm',$data);
      }
      // End Form Edit To Be Used

       // Update To Be Used
    public function updateToBeUsed($id=0){
      $data['codepage']              = "back_addProduct";
      $data['page_title'] 	         = 'Update To Be Used';
      $data['toBeUsed']              = $this->ToBeUsed->getToBeUsedById($id)->row_array();

     {
        $data_to_be_used = array(
        
          'name_to_be_used'          => $_POST['name_to_be_used']
        );
        $data = $this->ToBeUsed->updateToBeUsed($id,$data_to_be_used);
      }
      $this->session->set_flashdata('success_msg_register', 'Data berhasil diperbarui !');  
      redirect(base_url("admin/ToBeUsed/listToBeUsed"));

    }

        // End Update To Be Used

  
//   Delete Data toBeUsed
  public function delToBeUsed($id){
    $data= $this->ToBeUsed->delToBeUsed($id);
           $this->session->set_flashdata('success_msg_register', 'Data berhasil dihapus !');  
    redirect(base_url("admin/ToBeUsed/listToBeUsed"));
  }
//   Delete Data toBeUsed
}

/* End of file User.php */
