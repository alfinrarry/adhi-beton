<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensi extends PIS_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_rekap_absensi','rekap');
    $this->load->library('email');
    $this->load->library('session');
  }
  public function index($id=0)
  {
    $data['codepage']       = "back_index";
    $id = $_SESSION['id']   ;
    $data['user']           = $this->user->getUser($id)->row_array();
    $data['absen']           = $this->user->getRekapAbsen($id)->result_array();
    $data['image']          = $this->user->getImageUser($id)->result_array();
    $data['th']             = $this->rekap->getTahunAjaran()->result_array();


    $this->template->front_views('site/front/absensi', $data);
  }

  public function pencarian()
  {
    $data['codepage']      = "back_useradmin";
    $data['page_title'] 	 = "Hasil Pencarian  Santri";
    $id                    = $_SESSION['id'];
    $data['user']           = $this->user->getUser($id)->row_array();
    $data['image']          = $this->user->getImageUser($id)->result_array();
    $data['th']            = $this->rekap->getTahunAjaran()->result_array();
    

    if ($_SESSION['id'] == true) { 
      $this->session->set_userdata($_SESSION); 
      base_url('Absensi/index/');
  
    } else { 
      $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
      redirect(base_url('auth'));
    }

    
    $tahun_ajaran  = $this->input->get('tahun_ajaran');
    $data['hasil'] = $this->rekap->cari_santri($tahun_ajaran)->result_array();

    $this->template->front_views('site/front/absensiCari',$data);


  }  
 
  
}

/* End of file Home.php */