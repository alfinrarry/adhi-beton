<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends PIS_Controller {
  
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_system','system');
    $this->load->library('session');

  }
  public function index()
  {
    $data['codepage'] = "back_login";
    if($this->user->logged_id()){ 
      base_url('auth'); 
    } else { 
      $this->form_validation->set_rules('nis', 'Nis', 'required'); 
      $this->form_validation->set_rules('password', 'Password', 'required'); //jika session belum terdaftar 
      if ($this->form_validation->run() == false) {
        base_url('auth'); 
      } else { 
        $data_user = array(
          'nis'       => $_POST['nis'],
          'password'  => $_POST['password']
        );
        $checking = $this->user->check_login($data_user); 
        if ($checking == true) { 
          foreach ($checking as $apps) {
              $session_data = array( 
               'id'         => $apps->id,
               'nis'        => $apps->nis,
               'email'      => $apps->email, 
               'fullname'   => $apps->fullname,
              ); 
              $this->session->set_userdata($session_data); 
              redirect(base_url('Home'));
          } 
        } else { 
          redirect(base_url('auth'));
        } 
      } 
    } 
    $this->template->back_views('site/front/login',$data);
  }

  public function editPasswordAct($id=0){
    $data['codepage']             = "back_index";
    $id                           = $_SESSION['id'];
    $data['user']                 = $this->user->getUser($id)->row_array();
    $data['image']                = $this->user->getImageUser($id)->result_array();
    
    
 

 
   
    if(isset($_POST['submit'])){

      $this->load->library('form_validation');
      $this->load->helper('form');
      
      $newPassword = $this->input->post('newPassword');
      $tryNewPassword = $this->input->post('tryNewPassword');

      $this->form_validation->set_message('matches', 'Password tidak cocok');                

      $this->form_validation->set_rules('newPassword','Password Baru','required|matches[tryNewPassword]');
      $this->form_validation->set_rules('tryNewPassword','Ulangi Password Baru','required');
      if($this->form_validation->run()!= false){
        $data = array('password' => hash_password($newPassword));
        
        $this->user->updateData($id,$data);
      }
      else{
        $this->session->set_flashdata('fail_msg_register', 'Update password gagal, silahkan ulangi lagi.');  
        redirect(base_url('User/editPasswordAct/'.$_SESSION['id']));


      }
      $this->session->set_flashdata('success_msg_register', 'Update password berhasil.');  
      redirect(base_url('home'));
    
  }

   
    
    $this->template->front_views('site/front/editPassword',$data);
  
  }
  

  public function logout(){
    $this->session->sess_destroy();    
    redirect(base_url('auth'));
  }
  public function userDashboard(){
    $data['codepage']		= "profile";
    if($this->user->logged_id()){ 
      $data['user']         = $this->user->getUser($_SESSION['id'])->row_array();
      $this->template->front_views('site/front/dashboard', $data);  
    }else{    
      redirect('Home');      
    }
  }
  public function updateProfile(){
    $data['codepage']		= "profile_update";
    // print_r(getUser($_SESSION['id'])['img_path']);die();
    if($this->user->logged_id()){ 
      $data['user']         = $this->user->getUser($_SESSION['id'])->row_array();
      if(isset($_POST['submit'])){
        $config['upload_path']   = './assets/img/content/user/';
        $config['allowed_types'] = 'gif|jpg|png|ico|svg';
        $this->load->library('upload',$config);   
    
        if($this->upload->do_upload('userfile')){            
            $path = '/user/';
            $path.= $this->upload->data('file_name');
            // print_r($path);die();
            $img = array(
              'img_path' => $path
            );
            $this->user->updateImg($img);
        }
        $data_user = array(
          'fullname'  => $_POST['fullname'],
          'phone'     => $_POST['phone'],
          'gender'    => $_POST['gender'],
          'line'      => $_POST['line'],
          'whatsapp'  => $_POST['whatsapp'],
          'telegram'  => $_POST['telegram'],
          'updated_at'=> date('Y-m-j H:i:s')
        );
        $this->user->updateData($data_user);
        $email = array(
          'email' => $_POST['email']
        );
        $this->user->updateEmail($email);
        $this->template->front_views('site/front/userProfile', $data); 
      }else{
        $this->template->front_views('site/front/userProfile', $data);  
      }
    }else{    
      redirect('Home');      
    }
  }
  public function forgotPwd(){
    $data['code_page']= 'forgotpwd';
    $data 	= array ("page" => "forgot");
    $this->load->library('form_validation');
    $this->load->library('email');
    $config['mailtype'] = 'html'; 
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');            
    if($this->form_validation->run() == FALSE) {  
      $this->template->front_views('site/front/login', $data); 
    }else{  
        $email = $this->input->post('email');  
        $clean = $this->security->xss_clean($email);  
        $userInfo = $this->user->getUserInfoByEmail($clean);
        //build token           
        $token = $this->user->insertToken($userInfo->email);
        $user = $this->user->getUserByEmail($userInfo->email)->row_array();           
        $qstring = $this->base64url_encode($token);
        // $template = $this->system->getEmailByid(2)->row_array();           
        $url = site_url() . 'home/reset_password/' . $qstring;
        $subject= 'Reset Password';//@$template['subject'];
        $content = array  ( 
          'button'    => 'ResetPassword',
          'nis'       => $user['nis'],
          'fullname'  => $user['fullname'],
          'url'       => $url, 
          'subject'   => @$template['subject'],
          'opening'   => @$template['opening'],
          'content'   => @$template['content'],
          'closing'   => @$template['closing'],
          'detail'    => "",
          'time'      => "",
          'class'     => "",
          'rekening'  => "",
          'value'     => ""
          );
        $message = ''; 
        $message .= $this->load->view('email/email-reset',$content, TRUE);  
        $this->user->mg_send($email, $subject, $message);
        $this->session->set_flashdata('success_msg_register', 'Konfirmasi Reset Password sudah dikirim ke email anda.');  
        redirect(base_url('auth'),'refresh');
    }
  }
  public function checkEmail()
  {
    $email =$_POST['email'];
		$forgotpwd = $this->user->checkEmail($email)->num_rows();
		header('COntent-type: application/json');
		echo json_encode($forgotpwd);
  }
  
}

/* End of file User.php */
