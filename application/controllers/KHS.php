<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class KHS extends PIS_Controller {
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Mod_user','user');
    $this->load->model('Mod_rekap_nilai','rekap');
    $this->load->model('Mod_nilai','nilai');
    $this->load->library('email');
    $this->load->library('session');
  }
  public function index($id=0)
  {
    $data['codepage']       = "back_index";
    $id = $_SESSION['id']   ;
    $data['user']           = $this->user->getUser($id)->row_array();
    $data['nilai']          = $this->user->getUser($id)->row_array();
    $data['image']          = $this->user->getImageUser($id)->result_array();
    $data['th']             = $this->rekap->getTahunAjaran()->result_array();


    $this->template->front_views('site/front/khsCari', $data);
  }

  public function pencarian()
  {

 
    $data['codepage']       = "back_index";
    $id = $_SESSION['id']   ;
    $data['user']           = $this->user->getUser($id)->row_array();
    $data['nilai']          = $this->user->getUser($id)->row_array();
    $data['image']          = $this->user->getImageUser($id)->result_array();
    $data['th']            = $this->nilai->getTahunAjaran()->result_array();
    $data['kelas']         = $this->nilai->getKelasById($id)->row_array();
  
  if ($_SESSION['id'] == true) { 
    $this->session->set_userdata($_SESSION); 
    base_url('KHS/index');

  } else { 
    $this->session->set_flashdata('fail_msg_register', 'Silahkan login kembali !'); 
    redirect(base_url('auth'));
  }

  $kelas                 = 18;
  $kelas                 = $this->input->get('kelas');
 
  $data['hasil']         = $this->rekap->cari_rekap($kelas)->result_array();       
          
  if ($kelas <= 1 )
  {        
    $this->template->front_views('site/front/khs', $data);
  }
  elseif ($kelas <=8)
  {
    $this->template->front_views('site/front/khsUla', $data);
  }
  elseif ($kelas <=13)
  {
    $this->template->front_views('site/front/khsWustho', $data);
  }
  elseif ($kelas >=14)
  {
    $this->template->front_views('site/front/khsUlya', $data);
  }

  }
 
  
}

/* End of file Home.php */