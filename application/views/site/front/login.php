<!-- Page title -->

<!-- End Page title -->
<!-- Content -->
<?php if ($codepage != 'reset'):?>
  <div class="auth-wrapper bg-dark d-flex no-block justify-content-center align-items-center"
	style="background:url(<?php echo vendor_url('back/images/big/islami.jpg'); ?>) no-repeat center center;">
	<div class="auth-box">
		<div id="loginform">
			
<section>
  <div class="container" >
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <?php if(!empty($_SESSION['success_msg_register'])):?>
          <div class="alert alert-success" role="alert">
            <?php echo $_SESSION['success_msg_register']?>
          </div>
        <?php elseif(!empty($_SESSION['fail_msg_register'])):?>
        <div class="alert alert-danger" role="alert">
          <?php echo $_SESSION['fail_msg_register']?>
        </div>
        <?php endif;?>
       
        
			<div class="logo">
				<span class="db"><img src="<?php echo vendor_url('back/images/logo-icon.png'); ?>" alt="logo" /></span>
				<h5 class="font-big m-b-22">Login Santri</h5>
			</div>
        <nav>
          <div class="nav nav-tabs mb-3" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-login-tab" data-toggle="tab" href="#nav-login" role="tab"
              aria-controls="nav-login"
              aria-selected="true">Masuk</a>
            
            <a class="nav-item nav-link" id="nav-forgotpassword-tab" data-toggle="tab" href="#nav-forgotpassword" role="tab"
              aria-controls="nav-forgotpassword"
              aria-selected="false">Lupa Password</a>
          </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
          <div class="tab-pane fade show active" id="nav-login" role="tabpanel" aria-labelledby="nav-login-tab">
            <form class="form-horizontal m-t-20"  action="<?php echo base_url('auth'); ?>" method="POST">
            <div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text" id="basic-addon1"><i class="ti-user"></i></span>
							</div>
							<input type="text" name="nis" class="form-control form-control-lg" placeholder="Nomor Induk " aria-label="Nis"
								aria-describedby="basic-addon1">
						</div>
            <div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text" id="basic-addon2"><i class="ti-pencil"></i></span>
							</div>
							<input type="password" name="password" class="form-control form-control-lg" placeholder="Password" aria-label="Password"
								aria-describedby="basic-addon1">
						</div>
            <div class="form-group text-center">
							<div class="col-xs-12 p-b-20">
             
								<button class="btn btn-block bg-primary btn-lg btn-info" type="submit">Login</button>
							</div>
						</div>
            <div class="form-group m-b-0 m-t-10">
							<div class="col-sm-12 text-center">
								Kamu seorang admin? <a href="<?= base_url('login_admin')?>" class="text-info m-l-5"><b>Login </b></a>
							</div>
						</div>
            </form>
          </div>
        
          <div class="tab-pane fade" id="nav-forgotpassword" role="tabpanel" aria-labelledby="nav-forgotpassword-tab">
            <form action="<?= base_url('forgotpwd')?>" method="POST">
              <div class="form-group userEmail">
                <label for="forgotemail">Email</label>
                <input id="userEmail" type="email" class="form-control" name="email" placeholder="Email">
                <br>
                <div class="alert alert-danger" id="format-pwd-invalid"  style="display:none;" for="forgot">Format email salah, cek kembali penulisan email</div>
                <div class="alert alert-danger" id="forgot-pwd-invalid"  style="display:none;" class="errorreg " for="forgot">Email Tidak Tersedia</div>
                <div class="alert alert-info" id="forgot-valid"  style="display:none;" class="success" for="forgot">Email Tersedia</div>
              </div>
              <button type="submit" name="submit" class="btn btn-primary w-100">Kirim</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php else :?>
<section class="mt-4">
  <div class="container">
    <div class="row">
      <div class="col-md-8 offset-md-2">
        <?php if(!empty($_SESSION['success_msg_register'])):?>
          <div class="alert alert-success" role="alert">
            <?php echo $_SESSION['success_msg_register']?>
          </div>
        <?php elseif(!empty($_SESSION['fail_msg_register'])):?>
        <div class="alert alert-danger" role="alert">
          <?php echo $_SESSION['fail_msg_register']?>
        </div>
        <?php endif;?>
        <nav>
          <div class="nav nav-tabs mb-3" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-login-tab" data-toggle="tab" href="#nav-login" role="tab"
              aria-controls="nav-login"
              aria-selected="true"></a>
          </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
          <div class="tab-pane fade show active" id="nav-login" role="tabpanel" aria-labelledby="nav-login-tab">
            <?php echo form_open('Home/reset_password/'.$token); ?>
              <div class="form-group">
                <label for="registerPassword">Password Baru</label>
                <input class="form-control" type="password" name="password" value="<?php echo set_value('password'); ?>" required/>
                <?php echo form_error('password'); ?>
              </div>
              <div class="form-group">
                <label for="registerPassword">Konfirmasi Password</label>
                <input type="password" class="form-control" name="passconf" value="<?php echo set_value('passconf'); ?>" />
                <?php echo form_error('passconf'); ?>
              </div>
              <button type="submit" name="submit" class="btn btn-primary w-100">Perbarui</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endif;?>
<!-- End Content -->