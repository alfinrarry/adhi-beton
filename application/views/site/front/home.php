<div class="container-fluid" data-codepage="<?= $codepage?>">

    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body text-center">
                <?php if(!empty($_SESSION['success_msg_register'])):?>
                <div class="alert alert-success" role="alert">
                    <?php echo $_SESSION['success_msg_register']?>
                </div>
                    <?php elseif(!empty($_SESSION['fail_msg_register'])):?>
                <div class="alert alert-danger" role="alert">
                    <?php echo $_SESSION['fail_msg_register']?>
                </div>
                    <?php endif;?>
                    <div class="profile-pic m-b-20 m-t-20">
                        <img src="<?= img_url(getUser($_SESSION['id'])['img_path'])?>" width="150" class="rounded-circle" alt="user">
                        <h4 class="m-t-20 m-b-0"><?= ucwords(getUser($_SESSION['id'])['nama'])?></h4>
                        <h4 class="m-t-20 m-b-0"><?= $user['nama_komplek']?></h4>
                        <h4 class="m-t-20 m-b-0"><?= $user['kelas']?></h4>
                    </div>
                </div>
              
        <!-- Column -->
		</div>
		</div>
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link text-dark active show" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab"
                            aria-controls="pills-profile" aria-selected="false">Identitas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark " id="pills-profile-alamat" data-toggle="pill" href="#last-alamat" role="tab"
                            aria-controls="pills-alamat" aria-selected="false">Alamat</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link text-dark " id="pills-profile-kontak" data-toggle="pill" href="#last-kontak" role="tab"
                            aria-controls="pills-kontak" aria-selected="false">Kontak</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link text-dark" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                            role="tab" aria-controls="pills-setting" aria-selected="true">Edit</a>
                    </li>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">NIS</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?= $user['nis']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="ename" class="col-sm-3 text-right control-label col-form-label">Nama</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Nama Lengkap" value="<?= $user['nama']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="rate" class="col-sm-3 text-right control-label col-form-label">Email</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Email" value="<?= $user['email']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="stime" class="col-sm-3 text-right control-label col-form-label">Tempat Lahir</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Telegram" value="<?= $user['tempat_lahir']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Tanggal Lahir</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Whatsapp" value="<?= $user['tgl_lahir']?>">
                                        </div>
                                    </div>
                                
                                    
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Instansi</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Instansi" value="<?= $user['nama_institusi']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Jurusan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Instansi" value="<?= $user['jurusan']?>">
                                        </div>
                                    </div>

								

                                    <div class="form-group row">
                                        <label for="note1" class="col-sm-3 text-right control-label col-form-label">Terdaftar</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Nama Lengkap" value="<?= tgl_indo($user['tgl_masuk'])?>">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="tab-pane fade " id="last-alamat" role="tab"
                        aria-labelledby="pills-alamat-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                   

									<div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Alamat</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Alamat" value="<?= $user['alamat']?>">
                                        </div>
                                    </div>

									<div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Kelurahan/Kecamatan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="-" value="<?= $user['kecamatan']?>">
                                        </div>
                                    </div>

									<div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Kota/Kabupaten</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Kota" value="<?= $user['kota']?>">
                                        </div>
                                    </div>

									<div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Provinsi</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Provinsi" value="<?= $user['provinsi']?>">
                                        </div>
                                    </div>

									<div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Kode Pos</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Kode Pos" value="<?= $user['kode_pos']?>">
                                        </div>
                                    </div>

                                    
                                </div>
                            </form><br><br><br><br><br><br><br>
                        </div>
                    </div>

                    <div class="tab-pane fade " id="last-kontak" role="tab"
                        aria-labelledby="pills-kontak-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                   

                                <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Whatsapp</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $user['no_hp']?>">
                                        </div>
                                    </div>

									<div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Nomor Orang Tua</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $user['tlp_rmh']?>">
                                        </div>
                                    </div>

                                    
                                </div>
                            </form><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                        </div>
                    </div>
                   
                    <div class="tab-pane fade " id="previous-month" role="tab"
                        aria-labelledby="pills-setting-tab">
                        <div class="card-body">
                        <?= form_open_multipart('Home/index/'.$user['id']);  ?>
                           
                        
                               
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <input type="email" placeholder="emailedit" class="form-control form-control-line"  value="<?= $user['email']?>" name="emailedit">
                                    </div>
                                </div>
                               
                                

                                <div class="form-group">
                                        <label for="img" class="col-sm-12">Image</label>
										<div class="col-sm-12">
                                        <input type="file" id="img_path"   data-height="200" value="<?= img_url($user['img_path']); ?>" name="img_path" />
                                        </div>
                                    </div>
                               
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="submit"  class="btn btn-success" >Perbarui Profil</button>
                                    </div>
                                </div>
                        </div>
                        </form>  
<br><br><br><br><br><br><br><br><br><br><br><br><br>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
