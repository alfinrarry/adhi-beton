  
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<div class="container" data-codepage="<?= $codepage?>">
  <div class="auth-wrapper d-flex no-block justify-content-center align-items-center">
    <div class="col-sm-12 col-md-4">
 
          <?php echo form_open('home/reset_password/'.$token); ?>
            <div class="form-group">
                <label for="registerPassword">Password Baru</label>
                <input class="form-control" type="password" name="password" value="<?php echo set_value('password'); ?>" required/>
                <?php echo form_error('password'); ?>
            </div>
            <div class="form-group">
                <label for="registerPassword">Konfirmasi Password</label>
                <input type="password" class="form-control" name="passconf" value="<?php echo set_value('passconf'); ?>" />
                <?php echo form_error('passconf'); ?>
            </div>
              <button type="submit" name="btnSubmit" value="reset" class="btn btn-danger w-100">Masuk</button>
          </form>
  
    </div>
  </div>
 </div>

 