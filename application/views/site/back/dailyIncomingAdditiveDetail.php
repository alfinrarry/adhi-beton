<div class="container-fluid" data-codepage="<?php echo $codepage ?>">

    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-20 m-t-20">
                        <img src="<?= img_url($dailyIncomingAdditive['doc_img'])?>" width="150" class="rounded-circle" alt="user">
                        <h4 class="m-t-20 m-b-0">Dokumentasi</h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  active show" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab"
                            aria-controls="pills-profile" aria-selected="false">View</a>
                    </li>
                    <?php if ($dailyIncomingAdditive['id_daily_incoming_additive']):?>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                            role="tab" aria-controls="pills-setting" aria-selected="true">Edit</a>
                    </li>
                    <?php endif;?>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Merek Additive</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?= $dailyIncomingAdditive['merek_additive']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="ename" class="col-sm-3 text-right control-label col-form-label">Surat Jalan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Nama Lengkap" id="fullname" name="fullname" value="<?= $dailyIncomingAdditive['surat_jalan']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="rate" class="col-sm-3 text-right control-label col-form-label">Supplier</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Email" value="<?= $dailyIncomingAdditive['supplier_name']?>">
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">SG</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingAdditive['kriteria_millsheet_sg']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">PH</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingAdditive['kriteria_millsheet_ph']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Warna</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingAdditive['kriteria_millsheet_warna']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Pelaksana</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingAdditive['name_pelaksana']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Keterangan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingAdditive['keterangan']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Status</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingAdditive['status']?>">
                                        </div>
                                    </div>
                                    
                                    
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                   
                    <div class="tab-pane fade " id="previous-month" role="tab"
                        aria-labelledby="pills-setting-tab">
                        <div class="card-body">
                        <?= form_open_multipart('admin/DailyIncomingAdditive/updateIncomingAdditive/'.$dailyIncomingAdditive['id_daily_incoming_additive']);  ?>
                           
                        
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Merek Additive</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="surat_jalan" disabled name="merek_additive" class="form-control form-control-line"  value="<?= $dailyIncomingAdditive['merek_additive']?>">                                
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Surat Jalan</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="surat_jalan" name="surat_jalan" class="form-control form-control-line"  value="<?= $dailyIncomingAdditive['surat_jalan']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Supplier</label>
                                    <div class="col-md-12">
                                        <input type="text" name="id_supplier"  class="form-control form-control-line" disabled  value="<?= $dailyIncomingAdditive['supplier_name']?>">
                                    </div>
                                </div>
                              
                                <div class="form-group">
                                    <label class="col-md-12">SG</label>
                                    <div class="col-md-12">
                                    <input type="text" name="kriteria_millsheet_sg"  class="form-control form-control-line"  value="<?= $dailyIncomingAdditive['kriteria_millsheet_sg']?>">                                
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">PH</label>
                                    <div class="col-md-12">
                                    <input type="text" name="kriteria_millsheet_ph"  class="form-control form-control-line"  value="<?= $dailyIncomingAdditive['kriteria_millsheet_ph']?>"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Warna</label>
                                    <div class="col-md-12">
                                    <input type="text" name="kriteria_millsheet_warna"  class="form-control form-control-line"  value="<?= $dailyIncomingAdditive['kriteria_millsheet_warna']?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-12">Pelaksana</label>
                                    <div class="col-md-12">
                                        <input type="text" name="id_pelaksana"  class="form-control form-control-line" disabled  value="<?= $dailyIncomingAdditive['name_pelaksana']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Keterangan</label>
                                    <div class="col-md-12">
                                        <input type="text" name="keterangan"  class="form-control form-control-line"  value="<?= $dailyIncomingAdditive['keterangan']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Status</label>
                                    <div class="col-md-12">
                                        <input type="text" name="id_status"  class="form-control form-control-line" disabled  value="<?= $dailyIncomingAdditive['status']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label for="img" class="col-sm-12">Image</label>
                                        <div class="col-sm-9">
                                        <input type="file" id="doc_img"   data-height="200" value="<?= img_url($dailyIncomingAdditive['doc_img']); ?>" name="doc_img" />
                                        </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="submit"  class="btn btn-success" >Update Profile</button>
                                    </div>
                                </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
