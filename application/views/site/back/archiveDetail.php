<div class="container-fluid" data-codepage="<?php echo $codepage ?>">

    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">

            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-5 m-t-5">
                        <img src="<?= img_url($archive['sertif_img'])?>" width="150"  alt="user">
                        <h4 class="m-t-20 m-b-0">Sertifikat </h4>
                    </div>
                </div>
            </div>

        </div>
        
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  active show" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab"
                            aria-controls="pills-profile" aria-selected="false">View</a>
                    </li>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                   
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Jenis Test</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?= $archive['jenis_test']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Nama Test</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?= $archive['nama_test']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Sample Source</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?= $archive['sample_source_name']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Tested By</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $archive['name_pelaksana']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="rate" class="col-sm-3 text-right control-label col-form-label">Received Date</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Email" value="<?= $archive['received_date']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Sertifikat</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" 
                                            value="<?php $sertif = $archive['sertif_img'];
                                                       $tampil=substr($sertif,23,50);
                                                       echo $tampil; ?>">
                                        </div>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                   
                    
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
