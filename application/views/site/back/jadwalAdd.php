<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
<?php $isEdit = $page_title == "Perbarui Produk"? true: false; ?>
		<div class="row">
		<div class="col-12 card">
			<form enctype="multipart/form-data" id="add_jadwal" method="post" action="<?php echo base_url('admin/jadwal/create_data_jadwal/')?>"  data-dir="" data-url="">
            </div>

		<div class="col-sm-12 col-md-8">
                <div class="form-group">
				<label for="hari" class="control-label col-form-label">Pelajaran<span
                      class="text-danger">*</span></label>
					 
				  <select name="id_hari" class="form-control"   id="id_hari" required <?php  echo "value='".$hari['id_hari']."'"?>>
				  <?php foreach($hari as $r):?>
                  		<option value="<?php echo $r['id_hari'] ?>"><?php echo $r['nama_hari'] ?></option>
						  <?php endforeach ?>
				  </select>
				</div>
        </div>
			
        <div class="col-sm-12 col-md-8">
                <div class="form-group">
                  <label for="pelajaran" class="control-label col-form-label">Pelajaran<span
                      class="text-danger">*</span></label>
					 
				  <select name="id_pelajaran" class="form-control"   id="id_pelajaran" required <?php  echo "value='".$jadwal['id_pelajaran']."'"?>>
				  <?php foreach($jadwal as $r):?>
                  		<option value="<?php echo $r['id_pelajaran'] ?>"><?php echo $r['nama_pelajaran'] ?></option>
						  <?php endforeach ?>
				  </select>
				</div>
        </div>

        <div class="col-sm-12 col-md-8">
                <div class="form-group">
                  <label for="jabatan" class="control-label col-form-label">Kelas<span
                      class="text-danger">*</span></label>
					 
				  <select name="id_kelas" class="form-control"   id="id_kelas" required <?php  echo "value='".$kelas['id_kelas']."'"?>>
				  <?php foreach($kelas as $r):?>
                  		<option value="<?php echo $r['id_kelas'] ?>"><?php echo $r['kelas'] ?></option>
						  <?php endforeach ?>
				  </select>
				</div>
        </div>

        <div class="col-sm-12 col-md-8">
                <div class="form-group">
                  <label for="jabatan" class="control-label col-form-label">Ustadz<span
                      class="text-danger">*</span></label>
					 
				  <select name="id_ustadz" class="form-control"   id="id_ustadz" required <?php  echo "value='".$ustadz['id_ustadz']."'"?>>
				  <?php foreach($ustadz as $r):?>
                  		<option value="<?php echo $r['id_ustadz'] ?>"><?php echo $r['nama_ustadz'] ?></option>
						  <?php endforeach ?>
				  </select>
				 </div>
        </div>

       
			
	</div>
                  
		<div class="form-group text-right">
						<button class="btn btn-danger btn-sm waves-effect waves-light" type="submit" name="submit"><span
								class="btn-label"><i class="fas fa-save"></i></span> Simpan</button>
		</div>
				</form>
			</div>
      