<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Column rendering -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<?php if(!empty($_SESSION['success_msg_register'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg_register']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>

		<div class="form-group pull-right "><br>
            <div class="row">
				<div class="form-group col-sm-3">
                	<a href="<?php echo base_url('admin/InternalTest/formAgregatHalus')?>" type="submit" class="btn btn-md btn-primary">Tambah Data</a>
				</div>
          	</div>
        </div>

					<div class="table-responsive">
						<table id="listProduct" class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th></th>
									<th>Sample Source</th>
									<th>Received Date</th>
									<th>Tested By</th>
									<th>FM</th>
									<th>SG</th>
									<th>AB</th>
									<th>OI</th>
									<th>MP</th>
									<th>UW</th>
									<th>CL</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							
                  		
						 
              <?php
              $no = 1;
              ?>
			   <!-- <?php foreach($internalHalus as $r):?> -->
                <tr>
                    <td><?= $no++?></td>
                    <td><?php echo $r['sample_source_name'] ?></td>
                    <td><?php echo $r['received_date'] ?></td>
                    <td><?php echo $r['name_pelaksana'] ?></td>
                    <td><?php echo $r['satu_input_fm'] ?></td>
                    <td><?php echo $r['dua_input_satu'] ?></td>
                    <td><?php echo $r['dua_input_dua'] ?></td>
                    <td><?php echo $r['tiga_input_satu'] ?></td>
                    <td><?php echo $r['empat_input_satu'] ?></td>
                    <td><?php echo $r['lima_input_satu'] ?></td>
					<td><?php echo $r['enam_input_satu'] ?></td>
					<td><?php echo $r['status'] ?></td>
                    <td>
					<a href="<?php echo base_url('admin/InternalTest/detailInternalHalus/'.$r['id_internal_tes_halus'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/InternalTest/detailInternalHalus/'.$r['id_internal_tes_halus'])?>"><i class="fas fa-pencil-alt"></i></button>
                    <a href="<?php echo base_url('admin/InternalTest/delInternalHalus/'.$r['id_internal_tes_halus'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/InternalTest/delInternalHalus/'.$r['id_internal_tes_halus'])?>"><i class="fas fa-trash"></i></button><br><br>
					<a href="<?php echo base_url('admin/InternalTest/tolakInternalHalus/'.$r['id_internal_tes_halus'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/InternalTest/tolakInternalHalus/'.$r['id_internal_tes_halus'])?>"><i class="fas fa-false">TOLAK</i></button><br><br>
					<a href="<?php echo base_url('admin/InternalTest/setujuInternalHalus/'.$r['id_internal_tes_halus'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/InternalTest/setujuInternalHalus/'.$r['id_internal_tes_halus'])?>"><i class="fas fa-pencil">SETUJU</i></button>
					</td>
                </tr>      
				<!-- <?php endforeach ?> -->

      


				    
       
              <?php
              $no++; 
             ?>
								</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>


<!-- ============================================================== -->
<!-- End Container fluid  -->