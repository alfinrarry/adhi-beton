<div class="container-fluid" data-codepage="<?= $codepage?>">

    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">

            <?php if(!empty($_SESSION['success_msg_register'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg_register']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
			<?php endif;?>

                <div class="card-body text-center">
                    <div class="profile-pic m-b-20 m-t-20">
                        <img src="<?= img_url($img['img_path'])?>" width="150" class="rounded-circle" >
                        <h4 class="m-t-20 m-b-0"><?= $santri['nis']?></h4>
                        <h3 class="m-t-20 m-b-0"><?= $santri['nama']?></h3>
                        <h4 class="m-t-20 m-b-0"><?= $santri['kelas']?></h4>

                    </div>
                </div>
                
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  active show" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab"
                            aria-controls="pills-profile" aria-selected="false">Profil</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                            role="tab" aria-controls="pills-setting" aria-selected="true">Edit</a>
                    </li>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Nama Lengkap</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?= $santri['nama']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="ename" class="col-sm-3 text-right control-label col-form-label">Nama Panggilan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Nama Panggilan" id="nama_panggilan" name="nama_panggilan" value="<?= $santri['nama_panggilan']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="rate" class="col-sm-3 text-right control-label col-form-label">Tempat, Tangal Lahir</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Tempat Lahir" value="<?= $santri['tempat_lahir']?>, <?= tgl_indo($santri['tgl_lahir'])?>">
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Nomor Hp</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Nomor Hp" value="<?= $santri['no_hp']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Instansi</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Instansi" value="<?= $santri['nama_institusi']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Jurusan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Jurusan" value="<?= $santri['jurusan']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Nama Ayah</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Nama Ayah" value="<?= $santri['ayah']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Nama Ibu</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Nama Ibu" value="<?= $santri['ibu']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Nama Wali</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Wali" value="<?= $santri['wali']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Telepon Rumah</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Telepon Rumah" value="<?= $santri['tlp_rmh']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Alamat</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Alamat " value="<?= $santri['alamat']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Kecamatan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Kecamatan " value="<?= $santri['kecamatan']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Kota/Kabupaten</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Kota " value="<?= $santri['kota']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Provinsi</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Provinsi " value="<?= $santri['provinsi']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Kode Pos</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Kode Pos" value="<?= $santri['kode_pos']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Komplek/Kamar</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Komplek " value="<?= $santri['nama_komplek']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Kelas</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Kelas " value="<?= $santri['kelas']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Status Santri</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Status " value="<?= $santri['status_santri']?>">
                                        </div>
                                    </div>

                                    
                                   
                                    <div class="form-group row">
                                        <label for="note1" class="col-sm-3 text-right control-label col-form-label">Terdaftar</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder=" Terdaftar " value="<?= tgl_indo($santri['tgl_masuk'])?>">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                   
                    <div class="tab-pane fade " id="previous-month" role="tab"
                        aria-labelledby="pills-setting-tab">
                        <div class="card-body">
                        <?= form_open_multipart('admin/santri/detailSantri/'.$santri['nis']);  ?>
                           
                        
                                <div class="form-group">
                                    <label for="Nama Lengkap" class="col-md-12">Nama Lengkap</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Nama Lengkap" name="nama" id="nama" value="<?= $santri['nama']?>" class="form-control form-control-line">
                                    </div>
                                </div>
                                
                                <!-- <div class="form-group">
                                    <label class="col-md-12">Nama Panggilan</label>
                                    <div class="col-md-12">
                                        <input type="text" name="nama_panggilan"  class="form-control form-control-line"  value="<?= $santri['nama_panggilan']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Tempat Lahir</label>
                                    <div class="col-md-12">
                                        <input type="text" name="tempat_lahir"class="form-control form-control-line"  value="<?= $santri['tempat_lahir']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Tanggal Lahir</label>
                                    <div class="col-md-12">
                                        <input type="date" name="tgl_lahir" class="form-control form-control-line"  value="<?= $santri['tgl_lahir']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Nomor Hp</label>
                                    <div class="col-md-12">
                                        <input type="text" name="no_hp"class="form-control form-control-line"  value="<?= $santri['no_hp']?>">
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label for="profesi" class="control-label col-form-label">Institusi<span
                                        class="text-danger"></span></label>
                                        
                                        <select name="profesi" class="form-control"   id="profesi" required <?php  echo "value='".$santri['profesi']."'"?>>
                                    <?php foreach($institusi as $r):?>
                                            <option value="<?php echo $r['id_institusi'] ?>"><?php echo $r['nama_institusi'] ?></option>
                                    <?php endforeach ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Jurusan</label>
                                    <div class="col-md-12">
                                        <input type="text" name="jurusan"class="form-control form-control-line"  value="<?= $santri['jurusan']?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Nama Ayah</label>
                                    <div class="col-md-12">
                                        <input type="text" name="ayah"class="form-control form-control-line"  value="<?= $santri['ayah']?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Nama Ibu</label>
                                    <div class="col-md-12">
                                        <input type="text" name="ibu"class="form-control form-control-line"  value="<?= $santri['ibu']?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Nama Wali</label>
                                    <div class="col-md-12">
                                        <input type="text" name="wali"class="form-control form-control-line"  value="<?= $santri['wali']?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Telepon Rumah</label>
                                    <div class="col-md-12">
                                        <input type="text" name="tlp_rmh"class="form-control form-control-line"  value="<?= $santri['tlp_rmh']?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Alamat</label>
                                    <div class="col-md-12">
                                        <input type="text" name="alamat"class="form-control form-control-line"  value="<?= $santri['alamat']?>">
                                    </div>
                                </div>

                            

                                <div class="form-group">
                                    <label class="col-md-12">Kecamatan</label>
                                    <div class="col-md-12">
                                        <input type="text" name="kecamatan"class="form-control form-control-line"  value="<?= $santri['kecamatan']?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Kota/Kabupaten</label>
                                    <div class="col-md-12">
                                        <input type="text" name="kota"class="form-control form-control-line"  value="<?= $santri['kota']?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Provinsi</label>
                                    <div class="col-md-12">
                                        <input type="text" name="provinsi"class="form-control form-control-line"  value="<?= $santri['provinsi']?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Kode Pos</label>
                                    <div class="col-md-12">
                                        <input type="text" name="kode_pos"class="form-control form-control-line"  value="<?= $santri['kode_pos']?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="id_komplek" class="control-label col-form-label">Komplek/Kamar<span
                                        class="text-danger"></span></label>
                                        
                                        <select name="id_komplek" class="form-control"   id="id_komplek" required <?php  echo "value='".$santri['nama_komplek']."'"?>>
                                    <?php foreach($komplek as $r):?>
                                            <option value="<?php echo $r['id_komplek'] ?>"><?php echo $r['nama_komplek'] ?></option>
                                    <?php endforeach ?>
                                    </select>
                                </div> -->

                                <div class="form-group">
                                  <div class="col-md-12">
                                    <label for="id_kelas" class="control-label col-form-label">Kelas<span
                                        class="text-danger"></span></label>
                                        
                                        <select name="id_kelas" class="form-control"   id="id_kelas" required <?php  echo "value='".$santri['id_kelas']."'"?>>
                                    <?php foreach($kelas as $r):?>
                                            <option value="<?php echo $r['id_kelas'] ?>"><?php echo $r['kelas'] ?></option>
                                    <?php endforeach ?>
                                    </select>
                                  </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label for="id_status" class="control-label col-form-label">Status Santri<span
                                        class="text-danger"></span></label>
                                        
                                        <select name="id_status" class="form-control"   id="id_kelas" required <?php  echo "value='".$santri['id_status']."'"?>>
                                    <?php foreach($status_santri as $r):?>
                                            <option value="<?php echo $r['id_status'] ?>"><?php echo $r['status_santri'] ?></option>
                                    <?php endforeach ?>
                                    </select>
                                </div> -->

                               
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="submit"  class="btn btn-success" >Update Profile</button>
                                    </div>
                                </div>
                                </form>  
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
