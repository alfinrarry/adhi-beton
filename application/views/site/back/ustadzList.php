<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Column rendering -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<?php if(!empty($_SESSION['success_msg_register'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg_register']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>
					<div class="table-responsive">
						<table id="listProduct" class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th></th>
									<th>Nama Ustadz</th>
									<th>Alamat</th>
									<th>No Telp</th>
									<th>Edit | Delete</th>
								</tr>
							</thead>
							<tbody>
              <?php
              $no = 1;
              ?>
			  		<?php foreach($ustadz as $r):?>

                 <tr>
                    <td><?= $no++?></td>
                    <td><?php echo $r['nama_ustadz'] ?></td>
                    <td><?php echo $r['alamat'] ?></td>
                    <td><?php echo $r['no_telp'] ?></td>
                    <td>
                      <a href="<?php echo base_url('admin/ustadz/detailUstadz/'.$r['id_ustadz'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/ustadz/detailUstadz/'.$r['id_ustadz'])?>"><i class="fas fa-pencil-alt"></i></button>
                      <a href="<?php echo base_url('admin/ustadz/del_ustadz/'.$r['id_ustadz'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/ustadz/del_ustadz/'.$r['id_ustadz'])?>"><i class="fas fa-trash-alt"></i></button>
                    </td>
                </tr>               
              <?php
              $no++; 
             ?>

					<?php endforeach ?>

								</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->