<div class="container-fluid" data-codepage="<?= $codepage?>">

    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
               
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  active show" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab"
                            aria-controls="pills-profile" aria-selected="false">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                            role="tab" aria-controls="pills-setting" aria-selected="true">Setting</a>
                    </li>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Nama Pelajaran</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?= $pelajaran['nama_pelajaran']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="ename" class="col-sm-3 text-right control-label col-form-label">Created At</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Created At" id="alamat" name="created_at" value="<?= $pelajaran['created_at']?>">
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Updated At</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Updated At" value="<?= $pelajaran['updated_at']?>">
                                        </div>
                                    </div>
                                    
                                    
                                   
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                   
                    <div class="tab-pane fade " id="previous-month" role="tab"
                        aria-labelledby="pills-setting-tab">
                        <div class="card-body">
                        <?= form_open_multipart('admin/pelajaran/detailPelajaran/'.$pelajaran['id_pelajaran']);  ?>
                           
                        
                                <div class="form-group">
                                    <label for="Nama Lengkap" class="col-md-12">Nama Pelajaran</label>
                                    <div class="col-md-12">
                                    <input type="hidden" name="id_pelajaran" id="id_pelajaran" value="<?php $pelajaran['id_pelajaran'] ?>" >
                                        <input type="text" placeholder="Nama Pelajaran" name="nama_pelajaran" id="nama_pelajaran" value="<?= $pelajaran['nama_pelajaran']?>" class="form-control form-control-line">
                                    </div>
                                </div>
                                
                              
                                
                               
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="submit"  class="btn btn-success" >Update Profile</button>
                                    </div>
                                </div>
                                </form>  
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
