<div class="container-fluid" data-codepage="<?= $codepage?>">
<?php if(!empty($_SESSION['success_msg'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>

    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
           
                <div class="card-body text-center">
                    <div class="profile-pic m-b-20 m-t-20">
                        <h4 class="m-t-20 m-b-0"><?= $absensi['nama']?></h4>
                        <h4 class="m-t-20 m-b-0"><?= $absensi['kelas']?></h4>
                        <h4 class="m-t-20 m-b-0"><?= $absensi['nis']?></h4>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  active show" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab"
                            aria-controls="pills-profile" aria-selected="false">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                            role="tab" aria-controls="pills-setting" aria-selected="true">Setting</a>
                    </li>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Nama Santri</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?= $absensi['nama']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="ename" class="col-sm-3 text-right control-label col-form-label">Semester</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Semester" id="semester" name="semester" value="<?= $absensi['semester']?>">
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Bulan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Bulan" value="<?= $absensi['bulan']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Sakit</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="sakit" value="<?= $absensi['sakit']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Izin</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="izin" value="<?= $absensi['izin']?>">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Alpha</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="alpha" value="<?= $absensi['alpha']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Tahun Ajaran</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Tahun Ajaran" value="<?= $absensi['tahun_ajaran']?>">
                                        </div>
                                    </div>
                                    
                                   
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                   
                    <div class="tab-pane fade " id="previous-month" role="tab"
                        aria-labelledby="pills-setting-tab">
                        <div class="card-body">
                        <?= form_open_multipart('admin/absensi/detailAbsensi/'.$absensi['nis']);  ?>
                           
                        
                                <div class="form-group">
                                    <label for="Nama Lengkap" class="col-md-12">Nama Santri</label>
                                    <div class="col-md-12">
                                    <input type="hidden" name="nis" id="nis" value="<?php $absensi['nis'] ?>" >
                                        <input type="text" placeholder="Nama Santri" name="nama" id="nama" disabled value="<?= $absensi['nama']?>" class="form-control form-control-line">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                        <label for="absensi" class="control-label col-form-label">Semester
                                        <span class="text-danger"></span></label>
                                    <select name="semester" class="form-control"   id="semester" required <?php  echo "value='".$smt['semester']."'"?>>
                                        <?php foreach($smt as $s):?>
                                            <option value="<?php echo $s['semester'] ?>"><?php echo $s['semester'] ?></option>
                                        <?php endforeach ?>
                                    </select>
				                </div>
                                
                                <div class="form-group">
                                        <label for="absensi" class="control-label col-form-label">Bulan
                                        <span class="text-danger"></span></label>
                                    <select name="bulan" class="form-control"   id="bulan" required <?php  echo "value='".$bln['bulan']."'"?>>
                                        <?php foreach($bln as $s):?>
                                            <option value="<?php echo $s['bulan'] ?>"><?php echo $s['bulan'] ?></option>
                                        <?php endforeach ?>
                                    </select>
				                </div>

                                <div class="form-group">
                                    <label class="col-md-12">Sakit</label>
                                    <div class="col-md-12">
                                        <input type="text" name="sakit"class="form-control form-control-line"  value="<?= $absensi['sakit']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Izin</label>
                                    <div class="col-md-12">
                                        <input type="text" name="izin"class="form-control form-control-line"  value="<?= $absensi['izin']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Alpha</label>
                                    <div class="col-md-12">
                                        <input type="text" name="alpha"class="form-control form-control-line"  value="<?= $absensi['alpha']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label for="absensi" class="control-label col-form-label">Tahun Ajaran
                                        <span class="text-danger"></span></label>
                                    <select name="tahun_ajaran" class="form-control"   id="tahun_ajaran" required <?php  echo "value='".$th['tahun_ajaran']."'"?>>
                                        <?php foreach($th as $s):?>
                                            <option value="<?php echo $s['tahun_ajaran'] ?>"><?php echo $s['tahun_ajaran'] ?></option>
                                        <?php endforeach ?>
                                    </select>
				                </div>
                                
                               
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="submit"  class="btn btn-success" >Update Absen</button>
                                    </div>
                                </div>
                                </form>  
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
