<div class="container-fluid" data-codepage="<?= $codepage?>">

    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-20 m-t-20">
                        <h4 class="m-t-20 m-b-0"><?= $tahun_ajaran['tahun_ajaran']?></h4>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  active show" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab"
                            aria-controls="pills-profile" aria-selected="false">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                            role="tab" aria-controls="pills-setting" aria-selected="true">Setting</a>
                    </li>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Tahun Ajaran</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?= $tahun_ajaran['tahun_ajaran']?>">
                                        </div>
                                    </div>
                                    
                                    
                                   
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                   
                    <div class="tab-pane fade " id="previous-month" role="tab"
                        aria-labelledby="pills-setting-tab">
                        <div class="card-body">
                        <?= form_open_multipart('admin/TahunAjaran/detailTahunAjaran/'.$tahun_ajaran['id_tahun_ajaran']);  ?>
                           
                        
                                <div class="form-group">
                                    <label for="Nama Lengkap" class="col-md-12">Tahun Ajaran</label>
                                    <div class="col-md-12">
                                        <!-- <input type="hidden" name="id_tahun_ajaran" id="id_tahun_ajaran" value="<?php $tahun_ajaran['id_tahun_ajaran'] ?>" > -->
                                        <input type="text" placeholder="Tahun Ajaran" name="tahun_ajaran" id="tahun_ajaran" value="<?= $tahun_ajaran['tahun_ajaran']?>" class="form-control form-control-line">
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="submit"  class="btn btn-success" >Update Tahun</button>
                                    </div>
                                </div>
                                </form>  
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
