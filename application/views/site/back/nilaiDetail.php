<div class="container-fluid" data-codepage="<?= $codepage?>">

    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
            <?php if(!empty($_SESSION['success_msg'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>
                <div class="card-body text-center">
                    <div class="profile-pic m-b-20 m-t-20">
                    <img src="<?= img_url($img['img_path'])?>" width="150" class="rounded-circle" >
                        <h4 class="m-t-20 m-b-0"><?= $nilai['nama']?></h4>
                        <h4 class="m-t-20 m-b-0"><?= $nilai['kelas']?></h4>
                        <a href="mailto:<?= $nilai['nis']?>"><?= $nilai['nis']?></a>
                    </div>
                </div>
                
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  active show" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab"
                            aria-controls="pills-profile" aria-selected="false">Nilai</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                            role="tab" aria-controls="pills-setting" aria-selected="true">Edit</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:window.history.go(-1);"
                            >Kembali</a>
                    </li>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Tauhid</label>
                                        <div class="col-sm-9">
                                            <input type="hidden" class="form-control" id="nis" name="nis" disabled value="<?= $nilai['nis']?>">
                                            <input type="hidden" class="form-control" id="id_kelas" name="id_kelas" disabled value="<?= $nilai['kelas']?>">
                                            <input type="hidden" class="form-control" id="id_rekap" name="id_rekap" disabled value="<?= $rekap['id_rekap']?>">
                                            <input type="text" class="form-control" id="n_tauhid" name="n_tauhid" disabled value="<?= $nilai['n_tauhid']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Fiqih</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="n_fiqih" name="n_fiqih" disabled value="<?= $nilai['n_fiqih']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Mukhafadhoh</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="n_mukhofadhoh" name="n_mukhofadhoh" disabled value="<?= $nilai['n_mukhofadhoh']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Qiroatul Kitab</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="n_qiroatul_kitab" name="n_qiroatul_kitab" disabled value="<?= $nilai['n_qiroatul_kitab']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Tajwid</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="n_tajwid" name="n_tajwid" disabled value="<?= $nilai['n_tajwid']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Al-Qur'an</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="n_alquran" name="n_alquran" disabled value="<?= $nilai['n_alquran']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Imla'</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="n_imla" name="n_imla" disabled value="<?= $nilai['n_imla']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Tarikh</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="n_tarikh" name="n_tarikh" disabled value="<?= $nilai['n_tarikh']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Semester</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="semester" name="semester" disabled value="<?= $nilai['semester']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Tahun Ajaran</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="tahun_ajaran" name="tahun_ajaran" disabled value="<?= $nilai['tahun_ajaran']?>">
                                        </div>
                                    </div>
                                    
                                    
                                    
                                    
                                   
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                   
                    <div class="tab-pane fade " id="previous-month" role="tab"
                        aria-labelledby="pills-setting-tab">
                        <div class="card-body">
                        <?= form_open_multipart('admin/nilai/detailNilai/'.$nilai['nis']);  ?>
                           
                        
                    <div class="form-group">
                        <label for="nilai" class="col-md-12">Tauhid</label>
                            <div class="col-md-12">
                            <input type="text" class="form-control" name="n_tauhid" id="n_tauhid" required <?php  echo "value='".$nilai['n_tauhid']."'"?>>

                            </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="form-group">
                            <label for="nilai" class="control-label col-form-label">Fiqih<span
                                   class="text-danger"></span></label>
					 
                    <input type="text" class="form-control" name="n_fiqih" id="n_fiqih" required <?php  echo "value='".$nilai['n_fiqih']."'"?>>

				        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="form-group">
                            <label for="nilai" class="control-label col-form-label">Mukhafadhoh<span
                                   class="text-danger"></span></label>
					 
                    <input type="text" class="form-control" name="n_mukhofadhoh" id="n_mukhofadhoh" required <?php  echo "value='".$nilai['n_mukhofadhoh']."'"?>>

				        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="form-group">
                            <label for="nilai" class="control-label col-form-label">Qiroatul Kitab<span
                                   class="text-danger"></span></label>
					 
                    <input type="text" class="form-control" name="n_qiroatul_kitab" id="n_qiroatul_kitab" required <?php  echo "value='".$nilai['n_qiroatul_kitab']."'"?>>

				        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="form-group">
                            <label for="nilai" class="control-label col-form-label">Tajwid<span
                                   class="text-danger"></span></label>
					 
                    <input type="text" class="form-control" name="n_tajwid" id="n_tajwid" required <?php  echo "value='".$nilai['n_tajwid']."'"?>>

				        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="form-group">
                            <label for="nilai" class="control-label col-form-label">Al-Qur'an<span
                                   class="text-danger"></span></label>
					 
                    <input type="text" class="form-control" name="n_alquran" id="n_alquran" required <?php  echo "value='".$nilai['n_alquran']."'"?>>

				        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="form-group">
                            <label for="nilai" class="control-label col-form-label">Imla'<span
                                   class="text-danger"></span></label>
					 
                    <input type="text" class="form-control" name="n_imla" id="n_imla" required <?php  echo "value='".$nilai['n_imla']."'"?>>

				        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="form-group">
                            <label for="nilai" class="control-label col-form-label">Tarikh<span
                                   class="text-danger"></span></label>
					 
                    <input type="text" class="form-control" name="n_tarikh" id="n_tarikh" required <?php  echo "value='".$nilai['n_tarikh']."'"?>>

				        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="form-group">
                            <label for="nilai" class="control-label col-form-label">Tarikh<span
                                   class="text-danger"></span></label>
					 
                    <input type="text" class="form-control" name="n_tarikh" id="n_tarikh" required <?php  echo "value='".$nilai['n_tarikh']."'"?>>

				        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="form-group">
                            <label for="nilai" class="control-label col-form-label">Semester<span
                                   class="text-danger"></span></label>
                                   <select name="semester" class="form-control"   id="semester" required <?php  echo "value='".$smt['semester']."'"?>>
                                        <?php foreach($smt as $s):?>
                                            <option value="<?php echo $s['semester'] ?>"><?php echo $s['semester'] ?></option>
                                        <?php endforeach ?>
                                    </select>
				        </div>
                    </div>
                    <div class="col-sm-12 col-md-8">
                        <div class="form-group">
                            <label for="nilai" class="control-label col-form-label">Tahun Ajaran<span
                                   class="text-danger"></span></label>
                                   <select name="tahun_ajaran" class="form-control"   id="tahun_ajaran" required <?php  echo "value='".$th['tahun_ajaran']."'"?>>
                                        <?php foreach($th as $t):?>
                                            <option value="<?php echo $t['tahun_ajaran'] ?>"><?php echo $t['tahun_ajaran'] ?></option>
                                        <?php endforeach ?>
                                    </select>
				        </div>
                    </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="submit"  class="btn btn-success" >Update Nilai</button>
                                    </div>
                                </div>
                                </form>  
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
