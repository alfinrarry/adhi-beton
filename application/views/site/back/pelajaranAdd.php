<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
<?php $isEdit = $page_title == "Perbarui Produk"? true: false; ?>
		<div class="row">
			<div class="col-12 card">
			<form enctype="multipart/form-data" id="add_pelajaran" method="post" action="<?php echo base_url('admin/pelajaran/create_pelajaran/')?>"  data-dir="" data-url="">
            </div>
			  <div class="col-sm-12 col-md-8">
                <div class="form-group">
                  <label for="nama_pelajaran" class="control-label col-form-label">Nama Pelajaran<span
                      class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="nama_pelajaran" id="nama_pelajaran" required <?php if($isEdit) echo "value='".$pelajaran['nama_pelajaran']."'"?>>
                </div>
        </div>
    </div>

                  
				<div class="form-group text-right">
						<button class="btn btn-danger btn-sm waves-effect waves-light" type="submit" name="submit">
              <span   class="btn-label">
                <i class="fas fa-save"></i>
              </span> 
                Simpan
            </button>
				</div>
				</form>
			</div>
      