<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Column rendering -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<?php if(!empty($_SESSION['success_msg'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>

					<div class="form-group col-sm-5">
				 <form method="get" action="<?php echo base_url("admin/jadwal/pencarian/")?>">
				 <select class="form-control" name="kelas">
                            <option>--Pilih Kelas--</option>
                            <option value="1">2 ULA</option>
                            <option value="4">2B ULA</option>
                            <option value="5">3A ULA</option>
							<option value="6">3B ULA</option>
                            <option value="7">4A ULA</option>
                            <option value="8">4B ULA</option>
							<option value="9">1A WUSTHO</option>
                            <option value="10">1B WUSTHO</option>
                            <option value="11">2A WUSTHO</option>
							<option value="12">2B WUSTHO</option>
                            <option value="13">3A WUSTHO</option>
                            <option value="14">1 ULYA</option>
							<option value="15">2 ULYA</option>
                            <option value="16">3 ULYA</option>
                            <option value="17">LULUS</option>
							<option value="18">Belum Test</option>
                          </select>
						  <input type="submit" class="btn btn-primary" value="Cari">

            </div>
					<div class="table-responsive">
						<table id="listProduct" class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th></th>
									<th>Hari</th>
									<th>Pelajaran</th>
									<th>Kelas</th>
									<th>Ustadz</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
              <?php
              $no = 1;
              ?>
			  			   <?php foreach($hasil as $r):?>

                <tr>
                    <td><?= $no++?></td>
					<td><?php echo $r['nama_hari'] ?></td>
                    <td><?php echo $r['nama_pelajaran'] ?></td>
                    <td><?php echo $r['kelas'] ?></td>
                    <td><?php echo $r['nama_ustadz'] ?></td>
                    <td>
					<a href="<?php echo base_url('admin/jadwal/detailJadwal/'.$r['id_jadwal'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/jadwal/detailJadwal/'.$r['id_jadwal'])?>"><i class="fas fa-pencil-alt"></i></button>
                      <a href="<?php echo base_url('admin/jadwal/del_jadwal/'.$r['id_jadwal'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/jadwal/del_jadwal/'.$r['id_jadwal'])?>"><i class="fas fa-trash-alt"></i></button>
                    </td>
                </tr>      
				<?php endforeach ?>
          
              <?php
              $no++; 
             ?>
								</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->