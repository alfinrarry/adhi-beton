<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Column rendering -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<?php if(!empty($_SESSION['success_msg_register'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg_register']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>

		<div class="form-group pull-right "><br>
            <div class="row">
				<div class="form-group col-sm-3">
                	<a href="<?php echo base_url('admin/Archive/formArchive')?>" type="submit" class="btn btn-md btn-primary">Tambah Data</a>
				</div>
          	</div>
        </div>

					<div class="table-responsive">
						<table id="listProduct" class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th></th>
                                    <th>Jenis Test</th>
									<th>Nama Test</th>
                                    <th>Sample Source</th>
                                    <th>Tested By</th>
									<th>Received Date</th>
									<th>Serifikat</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							
                  		
						 
              <?php
              $no = 1;
              ?>
			   <!-- <?php foreach($archive as $r):?> -->
                <tr>
                    <td><?= $no++?></td>
                    <td><?php echo $r['jenis_test'] ?></td>
                    <td><?php echo $r['nama_test'] ?></td>
                    <td><?php echo $r['sample_source_name'] ?></td>
                    <td><?php echo $r['name_pelaksana'] ?></td>
                    <td><?php echo $r['received_date'] ?></td>
                    <td><?php 
                        $sertif= $r['sertif_img'] ;
                        $tampil=substr($sertif,20,50);
                        echo $tampil; 
                    ?><br>
					<a href="<?php echo base_url('admin/Archive/downloadArchive/'.$r['sertif_img'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/Archive/downloadArchive/'.$r['sertif_img'])?>"><i class="fas fa-alt">Unduh</i></button>
					</td>
                    <td>
					<a href="<?php echo base_url('admin/Archive/detailArchive/')?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/Archive/detailArchive/'.$r['id_archive_test'])?>"><i class="fas fa-pencil-alt"></i></button>
                    <a href="<?php echo base_url('admin/Archive/delArchive/'.$r['id_archive_test'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/Archive/delArchive/'.$r['id_archive_test'])?>"><i class="fas fa-trash"></i></button>
                </tr>      
				<!-- <?php endforeach ?> -->


				    
       
              <?php
              $no++; 
             ?>
								</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>


<!-- ============================================================== -->
<!-- End Container fluid  -->