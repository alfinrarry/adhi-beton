<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Column rendering -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<?php if(!empty($_SESSION['success_msg_register'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg_register']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>

		<div class="form-group pull-right "><br>
            <div class="row">
				<div class="form-group col-sm-3">
                	<a href="<?php echo base_url('admin/SampleDescription/formSampleDescription')?>" type="submit" class="btn btn-md btn-primary">Tambah Data</a>
				</div>
          	</div>
        </div>

					<div class="table-responsive">
						<table id="listProduct" class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Nama Sample Description</th>
									<th>Action</th>

								</tr>
							</thead>
							<tbody>
							
                  		
						 
              <?php
              $no = 1;
              ?>
			   <!-- <?php foreach($sampleDescription as $sd):?> -->





                <tr>
                    <td><?= $no++?></td>
                    <td><?php echo $sd['sample_description_name'] ?></td>
                    <td>
					<a href="<?php echo base_url('admin/SampleDescription/FormEditSampleDescription/'.$sd['id_sample_description'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/SampleDescription/FormEditSampleDescription/'.$sd['id_sample_description'])?>"><i class="fas fa-pencil-alt"></i></button>
                    <a href="<?php echo base_url('admin/SampleDescription/delSampleDescription/'.$sd['id_sample_description'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/SampleDescription/delSampleDescription/'.$sd['id_sample_description'])?>"><i class="fas fa-trash"></i></button>
					</td>
                </tr>      
				<!-- <?php endforeach ?> -->

      


				    
       
              <?php
              $no++; 
             ?>
								</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>


<!-- ============================================================== -->
<!-- End Container fluid  -->
