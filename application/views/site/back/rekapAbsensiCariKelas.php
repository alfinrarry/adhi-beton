<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Column rendering -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<?php if(!empty($_SESSION['success_msg'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>


                    <div class="form-group pull-right "><br>
            <div class="row">
              	<div class="form-group col-sm-3">
			  		<form method="get" action="<?php echo base_url("admin/RekapAbsensi/pencarian/")?>">
			  			<select class="form-control" name="kelas">
			  				<option>--Pilih Kelas--</option>
                            <option value="1">2 ULA</option>
                            <option value="5">3A ULA</option>
							<option value="6">3B ULA</option>
                            <option value="7">4A ULA</option>
                            <option value="8">4B ULA</option>
							<option value="9">1A WUSTHO</option>
                            <option value="10">1B WUSTHO</option>
                            <option value="11">2A WUSTHO</option>
							<option value="12">2B WUSTHO</option>
                            <option value="13">3A WUSTHO</option>
                            <option value="14">1 ULYA</option>
							<option value="15">2 ULYA</option>
                            <option value="16">3 ULYA</option>
                          </select>
              	</div>
                <div class="form-group col-sm-3">
				<select class="form-control" name="semester">
			  				<option>--Pilih Semester--</option>
                            <option value="1">Semester 1</option>
                            <option value="2">Semester 2</option>
                          </select>
                </div>
                <div class="form-group col-sm-3">
				<select class="form-control" name="bulan">
                        <?php foreach($bln as $b):?>
                            <option value="<?php echo $b['bulan'] ?>"><?php echo $b['bulan'] ?></option>
                        <?php endforeach ?>
                </select>
                </div>
                <div class="form-group col-sm-3">
				<select class="form-control" name="tahun_ajaran">
                        <?php foreach($th as $t):?>
                            <option value="<?php echo $t['tahun_ajaran'] ?>"><?php echo $t['tahun_ajaran'] ?></option>
                        <?php endforeach ?>
               </select>
                </div>
                </div>
              		<div class="form-group text-right">
                		<button type="submit" class="btn btn-md btn-primary" id="make_url">Tampilkan</button>
					</div>
            	</div>
          	</div>
        </div>


					<div class="table-responsive">
						<table id="id_kelas" class="table table-striped" style="width:100%">
							<thead>

								<tr>
									<th>No</th>
									<th style="text-align: center;">Nama Santri</th>
									<th style="text-align: center;">Kelas</th>
									<th style="text-align: center;">Semester</th>
									<th style="text-align: center;">Bulan</th>
									<th style="text-align: center;">S</th>
									<th style="text-align: center;">I</th>
									<th style="text-align: center;">A</th>
									<th style="text-align: center;">Tahun Ajaran</th>

								
								</tr>

							</thead>
							<tbody>

							
							
							





							

							
						

                  		
						 
              <?php
              $no = 1; 
              ?>
			   <?php foreach($hasil as $r):?>





                <tr>
                    <td><?= $no++?></td>
                    <td style="text-align: center;"><?php echo $r['nama'] ?></td>
                    <td style="text-align: center;"><?php echo $r['kelas'] ?></td>
                    <td style="text-align: center;"><?php echo $r['semester'] ?></td>
                    <td style="text-align: center;"><?php echo $r['bulan'] ?></td>
                    <td style="text-align: center;"><?php echo $r['sakit'] ?></td>
                    <td style="text-align: center;"><?php echo $r['izin'] ?></td>
                    <td style="text-align: center;"><?php echo $r['alpha'] ?></td>
                    <td style="text-align: center;"><?php echo $r['tahun_ajaran'] ?></td>
                    <td>
					<a href="<?php echo base_url('admin/RekapAbsensi/deleteRekap/'.$r['id_rekap'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm" type="button"  data-id="" data-dir="<?php echo base_url('admin/RekapAbsensi/deleteRekap/'.$r['id_rekap'])?>"><i class="fas fa-trash"></i></button>

                    </td>
                </tr>      
				<?php endforeach ?>

				
				

      


				    
       
              <?php
              $no++; 
             ?>
			 
								</tfoot>
						</table>
						
			</div>
		</div>
	</div>

	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>


<!-- ============================================================== -->
<!-- End Container fluid  -->