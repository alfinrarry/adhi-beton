<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Column rendering -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<?php if(!empty($_SESSION['success_msg_register'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg_register']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>

                    <div class="box box-info">
        <div class="box-header">
         
		  
        <div class="form-group pull-right "><br>
            <div class="row">
              	<div class="form-group col-sm-3">
			  		<form method="get" action="<?php echo base_url("admin/RekapAbsensi/pencarian/")?>">
			  			<select class="form-control" name="kelas">
			  				<option>--Pilih Kelas--</option>
                            <option value="1">2 ULA</option>
                            <option value="5">3A ULA</option>
							<option value="6">3B ULA</option>
                            <option value="7">4A ULA</option>
                            <option value="8">4B ULA</option>
							<option value="9">1A WUSTHO</option>
                            <option value="10">1B WUSTHO</option>
                            <option value="11">2A WUSTHO</option>
							<option value="12">2B WUSTHO</option>
                            <option value="13">3A WUSTHO</option>
                            <option value="14">1 ULYA</option>
							<option value="15">2 ULYA</option>
                            <option value="16">3 ULYA</option>
                          </select>
              	</div>
                <div class="form-group col-sm-3">
				<select class="form-control" name="semester">
			  				<option>--Pilih Semester--</option>
                            <option value="1">Semester 1</option>
                            <option value="2">Semester 2</option>
                          </select>
                </div>
                <div class="form-group col-sm-3">
				<select class="form-control" name="bulan">
                        <?php foreach($bln as $b):?>
                            <option value="<?php echo $b['bulan'] ?>"><?php echo $b['bulan'] ?></option>
                        <?php endforeach ?>
                </select>
                </div>
                <div class="form-group col-sm-3">
				<select class="form-control" name="tahun_ajaran">
                        <?php foreach($th as $t):?>
                            <option value="<?php echo $t['tahun_ajaran'] ?>"><?php echo $t['tahun_ajaran'] ?></option>
                        <?php endforeach ?>
               </select>
                </div>
                </div>
              		<div class="form-group text-right">
                		<button type="submit" class="btn btn-md btn-primary" id="make_url">Tampilkan</button>
					</div>
            	</div>
          	</div>
        </div>

			
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->