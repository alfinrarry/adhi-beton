<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
<style>
* {
  box-sizing: border-box;
}

body {
  background-color: #f1f1f1;
}

#regForm {
  background-color: #ffffff;
  margin: 100px auto;
  font-family: Raleway;
  padding: 40px;
  width: 70%;
  min-width: 300px;
}

h1 {
  text-align: center;  
}

input {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
}
option {
  padding: 10px;
  width: 100%;
  font-size: 17px;
  font-family: Raleway;
  border: 1px solid #aaaaaa;
}

/* Mark input boxes that gets an error on validation: */
input.invalid {
  background-color: #ffdddd;
}

/* Hide all steps by default: */
.tab {
  display: none;
}

button {
  background-color: #04AA6D;
  color: #ffffff;
  border: none;
  padding: 10px 20px;
  font-size: 17px;
  font-family: Raleway;
  cursor: pointer;
}

button:hover {
  opacity: 0.8;
}

#prevBtn {
  background-color: #bbbbbb;
}

/* Make circles that indicate the steps of the form: */
.step {
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbbbbb;
  border: none;  
  border-radius: 50%;
  display: inline-block;
  opacity: 0.5;
}

.step.active {
  opacity: 1;
}

/* Mark the steps that are finished and valid: */
.step.finish {
  background-color: #04AA6D;
}
</style>
<body>
<form id="regForm" enctype="multipart/form-data"  method="post" action="<?php echo base_url('admin/JobMixFormula/inputJobMixFormula/')?>"  data-dir="" data-url="">
  <!-- One "tab" for each step in the form: -->
  <div class="tab" >

    Nama Job Mix Formula
    <p><input type="text" class="form-control"  name="name_jmf" id="name_jmf"</p>

    <p>Nama Product<br>
    <select class="form-control" name="id_product">
      <?php foreach($product as $ss):?>
          <option value="<?php echo $ss['id_product'] ?>"><?php echo $ss['name_product'] ?></option>
      <?php endforeach ?>
    </select></p>

    <p>Mutu<br>
    <select class="form-control" name="id_mutu">
      <?php foreach($mutu as $ss):?>
          <option value="<?php echo $ss['id_mutu'] ?>"><?php echo $ss['name_mutu'] ?></option>
      <?php endforeach ?>
    </select></p>

    <p>Sumber Semen<br>
    <select class="form-control" name="id_sumber_semen">
      <?php foreach($sumberSemen as $ss):?>
          <option value="<?php echo $ss['id_sumber_semen'] ?>"><?php echo $ss['tempat_sumber'] ?></option>
      <?php endforeach ?>
    </select></p>

    Jumlah Semen
    <p><input type="text" class="form-control"  name="jumlah_semen" id="jumlah_semen"</p>

  <p>Sumber Halus <br>
    <select class="form-control" name="id_sumber_halus">
      <?php foreach($sumberHalus as $ss):?>
          <option value="<?php echo $ss['id_sumber_halus'] ?>"><?php echo $ss['tempat_sumber'] ?></option>
      <?php endforeach ?>
    </select></p>

    Jumlah Sumber Halus
    <p><input type="text" class="form-control"  name="jumlah_halus" id="jumlah_halus"</p>

    <p>Sumber Kasar<br>
    <select class="form-control" name="id_sumber_kasar">
      <?php foreach($sumberKasar as $ss):?>
          <option value="<?php echo $ss['id_sumber_kasar'] ?>"><?php echo $ss['tempat_sumber'] ?></option>
      <?php endforeach ?>
    </select></p>

    Jumlah Sumber Kasar
    <p><input type="text" class="form-control"  name="jumlah_kasar" id="jumlah_kasar"</p>

    <p>Sumber Additive<br>
    <select class="form-control" name="id_sumber_additive">
      <?php foreach($sumberAdditive as $ss):?>
          <option value="<?php echo $ss['id_sumber_additive'] ?>"><?php echo $ss['tempat_sumber'] ?></option>
      <?php endforeach ?>
    </select></p>

    Jumlah Sumber Additive
    <input type="text" class="form-control"  name="jumlah_additive" id="jumlah_additive"</p>

    Jumlah Sumber Air
    <p><input type="text" class="form-control"  name="jumlah_air" id="jumlah_air"</p>

    <p>Dokumentasi
    <input type="file" class="form-control"  name="doc_img" id="doc_img"</p>

  </div>

  <div style="overflow:auto;">
    <div style="float:right;">
      <button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
    </div>
  </div>
  <!-- Circles which indicates the steps of the form: -->
  <div style="text-align:center;margin-top:40px;">
    <span class="step"></span>
  </div>
</form>

<script>
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the current tab

function showTab(n) {
  // This function will display the specified tab of the form...
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  //... and fix the Previous/Next buttons:
  if (n == 0) {
    document.getElementById("prevBtn").style.display = "none";
  } else {
    document.getElementById("prevBtn").style.display = "inline";
  }
  if (n == (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Submit";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  //... and run a function that will display the correct step indicator:
  fixStepIndicator(n)
}

function nextPrev(n) {
  // This function will figure out which tab to display
  var x = document.getElementsByClassName("tab");
  // Exit the function if any field in the current tab is invalid:
  if (n == 1 && !validateForm()) return false;
  // Hide the current tab:
  x[currentTab].style.display = "none";
  // Increase or decrease the current tab by 1:
  currentTab = currentTab + n;
  // if you have reached the end of the form...
  if (currentTab >= x.length) {
    // ... the form gets submitted:
    document.getElementById("regForm").submit();
    return false;
  }
  // Otherwise, display the correct tab:
  showTab(currentTab);
}

function validateForm() {
  // This function deals with validation of the form fields
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  // A loop that checks every input field in the current tab:
  for (i = 0; i < y.length; i++) {
    // If a field is empty...
    if (y[i].value == "") {
      // add an "invalid" class to the field:
      y[i].className += " invalid";
      // and set the current valid status to false
      valid = false;
    }
  }
  // If the valid status is true, mark the step as finished and valid:
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  // This function removes the "active" class of all steps...
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  //... and adds the "active" class on the current step:
  x[n].className += " active";
}
</script>

</body>
</html>
