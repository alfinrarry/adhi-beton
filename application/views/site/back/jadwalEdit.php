<div class="container-fluid" data-codepage="<?= $codepage?>">

    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
               
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  active show" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab"
                            aria-controls="pills-profile" aria-selected="false">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                            role="tab" aria-controls="pills-setting" aria-selected="true">Setting</a>
                    </li>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Hari</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="nama_hari" name="nama_hari" disabled value="<?= $jadwal['nama_hari']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="ename" class="col-sm-3 text-right control-label col-form-label">Pelajaran</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="nama_pelajaran" name="nama_pelajaran" disabled value="<?= $jadwal['nama_pelajaran']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="ename" class="col-sm-3 text-right control-label col-form-label">Kelas</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="kelas" name="kelas" disabled value="<?= $jadwal['kelas']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="ename" class="col-sm-3 text-right control-label col-form-label">Ustadz</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="nama_ustadz" name="nama_ustadz" disabled value="<?= $jadwal['nama_ustadz']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Created At</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Created At" value="<?= $jadwal['created_at']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Updated At</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Updated At" value="<?= $jadwal['updated_at']?>">
                                        </div>
                                    </div>
                                    
                                    
                                   
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                   
                    <div class="tab-pane fade " id="previous-month" role="tab"
                        aria-labelledby="pills-setting-tab">
                        <div class="card-body">
                        <?= form_open_multipart('admin/jadwal/detailJadwal/'.$jadwal['id_jadwal']);  ?>
                           
                        
                                <div class="form-group">
                                    <label for="Nama Lengkap" class="col-md-12">Hari</label>
                                        <div class="col-md-12">
                                            <input type="hidden" name="id_jadwal" id="id_jadwal" value="<?php $jadwal['id_jadwal'] ?>" >
                                        <select name="id_hari" class="form-control"   id="id_hari" required <?php  echo "value='".$jadwal['id_hari']."'"?>>
                                            <?php foreach($hari as $r):?>
                                            <option value="<?php echo $r['id_hari'] ?>"><?php echo $r['nama_hari'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="id_pelajaran" class="control-label col-form-label">Pelajaran<span
                                        class="text-danger"></span></label>
                                        
                                        <select name="id_pelajaran" class="form-control"   id="id_pelajaran" required <?php  echo "value='".$pelajaran['id_pelajaran']."'"?>>
                                    <?php foreach($pelajaran as $r):?>
                                            <option value="<?php echo $r['id_pelajaran'] ?>"><?php echo $r['nama_pelajaran'] ?></option>
                                    <?php endforeach ?>
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label for="id_kelas" class="control-label col-form-label">Kelas<span
                                        class="text-danger"></span></label>
                                        
                                        <select name="id_kelas" class="form-control"   id="id_kelas" required <?php  echo "value='".$kelas['id_kelas']."'"?>>
                                    <?php foreach($kelas as $r):?>
                                            <option value="<?php echo $r['id_kelas'] ?>"><?php echo $r['kelas'] ?></option>
                                    <?php endforeach ?>
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label for="id_ustadz" class="control-label col-form-label">Ustadz<span
                                        class="text-danger"></span></label>
                                        
                                        <select name="id_ustadz" class="form-control"   id="id_ustadz" required <?php  echo "value='".$ustadz['id_ustadz']."'"?>>
                                    <?php foreach($ustadz as $r):?>
                                            <option value="<?php echo $r['id_ustadz'] ?>"><?php echo $r['nama_ustadz'] ?></option>
                                    <?php endforeach ?>
                                    </select>
                                </div>



                                
                              
                                
                               
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="submit"  class="btn btn-success" >Update Profile</button>
                                    </div>
                                </div>
                                </form>  
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
