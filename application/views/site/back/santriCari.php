<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Column rendering -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<?php if(!empty($_SESSION['success_msg'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>

				
					<div class="form-group pull-right "><br>
            <div class="row">
              	<div class="form-group col-sm-3">
			  		<form method="get" action="<?php echo base_url("admin/santri/pencarian/")?>">
			  			<select class="form-control" name="kelas">
			  				<option>--Pilih Kelas--</option>
                            <option value="1">2 ULA</option>
                            <option value="4">2B ULA</option>
                            <option value="5">3A ULA</option>
							<option value="6">3B ULA</option>
                            <option value="7">4A ULA</option>
                            <option value="8">4B ULA</option>
							<option value="9">1A WUSTHO</option>
                            <option value="10">1B WUSTHO</option>
                            <option value="11">2A WUSTHO</option>
							<option value="12">2B WUSTHO</option>
                            <option value="13">3A WUSTHO</option>
                            <option value="14">1 ULYA</option>
							<option value="15">2 ULYA</option>
                            <option value="16">3 ULYA</option>
                          </select>
              	</div>
				
				<div class="form-group col-sm-3">
                	<button type="submit" class="btn btn-md btn-primary" value=>Cari</button>
				</div>

                

              
            	</div>
          	</div>
        </div>

         

					<div class="table-responsive">
						<table id="listProduct" class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th></th>
									<th width="50%" >Nama Santri</th>
									<th>Komplek</th>
									<th>Kelas</th>
									<th>Edit | Delete</th>
								</tr>
							</thead>
							<tbody>
							
                  		
						 
              <?php
              $no = 1;
              ?>
			   <?php foreach($hasil as $r):?>





                <tr>
                    <td><?= $no++?></td>
                    <td><?php echo $r['nama'] ?></td>
                    <td><?php echo $r['nama_komplek'] ?></td>
                    <td><?php echo $r['kelas'] ?></td>
                    <td>
                      <a href="<?php echo base_url('admin/santri/detailSantri/'.$r['nis'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/santri/detailSantri/'.$r['nis'])?>"><i class="fas fa-pencil-alt"></i></button>
                      <a href="<?php echo base_url('admin/santri/del_santri/'.$r['nis'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/santri/del_santri/'.$r['nis'])?>"><i class="fas fa-trash-alt"></i></button>
                    </td>
                </tr>      
				<?php endforeach ?>

      


				    
       
              <?php
              $no++; 
             ?>
								</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>


<!-- ============================================================== -->
<!-- End Container fluid  -->