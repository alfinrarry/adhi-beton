<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Column rendering -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<?php if(!empty($_SESSION['success_msg_register'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg_register']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>

		<div class="form-group pull-right "><br>
            <div class="row">
				<div class="form-group col-sm-3">
                	<button type="submit" class="btn btn-md btn-primary" value=>Tambah Data</button>
				</div>
          	</div>
        </div>

					<div class="table-responsive">
						<table id="listProduct" class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th></th>
									<th>Sample Source</th>
									<th>Received Date</th>
									<th>Tested By</th>
									<th>FM</th>
									<th>SG</th>
									<th>AB</th>
									<th>OI</th>
									<th>MP</th>
									<th>UW</th>
									<th>Action</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
							
                  		
						 
              <?php
              $no = 1;
              ?>
			   <?php foreach($santri as $r):?>





                <tr>
                    <td><?= $no++?></td>
                    <td><?php echo "Sample A" ?></td>
                    <td><?php echo $r['nama_komplek'] ?></td>
                    <td><?php echo "BUDI" ?></td>
					<td><?php echo "80" ?></td>
                    <td><?php echo "80" ?></td>
                    <td><?php echo "80" ?></td>
					<td><?php echo "90" ?></td>
                    <td><?php echo "80" ?></td>
                    <td><?php echo "80" ?></td>
                    <td>
					<a href="<?php echo base_url('admin/santri/detailSantri/'.$r['nis'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/santri/detailSantri/'.$r['nis'])?>"><i class="fas fa-eye"></i></button>
                    <a href="<?php echo base_url('admin/santri/del_santri/'.$r['nis'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/santri/del_santri/'.$r['nis'])?>"><i class="fas fa-ban"></i></button>
                    <a href="<?php echo base_url('admin/santri/detailSantri/'.$r['nis'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/santri/detailSantri/'.$r['nis'])?>"><i class="fas fa-download"></i></button>
                    <a href="<?php echo base_url('admin/santri/del_santri/'.$r['nis'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/santri/del_santri/'.$r['nis'])?>"><i class="fas fa-trash"></i></button>
					<td><?php echo "DITOLAK" ?></td>
					</td>
                </tr>      
				<?php endforeach ?>

      


				    
       
              <?php
              $no++; 
             ?>
								</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>


<!-- ============================================================== -->
<!-- End Container fluid  -->