<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Column rendering -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<?php if(!empty($_SESSION['success_msg_register'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg_register']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>

		<!-- <div class="form-group pull-right "><br>
            <div class="row">
				<div class="form-group col-sm-3">
                	<a href="<?php echo base_url('admin/SlumpFlowTest/formSlumpFlowTest')?>" type="submit" class="btn btn-md btn-primary">Tambah Data</a>
				</div>
          	</div>
        </div> -->

					<div class="table-responsive">
						<table id="listProduct" class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th></th>
                                    <th>Nama JMF</th>
									<th>Product</th>
                                    <th>Mutu</th>
									<th>Customer</th>
                                    <th>Project</th>
                                    <th>Date of pouring</th>
									<th>Date of test</th>
									<th>Status</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							
                  		
						 
              <?php
              $no = 1;
              ?>
			   <!-- <?php foreach($compressive as $r):?> -->
                <tr>
                    <td><?= $no++?></td>
                    <td><?php echo $r['name_jmf'] ?>&nbsp<a href="<?php echo base_url('admin/DailyCompressive/formDailyCompressive/'.$r['id_daily_compressive'])?>"> <button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/DailyCompressive/formDailyCompressive/'.$r['id_daily_compressive'])?>"><i class="fas fa-plus"> Add Compressive</i></button></td>
					<td><?php echo $r['name_product'] ?></td>
                    <td><?php echo $r['name_mutu'] ?></td>
					<td><?php echo $r['customer_name'] ?></td>
                    <td><?php echo $r['id_project'] ?></td>
					<td><?php echo $r['date_of_pouring'] ?></td>
                    <td><?php echo $r['date_test'] ?></td>
					<td><?php echo $r['status'] ?></td>
                    <td>
					<a href="<?php echo base_url('admin/DailyCompressive/detailDailyCompressive/'.$r['id_daily_compressive'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/DailyCompressive/detailDailyCompressive/'.$r['id_daily_compressive'])?>"><i class="fas fa-pencil-alt"></i></button>
                    <a href="<?php echo base_url('admin/DailyCompressive/delDailyCompressive/'.$r['id_daily_compressive'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/DailyCompressive/delDailyCompressive/'.$r['id_daily_compressive'])?>"><i class="fas fa-trash"></i></button>
					<a href="<?php echo base_url('admin/DailyCompressive/detailDailyCompressive/'.$r['id_daily_compressive'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/DailyCompressive/detailDailyCompressive/'.$r['id_daily_compressive'])?>"><i class="fas fa-print"> Print</i></button>
                </tr>
                <!-- <?php endforeach ?> -->
      


				    
       
              <?php
              $no++; 
             ?>
								</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>


<!-- ============================================================== -->
<!-- End Container fluid  -->