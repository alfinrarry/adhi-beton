<div class="container-fluid" data-codepage="<?php echo $codepage ?>">

    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-20 m-t-20">
                        <img src="<?= img_url($dailyIncomingHalus['doc_img'])?>" width="150" class="rounded-circle" alt="user">
                        <h4 class="m-t-20 m-b-0">Dokumentasi</h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  active show" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab"
                            aria-controls="pills-profile" aria-selected="false">View</a>
                    </li>
                    <?php if ($dailyIncomingHalus['id_daily_incoming_halus']):?>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                            role="tab" aria-controls="pills-setting" aria-selected="true">Edit</a>
                    </li>
                    <?php endif;?>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Jenis Material</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?= $dailyIncomingHalus['name_material']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="ename" class="col-sm-3 text-right control-label col-form-label">Surat Jalan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Nama Lengkap" id="fullname" name="fullname" value="<?= $dailyIncomingHalus['surat_jalan']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="rate" class="col-sm-3 text-right control-label col-form-label">Vendor</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Email" value="<?= $dailyIncomingHalus['vendor_name']?>">
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Quarry</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingHalus['name_quarry']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Butiran</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingHalus['kriteria_visual_butiran']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Kekerasan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingHalus['kriteria_visual_kekerasan']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Warna</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingHalus['kriteria_visual_warna']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Slit Content</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingHalus['slit_content']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Pelaksana</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingHalus['name_pelaksana']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Keterangan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingHalus['keterangan']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Status</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingHalus['status']?>">
                                        </div>
                                    </div>
                                    
                                    
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                   
                    <div class="tab-pane fade " id="previous-month" role="tab"
                        aria-labelledby="pills-setting-tab">
                        <div class="card-body">
                        <?= form_open_multipart('admin/DailyIncomingHalus/updateIncomingHalus/'.$dailyIncomingHalus['id_daily_incoming_halus']);  ?>
                           
                        
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Jenis Material</label>
                                    <div class="col-md-12">
                                        <input type="hidden" name="id_daily_incoming_halus" id="id_daily_incoming_halus">
                                        <input type="text" placeholder="surat_jalan" disabled name="surat_jalan" class="form-control form-control-line"  value="<?= $dailyIncomingHalus['name_material']?>">                                
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Surat Jalan</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="surat_jalan" name="surat_jalan" class="form-control form-control-line"  value="<?= $dailyIncomingHalus['surat_jalan']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Vendor</label>
                                    <div class="col-md-12">
                                        <input type="text" name="id_vendor"  class="form-control form-control-line" disabled  value="<?= $dailyIncomingHalus['vendor_name']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Quarry</label>
                                    <div class="col-md-12">
                                        <input type="text" name="id_quarry"  class="form-control form-control-line" disabled  value="<?= $dailyIncomingHalus['name_quarry']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Butiran</label>
                                    <div class="col-md-12">
                                    <select class="form-control" name="kriteria_visual_butiran">
                                           <option value="Yes">Yes</option>
                                           <option value="No">No</option>
                                     </select>                                  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Kekerasan</label>
                                    <div class="col-md-12">
                                    <select class="form-control" name="kriteria_visual_kekerasan">
                                           <option value="Yes">Yes</option>
                                           <option value="No">No</option>
                                     </select>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Warna</label>
                                    <div class="col-md-12">
                                    <select class="form-control" name="kriteria_visual_warna">
                                           <option value="Yes">Yes</option>
                                           <option value="No">No</option>
                                     </select>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Slit Content</label>
                                    <div class="col-md-12">
                                    <input type="text" name="slit_content"  class="form-control form-control-line"   value="<?= $dailyIncomingHalus['slit_content']?>">  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Pelaksana</label>
                                    <div class="col-md-12">
                                        <input type="text" name="id_pelaksana"  class="form-control form-control-line" disabled  value="<?= $dailyIncomingHalus['name_pelaksana']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Keterangan</label>
                                    <div class="col-md-12">
                                        <input type="text" name="keterangan"  class="form-control form-control-line"  value="<?= $dailyIncomingHalus['keterangan']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Status</label>
                                    <div class="col-md-12">
                                        <input type="text" name="id_status"  class="form-control form-control-line" disabled  value="<?= $dailyIncomingHalus['status']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label for="img" class="col-sm-12">Image</label>
                                        <div class="col-sm-9">
                                        <input type="file" id="doc_img"   data-height="200" value="<?= img_url($dailyIncomingHalus['doc_img']); ?>" name="doc_img" />
                                        </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="submit"  class="btn btn-success" >Update Profile</button>
                                    </div>
                                </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
