<div class="container-fluid" data-codepage="<?php echo $codepage ?>">

    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-20 m-t-20">
                        <img src="<?= img_url($dailyIncomingKasar['doc_img'])?>" width="150" class="rounded-circle" alt="user">
                        <h4 class="m-t-20 m-b-0">Dokumentasi</h4>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  active show" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab"
                            aria-controls="pills-profile" aria-selected="false">View</a>
                    </li>
                    <?php if ($dailyIncomingKasar['id_daily_incoming_kasar']):?>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                            role="tab" aria-controls="pills-setting" aria-selected="true">Edit</a>
                    </li>
                    <?php endif;?>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Jenis Material</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?= $dailyIncomingKasar['name_material']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="ename" class="col-sm-3 text-right control-label col-form-label">Surat Jalan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Nama Lengkap" id="fullname" name="fullname" value="<?= $dailyIncomingKasar['surat_jalan']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="rate" class="col-sm-3 text-right control-label col-form-label">Vendor</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Email" value="<?= $dailyIncomingKasar['vendor_name']?>">
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Quarry</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingKasar['name_quarry']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Bersih</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingKasar['kriteria_visual_bersih']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Sedang</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingKasar['kriteria_visual_sedang']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Kotor</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingKasar['kriteria_visual_kotor']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Ukuran</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingKasar['ukuran']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Pelaksana</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingKasar['name_pelaksana']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Keterangan</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingKasar['keterangan']?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Status</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="No Handphone" value="<?= $dailyIncomingKasar['status']?>">
                                        </div>
                                    </div>
                                    
                                    
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                   
                    <div class="tab-pane fade " id="previous-month" role="tab"
                        aria-labelledby="pills-setting-tab">
                        <div class="card-body">
                        <?= form_open_multipart('admin/DailyIncomingKasar/updateIncomingKasar/'.$dailyIncomingKasar['id_daily_incoming_kasar']);  ?>
                           
                        
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Jenis Material</label>
                                    <div class="col-md-12">
                                        <input type="hidden" name="id_daily_incoming_kasar" id="id_daily_incoming_kasar">
                                        <input type="text" placeholder="surat_jalan" disabled name="surat_jalan" class="form-control form-control-line"  value="<?= $dailyIncomingKasar['name_material']?>">                                
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Surat Jalan</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="surat_jalan" name="surat_jalan" class="form-control form-control-line"  value="<?= $dailyIncomingKasar['surat_jalan']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Vendor</label>
                                    <div class="col-md-12">
                                        <input type="text" name="id_vendor"  class="form-control form-control-line" disabled  value="<?= $dailyIncomingKasar['vendor_name']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Quarry</label>
                                    <div class="col-md-12">
                                        <input type="text" name="id_quarry"  class="form-control form-control-line" disabled  value="<?= $dailyIncomingKasar['name_quarry']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Bersih</label>
                                    <div class="col-md-12">
                                    <select class="form-control" name="kriteria_visual_bersih">
                                           <option value="Yes">Yes</option>
                                           <option value="No">No</option>
                                     </select>                                  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Sedang</label>
                                    <div class="col-md-12">
                                    <select class="form-control" name="kriteria_visual_sedang">
                                           <option value="Yes">Yes</option>
                                           <option value="No">No</option>
                                     </select>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Kotor</label>
                                    <div class="col-md-12">
                                    <select class="form-control" name="kriteria_visual_kotor">
                                           <option value="Yes">Yes</option>
                                           <option value="No">No</option>
                                     </select>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Ukuran</label>
                                    <div class="col-md-12">
                                    <select class="form-control" name="ukuran">
                                           <option value="Yes">Yes</option>
                                           <option value="No">No</option>
                                     </select>  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Pelaksana</label>
                                    <div class="col-md-12">
                                        <input type="text" name="id_pelaksana"  class="form-control form-control-line" disabled  value="<?= $dailyIncomingKasar['name_pelaksana']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Keterangan</label>
                                    <div class="col-md-12">
                                        <input type="text" name="keterangan"  class="form-control form-control-line"  value="<?= $dailyIncomingKasar['keterangan']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Status</label>
                                    <div class="col-md-12">
                                        <input type="text" name="id_status"  class="form-control form-control-line" disabled  value="<?= $dailyIncomingKasar['status']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                        <label for="img" class="col-sm-12">Image</label>
                                        <div class="col-sm-9">
                                        <input type="file" id="doc_img"   data-height="200" value="<?= img_url($dailyIncomingKasar['doc_img']); ?>" name="doc_img" />
                                        </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="submit"  class="btn btn-success" >Update Profile</button>
                                    </div>
                                </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
