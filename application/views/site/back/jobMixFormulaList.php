<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Column rendering -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<?php if(!empty($_SESSION['success_msg_register'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg_register']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>

		<div class="form-group pull-right "><br>
            <div class="row">
				<div class="form-group col-sm-3">
                	<a href="<?php echo base_url('admin/JobMixFormula/formJobMixFormula')?>" type="submit" class="btn btn-md btn-primary">Tambah Data</a>
				</div>
          	</div>
        </div>

					<div class="table-responsive">
						<table id="listProduct" class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th></th>
									<th>Nama Job Mix Formula</th>
									<th>Nama Product</th>
                                    <th>Mutu</th>
                                    <th>Dokumentasi</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							
                  		
						 
              <?php
              $no = 1;
              ?>
			   <!-- <?php foreach($jmf as $r):?> -->
                <tr>
                    <td><?= $no++?></td>
					<td><?php echo $r['name_jmf'] ?></td>
                    <td><?php echo $r['name_product'] ?></td>
                    <td><?php echo $r['name_mutu'] ?></td>
                    <td><?php 
                        $sertif= $r['doc_img'] ;
                        $tampil=substr($sertif,26,50);
                        echo $tampil; 
                    ?><br>
					</td>
                    <td>
					<a href="<?php echo base_url('admin/JobMixFormula/detailJobMixFormula/'.$r['id_job_mix_formula'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/JobMixFormula/detailJobMixFormula/'.$r['id_job_mix_formula'])?>"><i class="fas fa-pencil-alt"></i></button>
                    <a href="<?php echo base_url('admin/JobMixFormula/delJobMixFormula/'.$r['id_job_mix_formula'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/JobMixFormula/delJobMixFormula/'.$r['id_job_mix_formula'])?>"><i class="fas fa-trash"></i></button>
					<a href="<?php echo base_url('admin/JobMixFormula/detailJobMixFormula/'.$r['id_job_mix_formula'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/JobMixFormula/detailJobMixFormula/'.$r['id_job_mix_formula'])?>"><i class="fas fa-print"></i></button>
                </tr>      
				<!-- <?php endforeach ?> -->


				    
       
              <?php
              $no++; 
             ?>
								</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>


<!-- ============================================================== -->
<!-- End Container fluid  -->