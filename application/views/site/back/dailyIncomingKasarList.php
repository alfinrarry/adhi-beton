<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<!-- Column rendering -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<?php if(!empty($_SESSION['success_msg_register'])):?>
						<div class="alert alert-success" role="alert">
							<?php echo $_SESSION['success_msg_register']?>
						</div>
					<?php elseif(!empty($_SESSION['fail_msg'])):?>
					<div class="alert alert-danger" role="alert">
						<?php echo $_SESSION['fail_msg']?>
					</div>
					<?php endif;?>

		<div class="form-group pull-right "><br>
            <div class="row">
				<div class="form-group col-sm-3">
                	<a href="<?php echo base_url('admin/DailyIncomingKasar/formDailyIncomingKasar')?>" type="submit" class="btn btn-md btn-primary">Tambah Data</a>
				</div>
          	</div>
        </div>

					<div class="table-responsive">
						<table id="listProduct" class="table table-striped" style="width:100%">
							<thead>
								<tr>
									<th>No</th>
									<th>Jenis Material</th>
                                    <th>Surat Jalan</th>
                                    <th>Vendor</th>
                                    <th>Quarry</th>
                                    <th>Bersih</th>
                                    <th>Sedang</th>
                                    <th>Kotor</th>
                                    <th>Ukuran</th>
                                    <th>Pelaksana</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
									<th>Action</th>

								</tr>
							</thead>
							<tbody>
							
                  		
						 
              <?php
              $no = 1;
              ?>
			   <!-- <?php foreach($dailyIncomingKasar as $dik):?> -->





                <tr>
                    <td><?= $no++?></td>
                    <td><?php echo $dik['name_material'] ?></td>
                    <td><?php echo $dik['surat_jalan'] ?></td>
                    <td><?php echo $dik['vendor_name'] ?></td>
                    <td><?php echo $dik['name_quarry'] ?></td>
                    <td><?php echo $dik['kriteria_visual_bersih'] ?></td>
                    <td><?php echo $dik['kriteria_visual_sedang'] ?></td>
                    <td><?php echo $dik['kriteria_visual_kotor'] ?></td>
                    <td><?php echo $dik['ukuran'] ?></td>
                    <td><?php echo $dik['name_pelaksana'] ?></td>
                    <td><?php echo $dik['keterangan'] ?></td>
                    <td><?php echo $dik['status'] ?></td>
                    <td>
					<a href="<?php echo base_url('admin/DailyIncomingKasar/detailIncomingKasar/'.$dik['id_daily_incoming_kasar'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/DailyIncomingKasar/detailIncomingKasar/'.$dik['id_daily_incoming_kasar'])?>"><i class="fas fa-pencil-alt"></i></button>
                    <a href="<?php echo base_url('admin/DailyIncomingKasar/delIncomingKasar/'.$dik['id_daily_incoming_kasar'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/DailyIncomingKasar/delIncomingKasar/'.$dik['id_daily_incoming_kasar'])?>"><i class="fas fa-trash"></i></button>
					<a href="<?php echo base_url('admin/DailyIncomingKasar/tolakIncomingKasar/'.$dik['id_daily_incoming_kasar'])?>"><button class="btn btn-googleplus waves-effect btn-rounded waves-light btn-danger btn-sm del-product" type="button" data-id="" data-dir="<?php echo base_url('admin/DailyIncomingKasar/tolakIncomingKasar/'.$dik['id_daily_incoming_kasar'])?>"><i class="fas fa-false">TOLAK</i></button>
					<a href="<?php echo base_url('admin/DailyIncomingKasar/setujuIncomingKasar/'.$dik['id_daily_incoming_kasar'])?>"><button class="btn btn-facebook waves-effect btn-rounded waves-light btn-info btn-sm edit-product " type="button"  data-id="" data-dir="<?php echo base_url('admin/DailyIncomingKasar/setujuIncomingKasar/'.$dik['id_daily_incoming_kasar'])?>"><i class="fas fa-pencil">SETUJU</i></button>
					</td>
                </tr>      
				<!-- <?php endforeach ?> -->

      


				    
       
              <?php
              $no++; 
             ?>
								</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>


<!-- ============================================================== -->
<!-- End Container fluid  -->
