<div class="container-fluid" data-codepage="<?php echo $codepage ?>">
<?php $isEdit = $page_title == "Perbarui Produk"? true: false; ?>
		<div class="row">
			<div class="col-12 card">
			<form enctype="multipart/form-data" id="add_ustadz" method="post" action="<?php echo base_url('admin/ustadz/addUstadz/')?>"  data-dir="" data-url="">
            </div>
			  <div class="col-sm-12 col-md-8">
                <div class="form-group">
                  <label for="nama_ustadz" class="control-label col-form-label">Nama Ustadz<span
                      class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="nama_ustadz" id="nama_ustadz" required <?php if($isEdit) echo "value='".$ustadz['nama_ustadz']."'"?>>
                </div>
              </div>
			  <div class="col-sm-12 col-md-8">
                <div class="form-group">
                  <label for="alamat" class="control-label col-form-label">Alamat<span
                      class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="alamat" id="alamat" required <?php if($isEdit) echo "value='".$ustadz['alamat']."'"?>>
                </div>
              </div>
			  <div class="col-sm-12 col-md-8">
                <div class="form-group">
                  <label for="no_telp" class="control-label col-form-label">Nomor Telepon<span
                      class="text-danger">*</span></label>
                  <input type="text" class="form-control" name="no_telp" id="no_telp" required <?php if($isEdit) echo "value='".$ustadz['no_telp']."'"?>>
				        </div>
        </div>
        </div>
        <div class="form-group text-right">
						<button class="btn btn-danger btn-sm waves-effect waves-light" type="submit" name="submit"><span
								class="btn-label"><i class="fas fa-save"></i></span> Simpan</button>
				</div>
				</form>
			
</div>
      