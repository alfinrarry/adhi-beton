<div class="container-fluid" data-codepage="<?= $codepage?>">

    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body text-center">
                    <div class="profile-pic m-b-20 m-t-20">
                        <h4 class="m-t-20 m-b-0"><?= $ustadz['nama_ustadz']?></h4>
                    </div>
                    <button type="button" class="btn btn-outline-primary btn-rounded"><i class="fa fa-uncheck"  ><?php if ($useradmin['is_ban'] == 1) echo"Terverifikasi"; else echo"Dinonaktifkan"; ?></i></button>
                </div>
                
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <!-- Tabs -->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link  active show" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab"
                            aria-controls="pills-profile" aria-selected="false">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                            role="tab" aria-controls="pills-setting" aria-selected="true">Setting</a>
                    </li>
                </ul>
                <!-- Tabs -->
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="card-body">
                                    <div class="form-group row">
                                        <label for="pname" class="col-sm-3 text-right control-label col-form-label">Nama Ustadz</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled value="<?= $ustadz['nama_ustadz']?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="ename" class="col-sm-3 text-right control-label col-form-label">Alamat</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Alamat" id="alamat" name="nama_panggilan" value="<?= $ustadz['alamat']?>">
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <label for="etime" class="col-sm-3 text-right control-label col-form-label">Nomor Telepon</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" disabled placeholder="Nomor Telepon" value="<?= $ustadz['no_telp']?>">
                                        </div>
                                    </div>
                                    
                                    
                                   
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                   
                    <div class="tab-pane fade " id="previous-month" role="tab"
                        aria-labelledby="pills-setting-tab">
                        <div class="card-body">
                        <?= form_open_multipart('admin/ustadz/detailUstadz/'.$ustadz['id_ustadz']);  ?>
                           
                        
                                <div class="form-group">
                                    <label for="Nama Lengkap" class="col-md-12">Nama Ustadz</label>
                                    <div class="col-md-12">
                                    <input type="hidden" name="nis" id="nis" value="<?php $ustadz['id_ustadz'] ?>" >
                                        <input type="text" placeholder="Nama Ustadz" name="nama_ustadz" id="nama_ustadz" value="<?= $ustadz['nama_ustadz']?>" class="form-control form-control-line">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-md-12">Alamat</label>
                                    <div class="col-md-12">
                                        <input type="text" name="alamat"  class="form-control form-control-line"  value="<?= $ustadz['alamat']?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Nomor Telepon</label>
                                    <div class="col-md-12">
                                        <input type="text" name="no_telp"class="form-control form-control-line"  value="<?= $ustadz['no_telp']?>">
                                    </div>
                                </div>
                                
                               
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" name="submit"  class="btn btn-success" >Update Profile</button>
                                    </div>
                                </div>
                                </form>  
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
</div>
