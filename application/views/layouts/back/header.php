<!-- Topbar header - style you can find in pages.scss -->
<!-- ============================================================== -->
<header class="topbar">
	<nav class="navbar top-navbar navbar-expand-md navbar-dark">
		<div class="navbar-header">
			<!-- This is for the sidebar toggle which is visible on mobile only -->
			<a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)">
				<i class="ti-menu ti-close"></i>
			</a>
			<!-- ============================================================== -->
			<!-- Logo -->
			<!-- ============================================================== -->
			<div class="navbar-brand">
				<a href="index.html" class="logo">
					<!-- Logo icon -->
					<b class="logo-icon">
						<!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
						<!-- Dark Logo icon -->
						<img src="<?php echo vendor_url('back/images/logo-icon.png'); ?>" alt="homepage" class="dark-logo" />
						<!-- Light Logo icon -->
						<img src="<?php echo vendor_url('back/images/logo-light-icon.png'); ?>" alt="homepage" class="light-logo" />
					</b>
					<!--End Logo icon -->
					<!-- Logo text -->
					<span class="logo-text">
						<!-- dark Logo text -->
						<img src="<?php echo vendor_url('back/images/logo-text.png'); ?>" alt="homepage" class="dark-logo" />
						<!-- Light Logo text -->
						<!-- <img src="<?php echo vendor_url('back/images/logo-light-text.png'); ?>" class="light-logo" alt="homepage" /> -->
					</span>
				</a>
				<a class="sidebartoggler d-none d-md-block" href="javascript:void(0)" data-sidebartype="mini-sidebar">
					<i class="mdi mdi-toggle-switch mdi-toggle-switch-off font-20"></i>
				</a>
			</div>
			<!-- ============================================================== -->
			<!-- End Logo -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Toggle which is visible on mobile only -->
			<!-- ============================================================== -->
			<a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)"
				data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
				aria-expanded="false" aria-label="Toggle navigation">
				<i class="ti-more"></i>
			</a>
		</div>
		<!-- ============================================================== -->
		<!-- End Logo -->
		<!-- ============================================================== -->
		<div class="navbar-collapse collapse" id="navbarSupportedContent">
			<!-- ============================================================== -->
			<!-- toggle and nav items -->
			<!-- ============================================================== -->
			<ul class="navbar-nav float-left mr-auto"> 
				<li class="nav-item d-none d-md-block">
                            <a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar">
                                <i class="mdi mdi-menu font-24"></i>
                            </a>
                        </li>
				<!-- ============================================================== -->
				<!-- Search -->
				<!-- ============================================================== -->
				
			</ul>
			<!-- ============================================================== -->
			<!-- Right side toggle and nav items -->
			<!-- ============================================================== -->
			<ul class="navbar-nav float-right">
				
				<!-- Comment -->
				<!-- ============================================================== -->
				
				<!-- ============================================================== -->
				<!-- End Comment -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- User profile and search -->
				<!-- ============================================================== -->	
				
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle waves-effect waves-dark pro-pic" href="#" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">
						<?php foreach ($image as $u):?>
						<img src="<?= img_url($u['img_path'])?>" alt="user" class="rounded-circle" width="40">
						<?php endforeach ?>

						<span class="m-l-5 font-medium d-none d-sm-inline-block"><?php echo ucwords($_SESSION['fullname']);?><i
								class="mdi mdi-chevron-down"></i></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
						<span class="with-arrow">
							<span class="bg-primary"></span>
						</span>
						<div class="d-flex no-block align-items-center p-15 bg-primary text-white m-b-10">
							<div class="">
							<?php foreach ($image as $u):?>
								<img src="<?= img_url($u['img_path'])?>" alt="user" class="rounded-circle" width="60">
								<?php endforeach ?>

							</div>
							
							<div class="m-l-10">
								<h4 class="mb-0"><?php echo ucwords($_SESSION['fullname']);?></h4>
								<p class=" mb-0"><?php echo ucwords($_SESSION['email']); ?></p>
							</div>
						</div>
						
						<a class="dropdown-item" href="<?php echo base_url('admin/User/editPasswordAct/'.$_SESSION['id'])?>">
							<i class="ti-settings m-r-5 m-l-5"></i> Account Setting</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="<?php echo base_url('admin/User/logout')?>">
							<i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
						<div class="dropdown-divider"></div>
						
					</div>
				</li>
				<!-- ============================================================== -->
				<!-- User profile and search -->
				<!-- ============================================================== -->
			</ul>
		</div>
	</nav>
</header>
<!-- ============================================================== -->
<!-- End Topbar header -->