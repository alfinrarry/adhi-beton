<aside class="left-sidebar">
	<!-- Sidebar scroll-->
	<div class="scroll-sidebar">
		<!-- Sidebar navigation-->
		<nav class="sidebar-nav">
			<ul id="sidebarnav">
				<!-- dashboard -->
				<br>
				<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('admin/dashboard')?>"
					 aria-expanded="false"><i class="mdi mdi-apps"></i><span class="hide-menu">Dashboard</span></a></li>
				<!-- end dashboard -->
				<li class="nav-small-cap">
					<i class="mdi mdi-dots-horizontal"></i>
					<span class="hide-menu">ADHI BETON PERSADA</span>
				</li>
				<!-- Master -->
				<li class="sidebar-item">
					<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
						<i class="mdi mdi-archive"></i>
						<span class="hide-menu">Master</span>
					</a>
					<ul aria-expanded="false" class="collapse  first-level">

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/Customer/listCustomer')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Customer</span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/Project/listProject')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Project </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/Product/listProduct')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Product </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/JenisMaterial/listJenisMaterial')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Jenis Material </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/SumberMaterial/listSumberMaterial')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Sumber Material Halus </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/SumberMaterialKasar/listSumberMaterialKasar')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Sumber Material Kasar</span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/SumberMaterialSemen/listSumberMaterialSemen')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Sumber Material Semen </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/SumberMaterialAdditive/listSumberMaterialAdditive')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Sumber Material Additive</span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/Mutu/listMutu')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Mutu </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/Pelaksana/listPelaksana')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Pelaksana </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/Type/listType')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Type </span>
							</a>
						</li>
						
						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/sampleSource/listSampleSource')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Sample Source </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/sampleDescription/listsampleDescription')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Sample Description </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/toBeUsed/listToBeUsed')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> To be used </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/Supplier/listSupplier')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Supplier </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/Vendor/listVendor')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Vendor </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/Quarry/listQuarry')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Quarry </span>
							</a>
						</li>
					</ul>
				</li>
				<!-- end Master -->

				<!-- Daily Incoming Test -->
				<li class="sidebar-item">
				<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
						<i class="mdi mdi-archive"></i>
						<span class="hide-menu">Daily Incoming Test</span>
					</a>
					<ul aria-expanded="false" class="collapse  first-level">
					
						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/DailyIncomingKasar/listDailyIncomingKasar')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu">Kasar</span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/DailyIncomingHalus/listDailyIncomingHalus')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu">  Halus</span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/DailyIncomingAdditive/listDailyIncomingAdditive')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu">  Additive</span>
							</a>
						</li>
						
					</ul>
				</li>
				<!-- End Daily Incoming Test -->

				<!-- Internal Test -->
				<li class="sidebar-item">
					<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
						<i class="mdi mdi-archive"></i>
						<span class="hide-menu">Internal Test </span>
					</a>
					<ul aria-expanded="false" class="collapse  first-level">

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/InternalTest/listAgregatHalus')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Agregat Halus</span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/InternalTestKasar/listAgregatKasar')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Agregat Kasar </span>
							</a>
						</li>
						
					</ul>
				</li>
				<!-- end Internal Test -->

				<!-- Independent Test -->
				<li class="sidebar-item">
						<a href="<?php echo base_url('admin/IndependentTest/listIndependentTest')?>" class="sidebar-link">
								<i class="mdi mdi-archive"></i>
								<span class="hide-menu"> Independent Test</span>
						</a>
				</li>
				<!-- End Independet Test -->

				<!-- Job Mix Formula -->
				<li class="sidebar-item">
						<a href="<?php echo base_url('admin/JobMixFormula/listJobMixFormula')?>" class="sidebar-link">
								<i class="mdi mdi-archive"></i>
								<span class="hide-menu"> Job Mix Formula</span>
						</a>
				</li>
				<!-- End Job Mix Formula -->

				<!-- Slump Flow Test -->
				<li class="sidebar-item">
						<a href="<?php echo base_url('admin/SlumpFlowTest/listSlumpFlowTest')?>" class="sidebar-link">
								<i class="mdi mdi-archive"></i>
								<span class="hide-menu"> Slump Flow Test</span>
						</a>
				</li>
				<!-- End Slump Flow Test -->

				<!-- Daily Compressive Strenght Data -->
				<li class="sidebar-item">
						<a href="<?php echo base_url('admin/DailyCompressive/listDailyCompressive')?>" class="sidebar-link">
								<i class="mdi mdi-archive"></i>
								<span class="hide-menu"> Daily Compressive Test</span>
						</a>
				</li>
				<!-- End Daily Compressive -->

				<!-- Archive -->
				<li class="sidebar-item">
						<a href="<?php echo base_url('admin/Archive/listArchive')?>" class="sidebar-link">
								<i class="mdi mdi-archive"></i>
								<span class="hide-menu"> Archive Test File</span>
						</a>
				</li>
				<!-- End Archive -->

				<!-- User admin -->

				<li class="sidebar-item">
					<a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
						<i class="mdi mdi-account-key"></i>
						<span class="hide-menu">User Admin</span>
					</a>
					<ul aria-expanded="false" class="collapse  first-level">
						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/user/formAddUser')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Tambah User Admin</span>
							</a>
						</li>
						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/user/listUser')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> List User Admin</span>
							</a>
						</li>
					</ul>
						
				<!-- end user admin -->

				

				

			</ul>
							


					
					
				
		</nav>
		<!-- End Sidebar navigation -->
	</div>
	<!-- End Sidebar scroll-->
</aside>