<!-- Topbar header - style you can find in pages.scss -->
<!-- ============================================================== -->
<header class="topbar">
	<nav class="navbar bg-dark top-navbar navbar-expand-md navbar-dark">
		<div class="navbar-header">
			<!-- This is for the sidebar toggle which is visible on mobile only -->
			</a>
			<!-- ============================================================== -->
			<!-- Logo -->
			<!-- ============================================================== -->
			<div class="navbar-brand">
				
			
			</div>
			<!-- ============================================================== -->
			<!-- End Logo -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Toggle which is visible on mobile only -->
			<!-- ============================================================== -->
		
		</div>
		<!-- ============================================================== -->
		<!-- End Logo -->
		<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- toggle and nav items -->
			<!-- ============================================================== -->
			<ul class="navbar-nav float-left mr-auto"> 
			
			<li class="nav-item ">
					<a class="nav-link text-white  waves-effect waves-dark pro-pic" href="<?php echo base_url('home')?>" data-toggle=""
						aria-haspopup="true" aria-expanded="false">
						<span class="m-l-5 font-medium d-none d-sm-inline-block">Home</span>
					</a>
			</li>

			<li class="nav-item ">
					<a class="nav-link text-white  waves-effect waves-dark pro-pic" href="<?php echo base_url('absensi')?>" data-toggle=""
						aria-haspopup="true" aria-expanded="false">
						<span class="m-l-5 font-medium d-none d-sm-inline-block">Absensi</span>
					</a>
			</li>

			<li class="nav-item ">
					<a class="nav-link text-white  waves-effect waves-dark pro-pic" href="<?php echo base_url('khs')?>" data-toggle=""
						aria-haspopup="true" aria-expanded="false">
						<span class="m-l-5 font-medium d-none d-sm-inline-block">Kartu Hasil Studi </span>
					</a>
			</li>

			<li class="nav-item ">
					<a class="nav-link text-white  waves-effect waves-dark pro-pic" href="<?php echo base_url('JadwalPelajaran')?>" data-toggle=""
						aria-haspopup="true" aria-expanded="false">
						<span class="m-l-5 font-medium d-none d-sm-inline-block">Jadwal Pelajaran </span>
					</a>
			</li>
				<!-- ============================================================== -->
				<!-- Search -->
				<!-- ============================================================== -->
				

				
			</ul>
		
			<!-- ============================================================== -->
			<!-- Right side toggle and nav items -->
			<!-- ============================================================== -->
			<ul class="navbar-nav float-right">
				
				<!-- Comment -->
				<!-- ============================================================== -->
				
				<!-- ============================================================== -->
				<!-- End Comment -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- User profile and search -->
				<!-- ============================================================== -->	
				
				<li class="nav-item dropdown">
					<a class="nav-link text-white dropdown-toggle waves-effect waves-dark pro-pic" href="#" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false">
						<img src="<?= img_url(getUser($_SESSION['id'])['img_path'])?>" alt="user" class="rounded-circle" width="40">
						<span class="m-l-5 font-medium d-none d-sm-inline-block"><?= ucwords(getUser($_SESSION['id'])['nama'])?><i
								class="mdi mdi-chevron-down"></i></span>
					</a>
					<div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
						<span class="with-arrow">
							<span class="bg-dark"></span>
						</span>
						<div class="d-flex no-block align-items-center p-15 bg-dark text-white m-b-10">
							<div class="">
							<?php foreach ($image as $u):?>
								<img src="<?= img_url($u['img_path'])?>" alt="user" class="rounded-circle" width="60">
								<?php endforeach ?>

							</div>
							
							<div class="m-l-10">
								<h4 class="mb-0"><?php echo ucwords($_SESSION['fullname']);?></h4>
								<p class=" mb-0"><?php echo ucwords($_SESSION['email']); ?></p>
							</div>
						</div>
						
						<a class="dropdown-item" href="<?php echo base_url('User/editPasswordAct/'.$_SESSION['id'])?>">
							<i class="ti-settings m-r-5 m-l-5"></i> Pengaturan Akun </a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="<?php echo base_url('logout')?>">
							<i class="fa fa-power-off m-r-5 m-l-5"></i> Keluar</a>
						<div class="dropdown-divider"></div>
						
					</div>
				</li>

				
				<!-- ============================================================== -->
				<!-- User profile and search -->
				<!-- ============================================================== -->
			</ul>
			
		</div>
	</nav>
</header>
<!-- ============================================================== -->
<!-- End Topbar header -->