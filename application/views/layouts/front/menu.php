<aside class="left-sidebar">
	<!-- Sidebar scroll-->
	<div class="scroll-sidebar">
		<!-- Sidebar navigation-->
		<nav class="sidebar-nav">
			<ul id="sidebarnav">
				<!-- dashboard -->
				<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('home')?>"
					 aria-expanded="false"><i class="mdi mdi-apps"></i><span class="hide-menu">Biodata</span></a></li>
				<!-- end dashboard -->
				<li class="nav-small-cap">
					<i class="mdi mdi-dots-horizontal"></i>
					<span class="hide-menu">SISTEM INFORMASI SANTRI</span>
				</li>

				<!-- Data Santri -->
				<li class="sidebar-item">
					
						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/Santri/formAddSantri')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu">Kartu Hasil Ujian</span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/Santri/listSantri')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Jadwal Pelajaran </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/Santri/listSantri')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Jadwal Ujian  </span>
							</a>
						</li>

						<li class="sidebar-item">
							<a href="<?php echo base_url('admin/Santri/listSantri')?>" class="sidebar-link">
								<i class="mdi mdi-adjust"></i>
								<span class="hide-menu"> Absensi </span>
							</a>
						</li>
					
				</li>
				<!-- end Data Santri -->

			
							


					
					
				
		</nav>
		<!-- End Sidebar navigation -->
	</div>
	<!-- End Sidebar scroll-->
</aside>